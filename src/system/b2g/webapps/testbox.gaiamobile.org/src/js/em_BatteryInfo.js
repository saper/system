/**
 * Created by xa on 6/11/14.
 */

'use strict';
$('menuItem-batteryInfo').addEventListener('click', function() {
  BatteryInfo.init();
});
// ------------------------------------------------------------------------
const BATTERY_CONTENT_FILE = 'batterycapacity';
const CHARGER_CURRENT_FILE = 'batterycurrent_now';
const CHARGER_VOLTAGE_FILE = 'batteryvoltage_now';
const BATTERY_HEALTHY_FILE = 'health';
const BATTERY_PLUGGED_FILE = 'plug';

var engmodeEx = null;
var _self;
var interval = 5;
var batteryInfoInterval;
var isfirstinit = true;
var BatteryInfo = {
  init: function batteryInfoinit(){
    _self = this;
    engmodeEx = navigator.engmodeExtension;
    if(true == isfirstinit)
    {
      _self.batteryInfoaddListeners();
      isfirstinit = false;
    }
  },
  batteryInfoaddListeners: function batteryInfoaddListeners(){
    $('btinfo_interval_TimeAdd').addEventListener('click', function() {
      interval += 1;
      $('lbdefault_interval_Time').textContent = interval;
    });
    $('btinfo_interval_TimeaSubtract').addEventListener('click', function() {
      if(1 < interval)
        interval -= 1;
      $('lbdefault_interval_Time').textContent = interval;
    });
    $('btinfo_start').addEventListener('click', function() {
      _self.setBTState(false);
      if (engmodeEx) {
        _self.batteryInfoUpdate();
        batteryInfoInterval = window.setInterval(function() {
          _self.batteryInfoUpdate();
        }, interval*1000);
      }
      else {
        $('battery_infoResultPanel').innerHTML = 'engmodeExtension Not support';
      }
    });
    $('btinfo_stop').addEventListener('click', function() {
      _self.setBTState(true);
      _self.cancelall();
    });
  },
  batteryInfoUpdate: function batteryInfoUpdate(){
      var request = engmodeEx.getSysInfo('BATTERY_TEMP');
      request.onsuccess = function(e) {
        var temp = request.result;
        var mytime = new Date();
        var testTime = mytime.toLocaleTimeString();
        var capacity = engmodeEx.fileReadLE(BATTERY_CONTENT_FILE);
        var health = engmodeEx.fileReadLE(BATTERY_HEALTHY_FILE);
        var plug = (engmodeEx.fileReadLE(BATTERY_PLUGGED_FILE)==1)?'plugged':'unplugged';
        var current_now = engmodeEx.fileReadLE(CHARGER_CURRENT_FILE);
        var voltage_now = engmodeEx.fileReadLE(CHARGER_VOLTAGE_FILE);
        var chargerStatus = window.navigator.battery.charging;

        var initCommand ='Test Time : ' + testTime + ' '
          + '{ ' + (temp / 10) + ', '
          + capacity.substr(0, capacity.length - 1) + ', '
          + current_now.substr(0, current_now.length - 1) + ', '
          + voltage_now.substr(0, voltage_now.length - 1) + ', '
          + (chargerStatus ? 'Charging' : 'Discharging') + ' }\n';
          engmodeEx.fileWriteLE(initCommand, '/data/testbox_log/BatteryInfo.log', 'a');

        $('battery_infoResultPanel').innerHTML
        = 'Test Time is: ' + testTime + '<br>'
        + 'Temperature =' + (temp / 10) + '<br>'
        + 'capacity = ' + capacity + '<br>'
        + 'health = ' + health + '<br>'
        + 'power plug = ' + plug + '<br>'
        + 'current_now = ' + current_now + '<br>'
        + 'voltage_now = ' + voltage_now + '<br>'
        + 'Charger Status = ' + (chargerStatus ? 'Charging' : 'Discharging')+ '<br>'
        + '****************************' + '<br>'
        + $('battery_infoResultPanel').innerHTML;

      };

      request.onerror = function(e) {
        $('battery_infoResultPanel').innerHTML =
          'get battery info failed.';
      };
  },
  cancelall: function batteryInfoCancelall(){
    /*if (batteryInfoInterval) {
      clearInterval(batteryInfoInterval);
      batteryInfoInterval = null;
    } */
  },
  setBTState: function batteryInfosetBTState(state){
    $('btinfo_interval_TimeAdd').disabled = !state;
    $('btinfo_interval_TimeaSubtract').disabled = !state;
    $('btinfo_start').disabled = !state;
    $('btinfo_stop').disabled = state;

    if(true == $('btinfo_start').disabled) {
      window.setTimeout(function() {$('btinfo_stop').focus(); }, 100);
    } else {
      window.setTimeout(function() {$('btinfo_start').focus(); }, 100);
    }
  }
};
