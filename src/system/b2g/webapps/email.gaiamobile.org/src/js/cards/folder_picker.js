define([ "require", "tmpl!./fld/folder_item.html", "tmpl!./fld/account_item.html", "folder_depth_classes", "cards", "model", "evt", "l10n!", "transition_end", "css!style/folder_cards", "./base", "template!./folder_picker.html" ], function(e) {
    var t = e("tmpl!./fld/folder_item.html"), n = e("tmpl!./fld/account_item.html"), o = e("folder_depth_classes"), i = e("cards"), r = e("model"), s = e("evt"), a = e("l10n!"), c = e("transition_end");
    return e("css!style/folder_cards"), [ e("./base")(e("template!./folder_picker.html")), {
        createdCallback: function() {
            if (console.log(this.localName + ".createdCallback, mode:" + this.mode), "folder" == this.mode) this.pickerTitle.setAttribute("data-l10n-id", "folders"), 
            this.bindContainerHandler(this.foldersContainer, "click", this.onClickFolder.bind(this)), 
            this.updateAccount = this.updateAccount.bind(this), r.latest("account", this.updateAccount); else if ("account" == this.mode) {
                this.pickerTitle.setAttribute("data-l10n-id", "opt-account"), r.latest("account", function(e) {
                    this.curAccount = e;
                }.bind(this)), this.bindContainerHandler(this.accountListContainer, "click", this.onClickAccount.bind(this));
                var e = r.getAccountCount();
                e > 1 && this.classList.remove("one-account"), this.acctsSlice = r.api.viewAccounts(!1), 
                this.acctsSlice.onsplice = this.onAccountsSplice.bind(this), this.acctsSlice.onchange = this.onAccountsChange.bind(this);
            }
            c(this, this.onTransitionEnd.bind(this));
        },
        onArgs: function(e) {
            this.folderPickCard = e.previousCard;
        },
        extraClasses: [ "anim-vertical", "anim-overlay", "one-account" ],
        onShowSettings: function() {
            i.pushCard("settings_main", "animate");
        },
        updateAccount: function(e) {
            var t = this.curAccount;
            this.mostRecentSyncTimestamp = 0, t !== e && (this.foldersContainer.innerHTML = "", 
            r.latestOnce("folder", function(t) {
                this.curAccount = e, this.curFolder || (this.curFolder = t), this.foldersSlice && (this.foldersSlice.onsplice = null, 
                this.foldersSlice.onchange = null), this.foldersSlice = r.foldersSlice, e.enabled && this.foldersSlice.syncList(e.id), 
                this.onFoldersSplice(0, 0, this.foldersSlice.items, !0, !1), this.foldersSlice.onsplice = this.onFoldersSplice.bind(this);
            }.bind(this)));
        },
        onClickAccount: function(e) {
            this.curAccount = e.account, this._waitingAccountId = this.curAccount.id, this._closeCard();
        },
        onAccountsSplice: function(e, t, o) {
            var i = this.accountListContainer, r = this.acctsSlice.items.length + o.length - t;
            this.classList.toggle("one-account", 1 >= r);
            var s;
            if (t) for (var a = e + t - 1; a >= e; a--) s = this.acctsSlice.items[a], s.element && i.removeChild(s.element);
            var c = e >= i.childElementCount ? null : i.children[e];
            o.forEach(function(e) {
                var t = e.element = n.cloneNode(!0);
                t.account = e, this.updateAccountDom(e, !0), i.insertBefore(t, c);
            }.bind(this));
        },
        onAccountsChange: function(e) {
            this.updateAccountDom(e, !1);
        },
        updateAccountDom: function(e, t) {
            var n = e.element;
            t && (n.querySelector(".fld-account-name").textContent = e.name, n.querySelector(".fld-account-label").innerHTML = e.label, 
            this.curAccount && this.curAccount.id === e.id && (n.classList.add("fld-account-selected"), 
            n.querySelector("input").setAttribute("checked", !0)));
        },
        onFoldersSplice: function(e, n, o) {
            var i, r = this.foldersContainer;
            if (n) for (var s = e + n - 1; s >= e; s--) i = this.foldersSlice.items[s], r.removeChild(i.element);
            var a = e >= r.childElementCount ? null : r.children[e], c = this;
            o.forEach(function(e) {
                if ("outbox" !== e.type || e.unread > 0) {
                    var n = e.element = t.cloneNode(!0);
                    n.folder = e, c.updateFolderDom(e, !0), r.insertBefore(n, a);
                }
            }), this.setNavigationMap();
        },
        updateFolderDom: function(e, t) {
            var n = e.element;
            if (t) {
                e.selectable || n.classList.add("fld-folder-unselectable");
                var i = Math.min(o.length - 1, e.depth);
                n.classList.add(o[i]), i > 0 && n.classList.add("fld-folder-depthnonzero"), n.querySelector(".fld-folder-name").textContent = e.name, 
                e.unread > 0 && (n.querySelector(".fld-folder-unread").textContent = "(" + e.unread + ")"), 
                n.dataset.type = e.type;
            }
            e === this.curFolder ? n.classList.add("fld-folder-selected") : n.classList.remove("fld-folder-selected");
        },
        onClickFolder: function(e) {
            var t = e.folder;
            if (t.selectable) {
                var n = this.curFolder;
                this.curFolder = t, this.updateFolderDom(n), this.updateFolderDom(t), this._showFolder(t), 
                this._closeCard();
            }
        },
        onTransitionEnd: function(e) {
            var t = function() {
                var e = a.get("toaster-switch-account");
                e += this.curAccount.label ? this.curAccount.label : this.curAccount.name, Toaster.showToast({
                    message: e,
                    latency: 2e3
                });
            }.bind(this);
            !this.classList.contains("opened") && e.target.classList.contains("fld-content") && (i.removeCardAndSuccessors(this, "animate"), 
            this.folderPickCard && this.folderShown === !0 && (this.folderPickCard.onBack(), 
            this.folderPickCard = null), this.folderShown = !1, this._waitingAccountId && (r.changeAccountFromId(this._waitingAccountId, function() {
                r.selectInbox(t);
            }), this._waitingAccountId = null));
        },
        _closeCard: function() {
            s.emit("folderPickerClosing"), this.classList.remove("opened");
        },
        _showFolder: function(e) {
            this.folderShown = !0, r.changeFolder(e);
        },
        onCardVisible: function(e) {
            this.classList.add("opened"), this.setNavigationMap(e), this.addEventListener("keydown", this.handleKeydown);
        },
        setNavigationMap: function(e) {
            const t = this.localName, n = "folder" === this.mode ? ".fld-folder-item" : ".fld-account-item", o = t + " " + n;
            "forward" !== e && e ? "back" === e && (this.initOption(), NavigationMap.setCurrentControl(o), 
            NavigationMap.setFocus("restore")) : (this.initOption(), NavigationMap.navSetup(t, n), 
            NavigationMap.setCurrentControl(o), NavigationMap.setFocus("first"));
        },
        die: function() {
            this.acctsSlice && this.acctsSlice.die(), r.removeListener("account", this.updateAccount), 
            this.removeListener("keydown", this.handleKeydown);
        },
        mode: "folder",
        optCancel: null,
        optSave: null,
        initOption: function() {
            this.optCancel = {
                name: "Cancel",
                l10nId: "cancel",
                priority: 1,
                method: this._closeCard.bind(this)
            }, this.optSave = {
                name: "Select",
                l10nId: "select",
                priority: 2,
                method: null
            };
            var e = [ this.optCancel, this.optSave ];
            NavigationMap.setSoftKeyBar(e);
        },
        onSelectFocused: function() {
            var e = NavigationMap.getCurrentItem();
            e && e.click();
        },
        onHidden: function() {
            this.removeListener("keydown", this.handleKeydown);
        },
        handleKeydown: function(e) {
            switch (e.key) {
              case "Backspace":
                e.preventDefault(), window.backToShowFolder && "cards-account-picker" !== this.localName ? this.foldersContainer.childNodes[0].click() : this._closeCard();
            }
        }
    } ];
});