var jsdom = require("jsdom");

module.exports = require("./lib/bleach.js"), module.exports.documentConstructor = jsdom.jsdom, 
module.exports._preCleanNodeHack = function(e, t) {
    "" === e.innerHTML && t.match(/<!--/) && (e.innerHTML = t + "-->");
};