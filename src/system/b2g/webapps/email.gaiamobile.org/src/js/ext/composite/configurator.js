define("errbackoff", [ "./date", "logic", "module", "exports" ], function(e, t, n, o) {
    function r(e, n) {
        this.state = "healthy", this._iNextBackoff = 0, t.defineScope(this, "BackoffEndpoint", {
            name: e
        }), t(this, "state", {
            state: this.state
        }), this._badResources = {}, this.listener = n;
    }
    var s = o.BACKOFF_DURATIONS = [ {
        fixedMS: 0,
        randomMS: 0
    }, {
        fixedMS: 800,
        randomMS: 400
    }, {
        fixedMS: 4500,
        randomMS: 1e3
    } ], i = window.setTimeout.bind(window);
    o.TEST_useTimeoutFunc = function(e) {
        i = e;
        for (var t = 0; t < s.length; t++) s[t].randomMS = 0;
    }, r.prototype = {
        _setState: function(e) {
            this.state !== e && (this.state = e, t(this, "state", {
                state: e
            }), this.listener && this.listener.onEndpointStateChange(e));
        },
        noteConnectSuccess: function() {
            this._setState("healthy"), this._iNextBackoff = 0;
        },
        noteConnectFailureMaybeRetry: function(e) {
            return t(this, "connectFailure", {
                reachable: e
            }), "shutdown" === this.state ? !1 : e ? (this._setState("broken"), !1) : (this._iNextBackoff > 0 && this._setState(e ? "broken" : "unreachable"), 
            this._iNextBackoff >= s.length ? !1 : !0);
        },
        noteBrokenConnection: function() {
            t(this, "connectFailure", {
                reachable: !0
            }), this._setState("broken"), this._iNextBackoff = s.length;
        },
        scheduleConnectAttempt: function(e) {
            if ("shutdown" !== this.state) {
                if (this._iNextBackoff >= s.length) return e(), void 0;
                var t = s[this._iNextBackoff++], n = t.fixedMS + Math.floor(Math.random() * t.randomMS);
                i(e, n);
            }
        },
        noteBadResource: function(t) {
            var n = e.NOW();
            if (this._badResources.hasOwnProperty(t)) {
                var o = this._badResources[t];
                o.count++, o.last = n;
            } else this._badResources[t] = {
                count: 1,
                last: n
            };
        },
        resourceIsOkayToUse: function(t) {
            return this._badResources.hasOwnProperty(t) ? (this._badResources[t], e.NOW(), void 0) : !0;
        },
        shutdown: function() {
            this._setState("shutdown");
        }
    }, o.createEndpoint = function(e, t) {
        return new r(e, t);
    };
}), define("composite/incoming", [ "logic", "../a64", "../accountmixins", "../mailslice", "../searchfilter", "../util", "../db/folder_info_rep", "require", "exports" ], function(e, t, n, o, r, s, i, a, c) {
    function d(e, t) {
        return e.path.localeCompare(t.path);
    }
    function u(e, t, n, r, s, i, a, c) {
        this.universe = t, this.compositeAccount = n, this.id = r, this.accountDef = n.accountDef, 
        this.enabled = !0, this._alive = !0, this._credentials = s, this._connInfo = i, 
        this._db = c;
        var d = this._folderStorages = {}, u = this.folders = [];
        this.FolderSyncer = e, this._deadFolderIds = null, this._folderInfos = a, this.meta = this._folderInfos.$meta, 
        this.mutations = this._folderInfos.$mutations;
        for (var l in a) if ("$" !== l[0]) {
            var h = a[l];
            d[l] = new o.FolderStorage(this, l, h, this._db, e), u.push(h.$meta);
        }
        this.folders.sort(function(e, t) {
            return e.path.localeCompare(t.path);
        });
        var p = this.getFirstFolderWithType("inbox");
        p || this._learnAboutFolder("INBOX", "INBOX", null, "inbox", "/", 0, !0);
    }
    var l = s.bsearchForInsert;
    c.CompositeIncomingAccount = u, u.prototype = {
        runOp: n.runOp,
        getFirstFolderWithType: n.getFirstFolderWithType,
        getFolderByPath: n.getFolderByPath,
        saveAccountState: n.saveAccountState,
        runAfterSaves: n.runAfterSaves,
        _learnAboutFolder: function(e, n, r, s, a, c, u) {
            var h = this.id + "/" + t.encodeInt(this.meta.nextFolderNum++), p = this._folderInfos[h] = {
                $meta: i.makeFolderMeta({
                    id: h,
                    name: e,
                    type: s,
                    path: n,
                    parentId: r,
                    delim: a,
                    depth: c,
                    lastSyncedAt: 0,
                    version: o.FOLDER_DB_VERSION
                }),
                $impl: {
                    nextId: 0,
                    nextHeaderBlock: 0,
                    nextBodyBlock: 0
                },
                accuracy: [],
                headerBlocks: [],
                bodyBlocks: [],
                serverIdHeaderBlockMapping: null
            };
            this._folderStorages[h] = new o.FolderStorage(this, h, p, this._db, this.FolderSyncer);
            var f = p.$meta, g = l(this.folders, f, d);
            return this.folders.splice(g, 0, f), u || this.universe.__notifyAddedFolder(this, f), 
            f;
        },
        _forgetFolder: function(e, t) {
            var n = this._folderInfos[e], o = n.$meta;
            delete this._folderInfos[e];
            var r = this._folderStorages[e];
            delete this._folderStorages[e];
            var s = this.folders.indexOf(o);
            this.folders.splice(s, 1), null === this._deadFolderIds && (this._deadFolderIds = []), 
            this._deadFolderIds.push(e), r.youAreDeadCleanupAfterYourself(), t || this.universe.__notifyRemovedFolder(this, o);
        },
        _recreateFolder: function(t, n) {
            e(this, "recreateFolder", {
                folderId: t
            });
            var r = this._folderInfos[t];
            r.$impl = {
                nextId: 0,
                nextHeaderBlock: 0,
                nextBodyBlock: 0
            }, r.accuracy = [], r.headerBlocks = [], r.bodyBlocks = [], null === this._deadFolderIds && (this._deadFolderIds = []), 
            this._deadFolderIds.push(t);
            var s = this;
            this.saveAccountState(null, function() {
                var e = new o.FolderStorage(s, t, r, s._db, s.FolderSyncer);
                for (var i in Iterator(s._folderStorages[t]._slices)) {
                    var a = i[1];
                    a._storage = e, a.reset(), e.sliceOpenMostRecent(a);
                }
                s._folderStorages[t]._slices = [], s._folderStorages[t] = e, n(e);
            }, "recreateFolder");
        },
        __checkpointSyncCompleted: function(e, t) {
            this.saveAccountState(null, e, t || "checkpointSync");
        },
        deleteFolder: function(t, n) {
            function o(e) {
                a = e, a.delBox(i.path, r);
            }
            function r(e) {
                e ? s("unknown") : s(null);
            }
            function s(o) {
                a && (c.__folderDoneWithConnection(a, !1, !1), a = null), o || (e(c, "deleteFolder", {
                    path: i.path
                }), c._forgetFolder(t)), n && n(o, i);
            }
            if (!this._folderInfos.hasOwnProperty(t)) throw new Error("No such folder: " + t);
            if (!this.universe.online) return n && n("offline"), void 0;
            var i = this._folderInfos[t].$meta, a = null, c = this;
            this.__folderDemandsConnection(null, "deleteFolder", o);
        },
        getFolderStorageForFolderId: function(e) {
            if (this._folderStorages.hasOwnProperty(e)) return this._folderStorages[e];
            throw new Error("No folder with id: " + e);
        },
        getFolderStorageForMessageSuid: function(e) {
            var t = e.substring(0, e.lastIndexOf("/"));
            if (this._folderStorages.hasOwnProperty(t)) return this._folderStorages[t];
            throw new Error("No folder with id: " + t);
        },
        getFolderMetaForFolderId: function(e) {
            return this._folderInfos.hasOwnProperty(e) ? this._folderInfos[e].$meta : null;
        },
        sliceFolderMessages: function(e, t) {
            var n = this._folderStorages[e], r = new o.MailSlice(t, n);
            n.sliceOpenMostRecent(r);
        },
        sortFolderMessages: function(e, t, n) {
            var r = this._folderStorages[e], s = new o.MailSlice(t, r, n);
            r.sliceOpenMostRecent(s);
        },
        searchFolderMessages: function(e, t, n, o) {
            var s = this._folderStorages[e], i = new r.SearchSlice(t, s, n, o);
            return s.sliceOpenSearch(i), i;
        },
        shutdownFolders: function() {
            for (var e = 0; e < this.folders.length; e++) {
                var t = this.folders[e], n = this._folderStorages[t.id];
                n.shutdown();
            }
        },
        scheduleMessagePurge: function(e, t) {
            this.universe.purgeExcessMessages(this.compositeAccount, e, t);
        },
        onEndpointStateChange: function(e) {
            switch (e) {
              case "healthy":
                this.universe.__removeAccountProblem(this.compositeAccount, "connection", "incoming");
                break;

              case "unreachable":
              case "broken":
                this.universe.__reportAccountProblem(this.compositeAccount, "connection", "incoming");
            }
        }
    };
}), define("imap/folder", [ "logic", "../a64", "../allback", "../date", "../syncbase", "../util", "module", "require", "exports" ], function(e, t, n, o, r, s, i, a, c) {
    function d(t, n) {
        this._account = t, this._storage = n, e.defineScope(this, "ImapFolderConn", {
            accountId: t.id,
            folderId: n.folderId
        }), this._conn = null, this.box = null, this._deathback = null;
    }
    function u(t, n) {
        this._account = t, this.folderStorage = n, e.defineScope(this, "ImapFolderSyncer", {
            accountId: t.id,
            folderId: n.folderId
        }), this._syncSlice = null, this._curSyncAccuracyStamp = null, this._curSyncDir = 1, 
        this._curSyncIsGrow = null, this._nextSyncAnchorTS = null, this._fallbackOriginTS = null, 
        this._syncThroughTS = null, this._curSyncDayStep = null, this._curSyncDoNotGrowBoundary = null, 
        this._curSyncDoneCallback = null, this.folderConn = new d(t, n);
    }
    function l() {}
    var h = null, p = null, f = null, g = null, m = null, y = (n.allbackMaker, s.bsearchForInsert, 
    s.bsearchMaybeExists, s.cmpHeaderYoungToOld, o.DAY_MILLIS), _ = o.NOW, v = (o.BEFORE, 
    o.ON_OR_BEFORE, o.SINCE), b = o.TIME_DIR_AT_OR_BEYOND, S = (o.TIME_DIR_ADD, o.TIME_DIR_DELTA, 
    o.makeDaysAgo), T = o.makeDaysBefore, w = o.quantizeDate, I = 1, E = -1, C = Math.pow(2, 32) - 1;
    d.prototype = {
        acquireConn: function(e, t, n, o) {
            var r = this;
            this._deathback = t, this._account.__folderDemandsConnection(this._storage.folderId, n, function(t) {
                r._conn = t, r._conn.selectMailbox(r._storage.folderMeta.path, function(t, n) {
                    if (t) {
                        if (console.error("Problem entering folder", r._storage.folderMeta.path), r._conn = null, 
                        r._account.__folderDoneWithConnection(r._conn, !1, !0), r._deathback) {
                            var o = r._deathback;
                            r.clearErrorHandler(), o();
                        }
                    } else r.box = n, e(r, r._storage);
                });
            }, function() {
                if (r._conn = null, r._deathback) {
                    var e = r._deathback;
                    r.clearErrorHandler(), e();
                }
            }, o);
        },
        relinquishConn: function() {
            this._conn && (this.clearErrorHandler(), this._account.__folderDoneWithConnection(this._conn, !0, !1), 
            this._conn = null);
        },
        withConnection: function(e, t, n, o) {
            return this._conn ? (this._deathback = t, e(this), void 0) : (this.acquireConn(function() {
                this.withConnection(e, t, n);
            }.bind(this), t, n, o), void 0);
        },
        clearErrorHandler: function() {
            this._deathback = null;
        },
        reselectBox: function(e) {
            this._conn.selectMailbox(this._storage.folderMeta.path, e);
        },
        _timelySyncSearch: function(e, t, n, o, r) {
            var s = !1;
            if (!this._conn) return this.acquireConn(this._timelySyncSearch.bind(this, e, t, n, o, r), n, "sync", !0), 
            void 0;
            if (!r) {
                var i = n;
                n = function() {
                    s ? i() : (console.warn("Broken connection for SEARCH. Retrying."), this._timelySyncSearch(e, t, i, o, !0));
                }.bind(this);
            }
            this._deathback = n, o && o(.1), this._account.isGmail && this._conn.exec("NOOP"), 
            this._conn.search(e, {
                byUid: !0
            }, function(o, r) {
                return s = !0, o ? (console.error("Search error on", e, "err:", o), n(), void 0) : (t(r), 
                void 0);
            });
        },
        syncDateRange: function() {
            var e = Array.slice(arguments), t = this;
            a([ "imap/protocol/sync" ], function(n) {
                m = n, (t.syncDateRange = t._lazySyncDateRange).apply(t, e);
            });
        },
        _lazySyncDateRange: function(t, o, s, i, a) {
            var c = e.subscope(this, {
                startTS: t,
                endTS: o
            });
            if (t && o && v(t, o)) return e(c, "illegalSync"), i("invariant"), void 0;
            var d = this, u = d._storage, l = !1;
            console.log("syncDateRange:", t, o), e(c, "syncDateRange_begin");
            var h = {
                not: {
                    deleted: !0
                }
            };
            t && (h.since = new Date(t)), o && (h.before = new Date(o));
            var p = new Promise(function(t) {
                this._timelySyncSearch(h, t, function() {
                    l || (l = !0, e(c, "syncDateRange_end", {
                        full: 0,
                        flags: 0,
                        deleted: 0
                    }), i("aborted"));
                }.bind(this), a, !1);
            }.bind(this)), f = t ? t - r.IMAP_SEARCH_AMBIGUITY_MS : null, g = o ? o + r.IMAP_SEARCH_AMBIGUITY_MS : null;
            e(c, "database-lookup", {
                dbStartTS: f,
                dbEndTS: g
            });
            var b = new Promise(function(e) {
                u.getAllMessagesInImapDateRange(f, g, e);
            });
            Promise.all([ p, b ]).then(function(h) {
                var p = h[0], f = h[1], g = o || w(_() + y), v = Math.round((g - t) / y), b = p.length > r.BISECT_DATE_AT_N_MESSAGES && v > 1;
                if (console.log("[syncDateRange]", "Should bisect?", b ? "***YES, BISECT!***" : "no.", "curDaysDelta =", v, "serverUIDs.length =", p.length), 
                b) {
                    e(c, "syncDateRange_end");
                    var S = {
                        oldStartTS: t,
                        oldEndTS: o,
                        numHeaders: p.length,
                        curDaysDelta: v,
                        newStartTS: t,
                        newEndTS: o
                    };
                    return "abort" === i("bisect", S, null) ? (d.clearErrorHandler(), i("bisect-aborted", null)) : d.syncDateRange(S.newStartTS, S.newEndTS, s, i, a), 
                    void 0;
                }
                a && a(.25);
                var T = new Set(), I = new Set(), E = {};
                f.forEach(function(e) {
                    null !== e.srvid && (T.add(e.srvid), E[e.srvid] = e);
                }), p.forEach(function(e) {
                    T.add(e), I.add(e);
                });
                var C = {
                    connection: d._conn,
                    storage: u,
                    newUIDs: [],
                    knownUIDs: [],
                    knownHeaders: []
                }, A = 0, k = n.latch();
                T.forEach(function(n) {
                    var s = E[n] || null, i = I.has(n);
                    if (!s && i) C.newUIDs.push(n), e(c, "new-uid", {
                        uid: n
                    }); else if (s && i) C.knownUIDs.push(n), C.knownHeaders.push(s), s.imapMissingInSyncRange && (s.imapMissingInSyncRange = null, 
                    e(c, "found-missing-uid", {
                        uid: n
                    }), u.updateMessageHeader(s.date, s.id, !0, s, null, k.defer(), {
                        silent: !0
                    })), e(c, "updated-uid", {
                        uid: n
                    }); else if (s && !i) {
                        var a, d = r.IMAP_SEARCH_AMBIGUITY_MS, l = s.date;
                        !s.imapMissingInSyncRange || s.imapMissingInSyncRange.endTS < t || s.imapMissingInSyncRange.startTS > o ? a = s.imapMissingInSyncRange = {
                            startTS: t || 0,
                            endTS: o || 1/0
                        } : (a = s.imapMissingInSyncRange, a.startTS = Math.min(t || 0, a.startTS || 0), 
                        a.endTS = Math.max(o || 1/0, a.endTS || 1/0)), a.startTS <= l - d && a.endTS >= l + d ? (e(c, "unambiguously-deleted-uid", {
                            uid: n,
                            date: l,
                            fuzz: d,
                            missingRange: a
                        }), u.deleteMessageHeaderAndBodyUsingHeader(s), A++) : (e(c, "ambiguously-missing-uid", {
                            uid: n,
                            missingRange: a,
                            rangeToDelete: {
                                startTS: l - d,
                                endTS: l + d
                            },
                            syncRange: {
                                startTS: t,
                                endTS: o
                            }
                        }), u.updateMessageHeader(s.date, s.id, !0, s, null, k.defer(), {
                            silent: !0
                        }));
                    }
                }), k.then(function() {
                    var n = new m.Sync(C);
                    n.onprogress = a, n.oncomplete = function(n, r) {
                        e(c, "syncDateRange_end", {
                            full: n,
                            flags: r,
                            deleted: A
                        });
                        var a = (d.box.highestModseq || "") + "";
                        u.markSyncRange(t, o, a, s), l || (l = !0, d.clearErrorHandler(), i(null, null, n + r, t, o));
                    };
                });
            }.bind(this));
        },
        downloadBodyReps: function() {
            var e = Array.slice(arguments), t = this;
            a([ "./imapchew", "./protocol/bodyfetcher", "./protocol/textparser", "./protocol/snippetparser" ], function(n, o, r, s) {
                g = n, f = o, h = r, p = s, (t.downloadBodyReps = t._lazyDownloadBodyReps).apply(t, e);
            });
        },
        _lazyDownloadBodyReps: function(e, t, o) {
            "function" == typeof t && (o = t, t = null), t = t || {};
            var r = this, s = function(s) {
                var i = g.selectSnippetBodyRep(e, s), a = t.maximumBytesToFetch, c = h.TextParser, d = [], u = n.latch();
                if (s.bodyReps.forEach(function(t, n) {
                    if (!t.isDownloaded) {
                        var o = Math.min(5 * t.sizeEstimate, C);
                        if (void 0 !== a) {
                            if (c = p.SnippetParser, 0 >= a) return;
                            t.sizeEstimate > a && (o = a), a -= t.sizeEstimate;
                        }
                        0 >= o && (o = 64);
                        var r = {
                            uid: e.srvid,
                            partInfo: t._partInfo,
                            bodyRepIndex: n,
                            createSnippet: n === i,
                            headerUpdatedCallback: u.defer(e.srvid + "-" + t._partInfo)
                        };
                        (void 0 !== a || t.amountDownloaded) && (r.bytes = [ t.amountDownloaded, o ]), d.push(r);
                    }
                }), !d.length) return o(null, s), void 0;
                var l = new f.BodyFetcher(r._conn, c, d);
                r._handleBodyFetcher(l, e, s, u.defer("body")), u.then(function(e) {
                    o(n.extractErrFromCallbackArgs(e), s);
                });
            };
            this._storage.getMessageBody(e.suid, e.date, s);
        },
        _handleBodyFetcher: function(e, t, n, o) {
            var r = {
                changeDetails: {
                    bodyReps: []
                }
            };
            e.onparsed = function(e, o, s) {
                return e ? (o.headerUpdatedCallback(e), void 0) : (g.updateMessageWithFetch(t, n, o, s), 
                t.bytesToDownloadForBodyDisplay = g.calculateBytesToDownloadForImapBodyDisplay(n), 
                this._storage.updateMessageHeader(t.date, t.id, !1, t, n, o.headerUpdatedCallback.bind(null, null)), 
                r.changeDetails.bodyReps.push(o.bodyRepIndex), void 0);
            }.bind(this), e.onend = function() {
                this._storage.updateMessageBody(t, n, {}, r, o.bind(null, null));
            }.bind(this);
        },
        _lazyDownloadBodies: function(e, t, o) {
            for (var r = 0, s = n.latch(), i = 0; i < e.length; i++) {
                var a = e[i];
                a && null === a.snippet && (r++, this.downloadBodyReps(e[i], t, s.defer(a.suid)));
            }
            s.then(function(e) {
                o(n.extractErrFromCallbackArgs(e), r);
            });
        },
        downloadBodies: function() {
            var e = Array.slice(arguments), t = this;
            a([ "./imapchew", "./protocol/bodyfetcher", "./protocol/snippetparser" ], function(n, o, r) {
                g = n, f = o, p = r, (t.downloadBodies = t._lazyDownloadBodies).apply(t, e);
            });
        },
        downloadMessageAttachments: function(e, t, o) {
            a([ "mimeparser" ], function(r) {
                var s = this._conn, i = n.latch(), a = null, c = [];
                t.forEach(function(t, n) {
                    var o = "body.peek[" + t.part + "]", d = i.defer(t.part);
                    s.listMessages(e, [ o ], {
                        byUid: !0
                    }, function(e, s) {
                        if (e) return a = e, console.error("attachments:download-error", {
                            error: e,
                            part: t.part,
                            type: t.type
                        }), d(), void 0;
                        var i, u = s[0];
                        for (var l in u) if (/body\[/.test(l)) {
                            i = u[l];
                            break;
                        }
                        if (!i) return console.error("attachments:download-error", {
                            error: "no body part?",
                            requestedPart: o,
                            responseKeys: Object.keys(u)
                        }), d(), void 0;
                        var h = new r();
                        h.write("Content-Type: " + t.type + "\r\n"), h.write("Content-Transfer-Encoding: " + t.encoding + "\r\n"), 
                        h.write("\r\n"), h.write(i), h.end();
                        var p = h.node;
                        c[n] = new Blob([ p.content ], {
                            type: p.contentType.value
                        }), d();
                    });
                }), i.then(function() {
                    o(a, c);
                });
            }.bind(this));
        },
        shutdown: function() {}
    }, c.ImapFolderSyncer = u, u.prototype = {
        syncable: !0,
        get canGrowSync() {
            return !this.folderStorage.isLocalOnly;
        },
        initialSync: function(e, t, n, o, s) {
            n("sync", !1), this.folderConn.withConnection(function(n) {
                var i = !1;
                n && n.box && n.box.exists < r.SYNC_WHOLE_FOLDER_AT_N_MESSAGES && (i = !0), this._startSync(e, I, "grow", null, r.OLDEST_SYNC_DATE, null, i ? null : t, o, s);
            }.bind(this), function() {
                o("aborted");
            }, "initialSync", !0);
        },
        refreshSync: function(e, t, n, o, r, s, i) {
            this._startSync(e, t, "refresh", t === I ? o : n, t === I ? n : o, r, null, s, i);
        },
        growSync: function(e, t, n, o, s, i) {
            var a;
            a = t === I ? r.OLDEST_SYNC_DATE : null, this._startSync(e, t, "grow", n, a, null, o, s, i);
        },
        _startSync: function(e, t, n, o, r, s, i, a, c) {
            var d, u;
            this._syncSlice = e, this._curSyncAccuracyStamp = _(), this._curSyncDir = t, this._curSyncIsGrow = "grow" === n, 
            this._fallbackOriginTS = s, t === I ? (u = o, i ? this._nextSyncAnchorTS = d = u ? u - i * y : S(i) : (d = r, 
            this._nextSyncAnchorTS = null)) : (d = o, i ? this._nextSyncAnchorTS = u = d + i * y : (u = r, 
            this._nextSyncAnchorTS = null)), this._syncThroughTS = r, this._curSyncDayStep = i, 
            this._curSyncDoNotGrowBoundary = null, this._curSyncDoneCallback = a, this.folderConn.syncDateRange(d, u, this._curSyncAccuracyStamp, this.onSyncCompleted.bind(this), c);
        },
        _doneSync: function(e) {
            this._syncSlice.desiredHeaders = this._syncSlice.headers.length, this._account.__checkpointSyncCompleted(function() {
                this._curSyncDoneCallback && this._curSyncDoneCallback(e), this._syncSlice = null, 
                this._curSyncAccuracyStamp = null, this._curSyncDir = null, this._nextSyncAnchorTS = null, 
                this._syncThroughTS = null, this._curSyncDayStep = null, this._curSyncDoNotGrowBoundary = null, 
                this._curSyncDoneCallback = null;
            }.bind(this));
        },
        onSyncCompleted: function(e, t, n, o, s) {
            if ("bisect" === e) {
                var i = t.curDaysDelta, a = t.numHeaders;
                if (this._curSyncDir === E && this._fallbackOriginTS) {
                    this.folderStorage.clearSyncedToDawnOfTime(this._fallbackOriginTS), t.oldStartTS = this._fallbackOriginTS, 
                    this._fallbackOriginTS = null;
                    var c = t.oldEndTS || w(_() + y);
                    i = Math.round((c - t.oldStartTS) / y), a = 1.5 * r.BISECT_DATE_AT_N_MESSAGES;
                } else i > 1e3 && (i = 30);
                var d = r.BISECT_DATE_AT_N_MESSAGES / (2 * a), u = Math.max(1, Math.min(i - 2, Math.ceil(d * i)));
                return this._curSyncDayStep = u, this._curSyncDir === I ? (t.newEndTS = t.oldEndTS, 
                this._nextSyncAnchorTS = t.newStartTS = T(t.newEndTS, u), this._curSyncDoNotGrowBoundary = t.oldStartTS) : (t.newStartTS = t.oldStartTS, 
                this._nextSyncAnchorTS = t.newEndTS = T(t.newStartTS, -u), this._curSyncDoNotGrowBoundary = t.oldEndTS), 
                void 0;
            }
            if (e) return this._doneSync(e), void 0;
            if (console.log("Sync Completed!", this._curSyncDayStep, "days", n, "messages synced"), 
            this._syncSlice.isDead) return this._doneSync(), void 0;
            var l = this.folderConn && this.folderConn.box && this.folderConn.box.exists, h = this.folderStorage.getKnownMessageCount(), p = this._curSyncDir === I ? o : s;
            if (console.log("folder message count", l, "dbCount", h, "syncedThrough", p, "oldest known", this.folderStorage.getOldestMessageTimestamp()), 
            this._curSyncDir === I && l === h && (!l || b(this._curSyncDir, p, this.folderStorage.getOldestMessageTimestamp()))) return this.folderStorage.markSyncedToDawnOfTime(), 
            this._doneSync(), void 0;
            if (!this._nextSyncAnchorTS || b(this._curSyncDir, this._nextSyncAnchorTS, this._syncThroughTS)) return this._doneSync(), 
            void 0;
            if (this._curSyncIsGrow && this._syncSlice.headers.length >= this._syncSlice.desiredHeaders) return console.log("SYNCDONE Enough headers retrieved.", "have", this._syncSlice.headers.length, "want", this._syncSlice.desiredHeaders, "conn knows about", this.folderConn.box.exists, "sync date", this._curSyncStartTS, "[oldest defined as", r.OLDEST_SYNC_DATE, "]"), 
            this._doneSync(), void 0;
            var f, g;
            n || null !== this._curSyncDoNotGrowBoundary && !b(this._curSyncDir, this._nextSyncAnchorTS, this._curSyncDoNotGrowBoundary) ? f = this._curSyncDayStep : (this._curSyncDoNotGrowBoundary = null, 
            g = (w(_()) - this._nextSyncAnchorTS) / y, f = Math.ceil(this._curSyncDayStep * r.TIME_SCALE_FACTOR_ON_NO_MESSAGES), 
            180 > g ? f > 45 && (f = 45) : 365 > g ? f > 90 && (f = 90) : 730 > g ? f > 120 && (f = 120) : 1825 > g ? f > 180 && (f = 180) : 3650 > g ? f > 365 && (f = 365) : f > 730 && (f = 730), 
            this._curSyncDayStep = f);
            var m, v;
            this._curSyncDir === I ? (v = this._nextSyncAnchorTS, this._nextSyncAnchorTS = m = T(v, f)) : (m = this._nextSyncAnchorTS, 
            this._nextSyncAnchorTS = v = T(m, -f), null === this._syncThroughTS && b(this._curSyncDir, this._nextSyncAnchorTS, this._syncThroughTS) && (v = null)), 
            this.folderConn.syncDateRange(m, v, this._curSyncAccuracyStamp, this.onSyncCompleted.bind(this));
        },
        allConsumersDead: function() {
            this.folderConn.relinquishConn();
        },
        shutdown: function() {
            this.folderConn.shutdown();
        }
    }, l.prototype = {};
}), function(e, t) {
    "function" == typeof define && define.amd ? define("mimeparser-tzabbr", t) : "object" == typeof exports ? module.exports = t() : e.tzabbr = t();
}(this, function() {
    return {
        ACDT: "+1030",
        ACST: "+0930",
        ACT: "+0800",
        ADT: "-0300",
        AEDT: "+1100",
        AEST: "+1000",
        AFT: "+0430",
        AKDT: "-0800",
        AKST: "-0900",
        AMST: "-0300",
        AMT: "+0400",
        ART: "-0300",
        AST: "+0300",
        AWDT: "+0900",
        AWST: "+0800",
        AZOST: "-0100",
        AZT: "+0400",
        BDT: "+0800",
        BIOT: "+0600",
        BIT: "-1200",
        BOT: "-0400",
        BRT: "-0300",
        BST: "+0600",
        BTT: "+0600",
        CAT: "+0200",
        CCT: "+0630",
        CDT: "-0500",
        CEDT: "+0200",
        CEST: "+0200",
        CET: "+0100",
        CHADT: "+1345",
        CHAST: "+1245",
        CHOT: "+0800",
        CHST: "+1000",
        CHUT: "+1000",
        CIST: "-0800",
        CIT: "+0800",
        CKT: "-1000",
        CLST: "-0300",
        CLT: "-0400",
        COST: "-0400",
        COT: "-0500",
        CST: "-0600",
        CT: "+0800",
        CVT: "-0100",
        CWST: "+0845",
        CXT: "+0700",
        DAVT: "+0700",
        DDUT: "+1000",
        DFT: "+0100",
        EASST: "-0500",
        EAST: "-0600",
        EAT: "+0300",
        ECT: "-0500",
        EDT: "-0400",
        EEDT: "+0300",
        EEST: "+0300",
        EET: "+0200",
        EGST: "+0000",
        EGT: "-0100",
        EIT: "+0900",
        EST: "-0500",
        FET: "+0300",
        FJT: "+1200",
        FKST: "-0300",
        FKT: "-0400",
        FNT: "-0200",
        GALT: "-0600",
        GAMT: "-0900",
        GET: "+0400",
        GFT: "-0300",
        GILT: "+1200",
        GIT: "-0900",
        GMT: "+0000",
        GST: "+0400",
        GYT: "-0400",
        HADT: "-0900",
        HAEC: "+0200",
        HAST: "-1000",
        HKT: "+0800",
        HMT: "+0500",
        HOVT: "+0700",
        HST: "-1000",
        ICT: "+0700",
        IDT: "+0300",
        IOT: "+0300",
        IRDT: "+0430",
        IRKT: "+0900",
        IRST: "+0330",
        IST: "+0530",
        JST: "+0900",
        KGT: "+0600",
        KOST: "+1100",
        KRAT: "+0700",
        KST: "+0900",
        LHST: "+1030",
        LINT: "+1400",
        MAGT: "+1200",
        MART: "-0930",
        MAWT: "+0500",
        MDT: "-0600",
        MET: "+0100",
        MEST: "+0200",
        MHT: "+1200",
        MIST: "+1100",
        MIT: "-0930",
        MMT: "+0630",
        MSK: "+0400",
        MST: "-0700",
        MUT: "+0400",
        MVT: "+0500",
        MYT: "+0800",
        NCT: "+1100",
        NDT: "-0230",
        NFT: "+1130",
        NPT: "+0545",
        NST: "-0330",
        NT: "-0330",
        NUT: "-1100",
        NZDT: "+1300",
        NZST: "+1200",
        OMST: "+0700",
        ORAT: "+0500",
        PDT: "-0700",
        PET: "-0500",
        PETT: "+1200",
        PGT: "+1000",
        PHOT: "+1300",
        PHT: "+0800",
        PKT: "+0500",
        PMDT: "-0200",
        PMST: "-0300",
        PONT: "+1100",
        PST: "-0800",
        PYST: "-0300",
        PYT: "-0400",
        RET: "+0400",
        ROTT: "-0300",
        SAKT: "+1100",
        SAMT: "+0400",
        SAST: "+0200",
        SBT: "+1100",
        SCT: "+0400",
        SGT: "+0800",
        SLST: "+0530",
        SRT: "-0300",
        SST: "+0800",
        SYOT: "+0300",
        TAHT: "-1000",
        THA: "+0700",
        TFT: "+0500",
        TJT: "+0500",
        TKT: "+1300",
        TLT: "+0900",
        TMT: "+0500",
        TOT: "+1300",
        TVT: "+1200",
        UCT: "+0000",
        ULAT: "+0800",
        UTC: "+0000",
        UYST: "-0200",
        UYT: "-0300",
        UZT: "+0500",
        VET: "-0430",
        VLAT: "+1000",
        VOLT: "+0400",
        VOST: "+0600",
        VUT: "+1100",
        WAKT: "+1200",
        WAST: "+0200",
        WAT: "+0100",
        WEDT: "+0100",
        WEST: "+0100",
        WET: "+0000",
        WST: "+0800",
        YAKT: "+1000",
        YEKT: "+0600",
        Z: "+0000"
    };
}), function(e, t) {
    "function" == typeof define && define.amd ? define("mimeparser", [ "mimefuncs", "addressparser", "mimeparser-tzabbr" ], t) : "object" == typeof exports ? module.exports = t(require("mimefuncs"), require("wo-addressparser"), require("./mimeparser-tzabbr")) : e.MimeParser = t(e.mimefuncs, e.addressparser, e.tzabbr);
}(this, function(e, t, n) {
    function o() {
        this.running = !0, this.nodes = {}, this.node = new r(null, this), this._remainder = "";
    }
    function r(e, t) {
        this.header = [], this.headers = {}, this.path = e ? e.path.concat(e._childNodes.length + 1) : [], 
        this._parser = t, this._parentNode = e, this._state = "HEADER", this._bodyBuffer = "", 
        this._lineCount = 0, this._childNodes = !1, this._currentChild = !1, this._lineRemainder = "", 
        this._isMultipart = !1, this._multipartBoundary = !1, this._isRfc822 = !1, this.raw = "", 
        this._parser.nodes["node" + this.path.join(".")] = this;
    }
    return o.prototype.write = function(t) {
        if (!t || !t.length) return !this.running;
        var n = (this._remainder + ("object" == typeof t ? e.fromTypedArray(t) : t)).split(/\r?\n/g);
        this._remainder = n.pop();
        for (var o = 0, r = n.length; r > o; o++) this.node.writeLine(n[o]);
        return !this.running;
    }, o.prototype.end = function(e) {
        e && e.length && this.write(e), (this.node._lineCount || this._remainder) && (this.node.writeLine(this._remainder), 
        this._remainder = ""), this.node && this.node.finalize(), this.onend();
    }, o.prototype.getNode = function(e) {
        return e = e || "", this.nodes["node" + e] || null;
    }, o.prototype.onend = function() {}, o.prototype.onheader = function() {}, o.prototype.onbody = function() {}, 
    r.prototype.writeLine = function(e) {
        this.raw += (this.raw ? "\n" : "") + e, "HEADER" === this._state ? this._processHeaderLine(e) : "BODY" === this._state && this._processBodyLine(e);
    }, r.prototype.finalize = function() {
        this._isRfc822 ? this._currentChild.finalize() : this._emitBody(!0);
    }, r.prototype._processHeaderLine = function(e) {
        return e ? (e.match(/^\s/) && this.header.length ? this.header[this.header.length - 1] += "\n" + e : this.header.push(e), 
        void 0) : (this._parseHeaders(), this._parser.onheader(this), this._state = "BODY", 
        void 0);
    }, r.prototype._parseHeaders = function() {
        for (var t, n, o, r = 0, s = this.header.length; s > r; r++) n = this.header[r].split(":"), 
        t = (n.shift() || "").trim().toLowerCase(), n = (n.join(":") || "").replace(/\n/g, "").trim(), 
        n.match(/[\u0080-\uFFFF]/) && (this.charset || (o = !0), n = e.charset.decode(e.charset.convert(e.toTypedArray(n), this.charset || "iso-8859-1"))), 
        this.headers[t] ? this.headers[t].push(this._parseHeaderValue(t, n)) : this.headers[t] = [ this._parseHeaderValue(t, n) ], 
        this.charset || "content-type" !== t || (this.charset = this.headers[t][this.headers[t].length - 1].params.charset), 
        o && this.charset && (o = !1, this.headers = {}, r = -1);
        this._processContentType(), this._processContentTransferEncoding();
    }, r.prototype._parseHeaderValue = function(n, o) {
        var r, s = !1;
        switch (n) {
          case "content-type":
          case "content-transfer-encoding":
          case "content-disposition":
          case "dkim-signature":
            r = e.parseHeaderValue(o);
            break;

          case "from":
          case "sender":
          case "to":
          case "reply-to":
          case "cc":
          case "bcc":
          case "abuse-reports-to":
          case "errors-to":
          case "return-path":
          case "delivered-to":
            s = !0, r = {
                value: [].concat(t.parse(o) || [])
            };
            break;

          case "date":
            r = {
                value: this._parseDate(o)
            };
            break;

          default:
            r = {
                value: o
            };
        }
        return r.initial = o, this._decodeHeaderCharset(r, {
            isAddress: s
        }), r;
    }, r.prototype._parseDate = function(e) {
        e = (e || "").toString().trim();
        var t = new Date(e);
        return this._isValidDate(t) ? t.toUTCString().replace(/GMT/, "+0000") : (e = e.replace(/\b[a-z]+$/i, function(e) {
            return e = e.toUpperCase(), n.hasOwnProperty(e) ? n[e] : e;
        }), t = new Date(e), this._isValidDate(t) ? t.toUTCString().replace(/GMT/, "+0000") : e);
    }, r.prototype._isValidDate = function(e) {
        return "[object Date]" === Object.prototype.toString.call(e) && "Invalid Date" !== e.toString();
    }, r.prototype._decodeHeaderCharset = function(t, n) {
        return n = n || {}, "string" == typeof t.value && (t.value = e.mimeWordsDecode(t.value)), 
        Object.keys(t.params || {}).forEach(function(n) {
            "string" == typeof t.params[n] && (t.params[n] = e.mimeWordsDecode(t.params[n]));
        }), n.isAddress && Array.isArray(t.value) && t.value.forEach(function(t) {
            t.name && (t.name = e.mimeWordsDecode(t.name), Array.isArray(t.group) && this._decodeHeaderCharset({
                value: t.group
            }, {
                isAddress: !0
            }));
        }.bind(this)), t;
    }, r.prototype._processContentType = function() {
        var t;
        this.contentType = this.headers["content-type"] && this.headers["content-type"][0] || e.parseHeaderValue("text/plain"), 
        this.contentType.value = (this.contentType.value || "").toLowerCase().trim(), this.contentType.type = this.contentType.value.split("/").shift() || "text", 
        this.contentType.params && this.contentType.params.charset && !this.charset && (this.charset = this.contentType.params.charset), 
        "multipart" === this.contentType.type && this.contentType.params.boundary && (this._childNodes = [], 
        this._isMultipart = this.contentType.value.split("/").pop() || "mixed", this._multipartBoundary = this.contentType.params.boundary), 
        "message/rfc822" === this.contentType.value && (t = this.headers["content-disposition"] && this.headers["content-disposition"][0] || e.parseHeaderValue(""), 
        "attachment" !== (t.value || "").toLowerCase().trim() && (this._childNodes = [], 
        this._currentChild = new r(this, this._parser), this._childNodes.push(this._currentChild), 
        this._isRfc822 = !0));
    }, r.prototype._processContentTransferEncoding = function() {
        this.contentTransferEncoding = this.headers["content-transfer-encoding"] && this.headers["content-transfer-encoding"][0] || e.parseHeaderValue("7bit"), 
        this.contentTransferEncoding.value = (this.contentTransferEncoding.value || "").toLowerCase().trim();
    }, r.prototype._processBodyLine = function(t) {
        var n, o;
        if (this._lineCount++, this._isMultipart) t === "--" + this._multipartBoundary ? (this._currentChild && this._currentChild.finalize(), 
        this._currentChild = new r(this, this._parser), this._childNodes.push(this._currentChild)) : t === "--" + this._multipartBoundary + "--" ? (this._currentChild && this._currentChild.finalize(), 
        this._currentChild = !1) : this._currentChild && this._currentChild.writeLine(t); else if (this._isRfc822) this._currentChild.writeLine(t); else switch (this.contentTransferEncoding.value) {
          case "base64":
            n = this._lineRemainder + t.trim(), n.length % 4 ? (this._lineRemainder = n.substr(-n.length % 4), 
            n = n.substr(0, n.length - this._lineRemainder.length)) : this._lineRemainder = "", 
            n.length && (this._bodyBuffer += e.fromTypedArray(e.base64.decode(n)));
            break;

          case "quoted-printable":
            n = this._lineRemainder + (this._lineCount > 1 ? "\n" : "") + t, (o = n.match(/=[a-f0-9]{0,1}$/i)) ? (this._lineRemainder = o[0], 
            n = n.substr(0, n.length - this._lineRemainder.length)) : this._lineRemainder = "", 
            this._bodyBuffer += n.replace(/\=(\r?\n|$)/g, "").replace(/=([a-f0-9]{2})/gi, function(e, t) {
                return String.fromCharCode(parseInt(t, 16));
            });
            break;

          default:
            this._bodyBuffer += (this._lineCount > 1 ? "\n" : "") + t;
        }
    }, r.prototype._emitBody = function() {
        var t, n = this.headers["content-disposition"] && this.headers["content-disposition"][0] || e.parseHeaderValue("");
        !this._isMultipart && this._bodyBuffer && (/^text\/(plain|html)$/i.test(this.contentType.value) && this.contentType.params && /^flowed$/i.test(this.contentType.params.format) && (t = /^yes$/i.test(this.contentType.params.delsp), 
        this._bodyBuffer = this._bodyBuffer.split("\n").reduce(function(e, n) {
            var o = e;
            return t && (o = o.replace(/[ ]+$/, "")), / $/.test(e) && !/(^|\n)\-\- $/.test(e) ? o + n : o + "\n" + n;
        }).replace(/^ /gm, "")), this.content = e.toTypedArray(this._bodyBuffer), /^text\/(plain|html)$/i.test(this.contentType.value) && !/^attachment$/i.test(n.value) && (!this.charset && /^text\/html$/i.test(this.contentType.value) && (this.charset = this._detectHTMLCharset(this._bodyBuffer)), 
        /^utf[\-_]?8$/i.test(this.charset) || (this.content = e.charset.convert(e.toTypedArray(this._bodyBuffer), this.charset || "iso-8859-1")), 
        this.charset = this.contentType.params.charset = "utf-8"), this._bodyBuffer = "", 
        this._parser.onbody(this, this.content));
    }, r.prototype._detectHTMLCharset = function(e) {
        var t, n, o;
        return "string" != typeof e && (e = e.toString("ascii")), e = e.replace(/\r?\n|\r/g, " "), 
        (o = e.match(/<meta\s+http-equiv=["'\s]*content-type[^>]*?>/i)) && (n = o[0]), n && (t = n.match(/charset\s?=\s?([a-zA-Z\-_:0-9]*);?/), 
        t && (t = (t[1] || "").trim().toLowerCase())), !t && (o = e.match(/<meta\s+charset=["'\s]*([^"'<>\/\s]+)/i)) && (t = (o[1] || "").trim().toLowerCase()), 
        t;
    }, o;
}), define("imap/jobs", [ "logic", "mix", "../jobmixins", "../drafts/jobs", "../allback", "mimeparser", "module", "exports" ], function(e, t, n, o, r, s, i, a) {
    function c(t, n) {
        this.account = t, this.resilientServerIds = !1, this._heldMutexReleasers = [], e.defineScope(this, "ImapJobDriver", {
            accountId: this.account.id
        }), this._state = n, n.hasOwnProperty("suidToServerId") || (n.suidToServerId = {}, 
        n.moveMap = {}), this._stateDelta = {
            serverIdMap: null,
            moveMap: null
        };
    }
    function d() {}
    var u = "idempotent", l = "/", h = a.deriveFolderPath = function(e, t, n, o) {
        var r, s, i;
        return n ? (s = n.delim || o.delimiter || l, r = n.path || "", i = n.depth + 1) : (s = o.delimiter || l, 
        r = o.prefix || "", i = r ? 1 : 0), r && (r += s), r += e, t && (r += s), {
            path: r,
            delimiter: s,
            depth: i
        };
    };
    a.ImapJobDriver = c, c.prototype = {
        _accessFolderForMutation: function(t, n, o, r, s) {
            var i = this.account.getFolderStorageForFolderId(t), a = this;
            i.runMutexed(s, function(t) {
                var c = i.folderSyncer, d = function() {
                    a._heldMutexReleasers.push(t);
                    try {
                        o(c.folderConn, i);
                    } catch (n) {
                        e(a, "callbackErr", {
                            ex: n
                        });
                    }
                };
                n && !i.isLocalOnly ? c.folderConn.withConnection(function() {
                    a._heldMutexReleasers.push(function() {
                        c.folderConn.clearErrorHandler();
                    }), d();
                }, r, s, !0) : d();
            });
        },
        _partitionAndAccessFoldersSequentially: n._partitionAndAccessFoldersSequentially,
        _acquireConnWithoutFolder: function(t, n, o) {
            e(this, "acquireConnWithoutFolder_begin", {
                label: t
            });
            var r = this;
            this.account.__folderDemandsConnection(null, t, function(o) {
                e(r, "acquireConnWithoutFolder_end", {
                    label: t
                }), r._heldMutexReleasers.push(function() {
                    r.account.__folderDoneWithConnection(o, !1, !1);
                });
                try {
                    n(o);
                } catch (s) {
                    e(r, "callbackErr", {
                        ex: s
                    });
                }
            }, o);
        },
        postJobCleanup: n.postJobCleanup,
        allJobsDone: n.allJobsDone,
        local_do_downloadBodies: n.local_do_downloadBodies,
        do_downloadBodies: n.do_downloadBodies,
        check_downloadBodies: n.check_downloadBodies,
        local_do_downloadBodyReps: n.local_do_downloadBodyReps,
        do_downloadBodyReps: n.do_downloadBodyReps,
        check_downloadBodyReps: n.check_downloadBodyReps,
        local_do_download: n.local_do_download,
        do_download: n.do_download,
        check_download: n.check_download,
        local_undo_download: n.local_undo_download,
        undo_download: n.undo_download,
        local_do_upgradeDB: n.local_do_upgradeDB,
        local_do_modtags: n.local_do_modtags,
        do_modtags: function(e, t, n) {
            var o = n ? e.removeTags : e.addTags, s = n ? e.addTags : e.removeTags, i = null;
            this._partitionAndAccessFoldersSequentially(e.messages, !0, function(t, a, c, d, u) {
                for (var l = [], h = 0; h < c.length; h++) {
                    var p = c[h];
                    p && l.push(p);
                }
                if (!l.length) return u(), void 0;
                var f = r.latch();
                o ? t._conn.setFlags(l.join(","), {
                    add: o
                }, {
                    byUid: !0
                }, f.defer("add")) : s && t._conn.setFlags(l.join(","), {
                    remove: s
                }, {
                    byUid: !0
                }, f.defer("remove")), f.then(function(t) {
                    var o = t.add && t.add[0] || t.remove && t.remove[0];
                    o ? (console.error("failure modifying tags", o), i = "unknown") : e.progress += n ? -c.length : c.length, 
                    u();
                });
            }, function() {
                t(i);
            }, function() {
                i = "aborted-retry";
            }, n, "modtags");
        },
        check_modtags: function(e, t) {
            t(null, u);
        },
        local_undo_modtags: n.local_undo_modtags,
        undo_modtags: function(e, t) {
            return this.do_modtags(e, t, !0);
        },
        local_do_delete: n.local_do_delete,
        do_delete: function(e, t) {
            var n = this.account.getFirstFolderWithType("trash");
            this.do_move(e, t, n.id);
        },
        check_delete: function(e, t) {
            var n = this.account.getFirstFolderWithType("trash");
            this.check_move(e, t, n.id);
        },
        local_undo_delete: n.local_undo_delete,
        undo_delete: function() {},
        local_do_move: n.local_do_move,
        do_move: function(e, t, n) {
            var o = this._state, i = this._stateDelta, a = null;
            i.serverIdMap || (i.serverIdMap = {}), n || (n = e.targetFolder), this._partitionAndAccessFoldersSequentially(e.messages, !0, function(t, c, d, u, l) {
                function h(e, n) {
                    function a() {
                        e._conn.selectMailbox(n.folderMeta.path, c);
                    }
                    function c() {
                        e._conn.listMessages(h + ":*", [ "UID", "BODY.PEEK[HEADER.FIELDS (MESSAGE-ID)]" ], {
                            byUid: u
                        }, function(e, t) {
                            if (e) l(); else {
                                var a = r.latch();
                                t.forEach(function(e) {
                                    var t = a.defer();
                                    for (var r in e) if (/header\.fields/.test(r)) {
                                        var c = new s();
                                        c.write(e[r] + "\r\n"), c.end(), e.headers = c.node.headers;
                                        break;
                                    }
                                    var d = e.headers["message-id"] && e.headers["message-id"][0] && e.headers["message-id"][0].value;
                                    if (d && "<" === d[0] && (d = d.slice(1, -1)), !g.hasOwnProperty(d)) return t(), 
                                    void 0;
                                    var u = g[d];
                                    i.serverIdMap[u.suid] = e.uid;
                                    var l = o.moveMap[u.suid], h = parseInt(l.substring(l.lastIndexOf("/") + 1));
                                    n.updateMessageHeader(u.date, h, !1, function(n) {
                                        return n ? n.srvid = e.uid : console.warn("did not find header for", u.suid, l, u.date, h), 
                                        t(), !0;
                                    }, null);
                                }), a.then(p);
                            }
                        });
                    }
                    var u, h;
                    e.box.uidNext ? (u = !0, h = e.box.uidNext) : (u = !1, h = e.box.exists + 1), t._conn.copyMessages(d.join(","), n.folderMeta.path, {
                        byUid: !0
                    }, a);
                }
                function p() {
                    t._conn.deleteMessages(d.join(","), {
                        byUid: !0
                    }, f);
                }
                function f(e) {
                    e && (a = !0), l();
                }
                for (var g = {}, m = u.length - 1; m >= 0; m--) {
                    var y = d[m];
                    if (y) {
                        var _ = u[m];
                        g[_.guid] = _;
                    } else d.splice(m, 1), u.splice(m, 1);
                }
                return 0 === d.length ? (l(), void 0) : (c.isLocalOnly ? l() : c.folderId === n ? "move" === e.type ? l() : p() : this._accessFolderForMutation(n, !0, h, function() {}, "move target"), 
                void 0);
            }.bind(this), function() {
                t(a);
            }, null, !1, "server move source");
        },
        check_move: function(e, t) {
            t(null, "moot");
        },
        local_undo_move: n.local_undo_move,
        undo_move: function(e, t) {
            t("moot");
        },
        local_do_append: function(e, t) {
            t(null);
        },
        do_append: function(e, t) {
            var n, o = this.account.getFolderStorageForFolderId(e.folderId), r = o.folderMeta, s = 0, i = function(e) {
                return e ? (n = e, c(), void 0) : (u("unknown"), void 0);
            }, a = function() {
                t("aborted-retry");
            }, c = function() {
                var t = e.messages[s++], o = new FileReaderSync().readAsBinaryString(t.messageText);
                n._conn.upload(r.path, o, {
                    flags: t.flags
                }, d);
            }, d = function(t) {
                return t ? (console.error("failure appending message", t), u("unknown"), void 0) : (s < e.messages.length ? c() : u(null), 
                void 0);
            }, u = function(e) {
                n && (n = null), t(e);
            };
            this._accessFolderForMutation(e.folderId, !0, i, a, "append");
        },
        check_append: function(e, t) {
            t(null, "moot");
        },
        local_undo_append: function(e, t) {
            t(null);
        },
        undo_append: function(e, t) {
            t("moot");
        },
        local_do_syncFolderList: function(e, t) {
            t(null);
        },
        do_syncFolderList: function(e, t) {
            var n = this.account, o = !1;
            this._acquireConnWithoutFolder("syncFolderList", function(e) {
                n._syncFolderList(e, function(e) {
                    e || (n.meta.lastFolderSyncAt = Date.now()), o || t(e ? "aborted-retry" : null, null, !e), 
                    o = !0;
                });
            }, function() {
                o || t("aborted-retry"), o = !0;
            });
        },
        check_syncFolderList: function(e, t) {
            t(null, "coherent-notyet");
        },
        local_undo_syncFolderList: function(e, t) {
            t("moot");
        },
        undo_syncFolderList: function(e, t) {
            t("moot");
        },
        local_do_createFolder: function(e, t) {
            t(null);
        },
        do_createFolder: function(t, n) {
            function o(e, t) {
                n && (n(e, t), n = null);
            }
            function r() {
                o("aborted-retry", null);
            }
            var s;
            if (t.parentFolderId) {
                if (!this.account._folderInfos.hasOwnProperty(t.parentFolderId)) throw new Error("No such folder: " + t.parentFolderId);
                s = this.account._folderInfos[t.parentFolderId].$meta;
            }
            var i = this.account._namespaces && this.account._namespaces.personal || {
                prefix: "",
                delimiter: l
            }, a = h(t.folderName, t.containOtherFolders, s, i), c = a.path, d = e.subscope(this, {
                _path: c
            }), u = function(t) {
                e(d, "creatingFolder", {
                    _path: c
                }), t.createMailbox(c, p);
            }.bind(this), p = function(n, r) {
                if (n) {
                    if (!n.message || !/already/i.test(n.message)) return e(d, "createFolderErr", {
                        err: n
                    }), o("failure-give-up", null), void 0;
                    r = !0;
                }
                e(d, "createdFolder", {
                    alreadyExists: r
                });
                var s = this.account._learnAboutFolder(t.folderName, c, t.parentFolderId, t.folderType, a.delimiter, a.depth, !1);
                o(null, s);
            }.bind(this), f = this.account.getFolderByPath(c);
            f ? o(null, f) : this._acquireConnWithoutFolder("createFolder", u, r);
        },
        check_createFolder: function(e, t) {
            t("moot");
        },
        local_undo_createFolder: function(e, t) {
            t(null);
        },
        undo_createFolder: function(e, t) {
            t("moot");
        },
        local_do_purgeExcessMessages: function(e, t) {
            this._accessFolderForMutation(e.folderId, !1, function(e, n) {
                n.purgeExcessMessages(function(e) {
                    t(null, null, e > 0);
                });
            }, null, "purgeExcessMessages");
        },
        do_purgeExcessMessages: function(e, t) {
            t(null);
        },
        check_purgeExcessMessages: function() {
            return u;
        },
        local_undo_purgeExcessMessages: function(e, t) {
            t(null);
        },
        undo_purgeExcessMessages: function(e, t) {
            t(null);
        },
        local_do_sendOutboxMessages: n.local_do_sendOutboxMessages,
        do_sendOutboxMessages: n.do_sendOutboxMessages,
        check_sendOutboxMessages: n.check_sendOutboxMessages,
        local_undo_sendOutboxMessages: n.local_undo_sendOutboxMessages,
        undo_sendOutboxMessages: n.undo_sendOutboxMessages,
        local_do_setOutboxSyncEnabled: n.local_do_setOutboxSyncEnabled
    }, d.prototype = {
        do_xmove: function() {},
        check_xmove: function() {},
        undo_xmove: function() {},
        do_xcopy: function() {},
        check_xcopy: function() {},
        undo_xcopy: function() {}
    }, t(c.prototype, o.draftsMixins);
}), function(e, t) {
    "function" == typeof define && define.amd ? define("imap-formal-syntax", t) : "object" == typeof exports ? module.exports = t() : e.imapFormalSyntax = t();
}(this, function() {
    function e(e, t) {
        for (var n = [], o = e; t >= o; o++) n.push(o);
        return String.fromCharCode.apply(String, n);
    }
    function t(e, t) {
        for (var n = Array.prototype.slice.call(e), o = n.length - 1; o >= 0; o--) t.indexOf(n[o]) >= 0 && n.splice(o, 1);
        return n.join("");
    }
    return {
        CHAR: function() {
            var t = e(1, 127);
            return this.CHAR = function() {
                return t;
            }, t;
        },
        CHAR8: function() {
            var t = e(1, 255);
            return this.CHAR8 = function() {
                return t;
            }, t;
        },
        SP: function() {
            return " ";
        },
        CTL: function() {
            var t = e(0, 31) + "";
            return this.CTL = function() {
                return t;
            }, t;
        },
        DQUOTE: function() {
            return '"';
        },
        ALPHA: function() {
            var t = e(65, 90) + e(97, 122);
            return this.ALPHA = function() {
                return t;
            }, t;
        },
        DIGIT: function() {
            var t = e(48, 57) + e(97, 122);
            return this.DIGIT = function() {
                return t;
            }, t;
        },
        "ATOM-CHAR": function() {
            var e = t(this.CHAR(), this["atom-specials"]());
            return this["ATOM-CHAR"] = function() {
                return e;
            }, e;
        },
        "ASTRING-CHAR": function() {
            var e = this["ATOM-CHAR"]() + this["resp-specials"]();
            return this["ASTRING-CHAR"] = function() {
                return e;
            }, e;
        },
        "TEXT-CHAR": function() {
            var e = t(this.CHAR(), "\r\n");
            return this["TEXT-CHAR"] = function() {
                return e;
            }, e;
        },
        "atom-specials": function() {
            var e = "(){" + this.SP() + this.CTL() + this["list-wildcards"]() + this["quoted-specials"]() + this["resp-specials"]();
            return this["atom-specials"] = function() {
                return e;
            }, e;
        },
        "list-wildcards": function() {
            return "%*";
        },
        "quoted-specials": function() {
            var e = this.DQUOTE() + "\\";
            return this["quoted-specials"] = function() {
                return e;
            }, e;
        },
        "resp-specials": function() {
            return "]";
        },
        tag: function() {
            var e = t(this["ASTRING-CHAR"](), "+");
            return this.tag = function() {
                return e;
            }, e;
        },
        command: function() {
            var e = this.ALPHA() + this.DIGIT();
            return this.command = function() {
                return e;
            }, e;
        },
        verify: function(e, t) {
            for (var n = 0, o = e.length; o > n; n++) if (t.indexOf(e.charAt(n)) < 0) return n;
            return -1;
        }
    };
}), function(e, t) {
    "function" == typeof define && define.amd ? define("imap-handler/src/imap-parser", [ "imap-formal-syntax" ], t) : "object" == typeof exports ? module.exports = t(require("./imap-formal-syntax")) : e.imapParser = t(e.imapFormalSyntax);
}(this, function(e) {
    function t(e, t) {
        this.input = (e || "").toString(), this.options = t || {}, this.remainder = this.input, 
        this.pos = 0;
    }
    function n(e, t, n, o) {
        this.str = (n || "").toString(), this.options = o || {}, this.parent = e, this.tree = this.currentNode = this.createNode(), 
        this.pos = t || 0, this.currentNode.type = "TREE", this.state = "NORMAL", this.processString();
    }
    return t.prototype.getTag = function() {
        return this.tag || (this.tag = this.getElement(e.tag() + "*+", !0)), this.tag;
    }, t.prototype.getCommand = function() {
        var t;
        switch (this.command || (this.command = this.getElement(e.command())), (this.command || "").toString().toUpperCase()) {
          case "OK":
          case "NO":
          case "BAD":
          case "PREAUTH":
          case "BYE":
            t = this.remainder.match(/^ \[(?:[^\]]*\])+/), t ? (this.humanReadable = this.remainder.substr(t[0].length).trim(), 
            this.remainder = t[0]) : (this.humanReadable = this.remainder.trim(), this.remainder = "");
        }
        return this.command;
    }, t.prototype.getElement = function(t) {
        var n, o, r;
        if (this.remainder.match(/^\s/)) throw new Error("Unexpected whitespace at position " + this.pos);
        if (!(n = this.remainder.match(/^[^\s]+(?=\s|$)/))) throw new Error("Unexpected end of input at position " + this.pos);
        if (o = n[0], (r = e.verify(o, t)) >= 0) throw new Error("Unexpected char at position " + (this.pos + r));
        return this.pos += n[0].length, this.remainder = this.remainder.substr(n[0].length), 
        o;
    }, t.prototype.getSpace = function() {
        if (!this.remainder.length) throw new Error("Unexpected end of input at position " + this.pos);
        if (e.verify(this.remainder.charAt(0), e.SP()) >= 0) throw new Error("Unexpected char at position " + this.pos);
        this.pos++, this.remainder = this.remainder.substr(1);
    }, t.prototype.getAttributes = function() {
        if (!this.remainder.length) throw new Error("Unexpected end of input at position " + this.pos);
        if (this.remainder.match(/^\s/)) throw new Error("Unexpected whitespace at position " + this.pos);
        return new n(this, this.pos, this.remainder, this.options).getAttributes();
    }, n.prototype.getAttributes = function() {
        var e = [], t = e, n = function(e) {
            var o, r, s = t;
            if (e.closed || "SEQUENCE" !== e.type || "*" !== e.value || (e.closed = !0, e.type = "ATOM"), 
            !e.closed) throw new Error("Unexpected end of input at position " + (this.pos + this.str.length - 1));
            switch (e.type.toUpperCase()) {
              case "LITERAL":
              case "STRING":
              case "SEQUENCE":
                o = {
                    type: e.type.toUpperCase(),
                    value: e.value
                }, t.push(o);
                break;

              case "ATOM":
                if ("NIL" === e.value.toUpperCase()) {
                    t.push(null);
                    break;
                }
                o = {
                    type: e.type.toUpperCase(),
                    value: e.value
                }, t.push(o);
                break;

              case "SECTION":
                t = t[t.length - 1].section = [];
                break;

              case "LIST":
                o = [], t.push(o), t = o;
                break;

              case "PARTIAL":
                if (r = e.value.split(".").map(Number), r.slice(-1)[0] < r.slice(0, 1)[0]) throw new Error("Invalid partial value at position " + e.startPos);
                t[t.length - 1].partial = r;
            }
            e.childNodes.forEach(function(e) {
                n(e);
            }), t = s;
        }.bind(this);
        return n(this.tree), e;
    }, n.prototype.createNode = function(e, t) {
        var n = {
            childNodes: [],
            type: !1,
            value: "",
            closed: !0
        };
        return e && (n.parentNode = e), "number" == typeof t && (n.startPos = t), e && e.childNodes.push(n), 
        n;
    }, n.prototype.processString = function() {
        var t, n, o, r = function() {
            for (;" " === this.str.charAt(n + 1); ) n++;
        }.bind(this);
        for (n = 0, o = this.str.length; o > n; n++) switch (t = this.str.charAt(n), this.state) {
          case "NORMAL":
            switch (t) {
              case '"':
                this.currentNode = this.createNode(this.currentNode, this.pos + n), this.currentNode.type = "string", 
                this.state = "STRING", this.currentNode.closed = !1;
                break;

              case "(":
                this.currentNode = this.createNode(this.currentNode, this.pos + n), this.currentNode.type = "LIST", 
                this.currentNode.closed = !1;
                break;

              case ")":
                if ("LIST" !== this.currentNode.type) throw new Error("Unexpected list terminator ) at position " + (this.pos + n));
                this.currentNode.closed = !0, this.currentNode.endPos = this.pos + n, this.currentNode = this.currentNode.parentNode, 
                r();
                break;

              case "]":
                if ("SECTION" !== this.currentNode.type) throw new Error("Unexpected section terminator ] at position " + (this.pos + n));
                this.currentNode.closed = !0, this.currentNode.endPos = this.pos + n, this.currentNode = this.currentNode.parentNode, 
                r();
                break;

              case "<":
                "]" !== this.str.charAt(n - 1) ? (this.currentNode = this.createNode(this.currentNode, this.pos + n), 
                this.currentNode.type = "ATOM", this.currentNode.value = t, this.state = "ATOM") : (this.currentNode = this.createNode(this.currentNode, this.pos + n), 
                this.currentNode.type = "PARTIAL", this.state = "PARTIAL", this.currentNode.closed = !1);
                break;

              case "{":
                this.currentNode = this.createNode(this.currentNode, this.pos + n), this.currentNode.type = "LITERAL", 
                this.state = "LITERAL", this.currentNode.closed = !1;
                break;

              case "*":
                this.currentNode = this.createNode(this.currentNode, this.pos + n), this.currentNode.type = "SEQUENCE", 
                this.currentNode.value = t, this.currentNode.closed = !1, this.state = "SEQUENCE";
                break;

              case " ":
                break;

              case "[":
                if ([ "OK", "NO", "BAD", "BYE", "PREAUTH" ].indexOf(this.parent.command.toUpperCase()) >= 0 && this.currentNode === this.tree) {
                    this.currentNode.endPos = this.pos + n, this.currentNode = this.createNode(this.currentNode, this.pos + n), 
                    this.currentNode.type = "ATOM", this.currentNode = this.createNode(this.currentNode, this.pos + n), 
                    this.currentNode.type = "SECTION", this.currentNode.closed = !1, this.state = "NORMAL", 
                    "REFERRAL " === this.str.substr(n + 1, 9).toUpperCase() && (this.currentNode = this.createNode(this.currentNode, this.pos + n + 1), 
                    this.currentNode.type = "ATOM", this.currentNode.endPos = this.pos + n + 8, this.currentNode.value = "REFERRAL", 
                    this.currentNode = this.currentNode.parentNode, this.currentNode = this.createNode(this.currentNode, this.pos + n + 10), 
                    this.currentNode.type = "ATOM", n = this.str.indexOf("]", n + 10), this.currentNode.endPos = this.pos + n - 1, 
                    this.currentNode.value = this.str.substring(this.currentNode.startPos - this.pos, this.currentNode.endPos - this.pos + 1), 
                    this.currentNode = this.currentNode.parentNode, this.currentNode.closed = !0, this.currentNode = this.currentNode.parentNode, 
                    r());
                    break;
                }

              default:
                if (e["ATOM-CHAR"]().indexOf(t) < 0 && "\\" !== t && "%" !== t) throw new Error("Unexpected char at position " + (this.pos + n));
                this.currentNode = this.createNode(this.currentNode, this.pos + n), this.currentNode.type = "ATOM", 
                this.currentNode.value = t, this.state = "ATOM";
            }
            break;

          case "ATOM":
            if (" " === t) {
                this.currentNode.endPos = this.pos + n - 1, this.currentNode = this.currentNode.parentNode, 
                this.state = "NORMAL";
                break;
            }
            if (this.currentNode.parentNode && (")" === t && "LIST" === this.currentNode.parentNode.type || "]" === t && "SECTION" === this.currentNode.parentNode.type)) {
                this.currentNode.endPos = this.pos + n - 1, this.currentNode = this.currentNode.parentNode, 
                this.currentNode.closed = !0, this.currentNode.endPos = this.pos + n, this.currentNode = this.currentNode.parentNode, 
                this.state = "NORMAL", r();
                break;
            }
            if ("," !== t && ":" !== t || !this.currentNode.value.match(/^\d+$/) || (this.currentNode.type = "SEQUENCE", 
            this.currentNode.closed = !0, this.state = "SEQUENCE"), "[" === t) {
                if ([ "BODY", "BODY.PEEK" ].indexOf(this.currentNode.value.toUpperCase()) < 0) throw new Error("Unexpected section start char [ at position " + this.pos);
                this.currentNode.endPos = this.pos + n, this.currentNode = this.createNode(this.currentNode.parentNode, this.pos + n), 
                this.currentNode.type = "SECTION", this.currentNode.closed = !1, this.state = "NORMAL";
                break;
            }
            if ("<" === t) throw new Error("Unexpected start of partial at position " + this.pos);
            if (e["ATOM-CHAR"]().indexOf(t) < 0 && "]" !== t && ("*" !== t || "\\" !== this.currentNode.value)) throw new Error("Unexpected char at position " + (this.pos + n));
            if ("\\*" === this.currentNode.value) throw new Error("Unexpected char at position " + (this.pos + n));
            this.currentNode.value += t;
            break;

          case "STRING":
            if ('"' === t) {
                this.currentNode.endPos = this.pos + n, this.currentNode.closed = !0, this.currentNode = this.currentNode.parentNode, 
                this.state = "NORMAL", r();
                break;
            }
            if ("\\" === t) {
                if (n++, n >= o) throw new Error("Unexpected end of input at position " + (this.pos + n));
                t = this.str.charAt(n);
            }
            this.currentNode.value += t;
            break;

          case "PARTIAL":
            if (">" === t) {
                if ("." === this.currentNode.value.substr(-1)) throw new Error("Unexpected end of partial at position " + this.pos);
                this.currentNode.endPos = this.pos + n, this.currentNode.closed = !0, this.currentNode = this.currentNode.parentNode, 
                this.state = "NORMAL", r();
                break;
            }
            if ("." === t && (!this.currentNode.value.length || this.currentNode.value.match(/\./))) throw new Error("Unexpected partial separator . at position " + this.pos);
            if (e.DIGIT().indexOf(t) < 0 && "." !== t) throw new Error("Unexpected char at position " + (this.pos + n));
            if (this.currentNode.value.match(/^0$|\.0$/) && "." !== t) throw new Error("Invalid partial at position " + (this.pos + n));
            this.currentNode.value += t;
            break;

          case "LITERAL":
            if (this.currentNode.started) {
                if ("\0" === t) throw new Error("Unexpected \\x00 at position " + (this.pos + n));
                this.currentNode.value += t, this.currentNode.value.length >= this.currentNode.literalLength && (this.currentNode.endPos = this.pos + n, 
                this.currentNode.closed = !0, this.currentNode = this.currentNode.parentNode, this.state = "NORMAL", 
                r());
                break;
            }
            if ("+" === t && this.options.literalPlus) {
                this.currentNode.literalPlus = !0;
                break;
            }
            if ("}" === t) {
                if (!("literalLength" in this.currentNode)) throw new Error("Unexpected literal prefix end char } at position " + (this.pos + n));
                if ("\n" === this.str.charAt(n + 1)) n++; else {
                    if ("\r" !== this.str.charAt(n + 1) || "\n" !== this.str.charAt(n + 2)) throw new Error("Unexpected char at position " + (this.pos + n));
                    n += 2;
                }
                this.currentNode.literalLength = Number(this.currentNode.literalLength), this.currentNode.started = !0, 
                this.currentNode.literalLength || (this.currentNode.endPos = this.pos + n, this.currentNode.closed = !0, 
                this.currentNode = this.currentNode.parentNode, this.state = "NORMAL", r());
                break;
            }
            if (e.DIGIT().indexOf(t) < 0) throw new Error("Unexpected char at position " + (this.pos + n));
            if ("0" === this.currentNode.literalLength) throw new Error("Invalid literal at position " + (this.pos + n));
            this.currentNode.literalLength = (this.currentNode.literalLength || "") + t;
            break;

          case "SEQUENCE":
            if (" " === t) {
                if (!this.currentNode.value.substr(-1).match(/\d/) && "*" !== this.currentNode.value.substr(-1)) throw new Error("Unexpected whitespace at position " + (this.pos + n));
                if ("*" === this.currentNode.value.substr(-1) && ":" !== this.currentNode.value.substr(-2, 1)) throw new Error("Unexpected whitespace at position " + (this.pos + n));
                this.currentNode.closed = !0, this.currentNode.endPos = this.pos + n - 1, this.currentNode = this.currentNode.parentNode, 
                this.state = "NORMAL";
                break;
            }
            if (this.currentNode.parentNode && "]" === t && "SECTION" === this.currentNode.parentNode.type) {
                this.currentNode.endPos = this.pos + n - 1, this.currentNode = this.currentNode.parentNode, 
                this.currentNode.closed = !0, this.currentNode.endPos = this.pos + n, this.currentNode = this.currentNode.parentNode, 
                this.state = "NORMAL", r();
                break;
            }
            if (":" === t) {
                if (!this.currentNode.value.substr(-1).match(/\d/) && "*" !== this.currentNode.value.substr(-1)) throw new Error("Unexpected range separator : at position " + (this.pos + n));
            } else if ("*" === t) {
                if ([ ",", ":" ].indexOf(this.currentNode.value.substr(-1)) < 0) throw new Error("Unexpected range wildcard at position " + (this.pos + n));
            } else if ("," === t) {
                if (!this.currentNode.value.substr(-1).match(/\d/) && "*" !== this.currentNode.value.substr(-1)) throw new Error("Unexpected sequence separator , at position " + (this.pos + n));
                if ("*" === this.currentNode.value.substr(-1) && ":" !== this.currentNode.value.substr(-2, 1)) throw new Error("Unexpected sequence separator , at position " + (this.pos + n));
            } else if (!t.match(/\d/)) throw new Error("Unexpected char at position " + (this.pos + n));
            if (t.match(/\d/) && "*" === this.currentNode.value.substr(-1)) throw new Error("Unexpected number at position " + (this.pos + n));
            this.currentNode.value += t;
        }
    }, function(n, o) {
        var r, s = {};
        return o = o || {}, r = new t(n, o), s.tag = r.getTag(), r.getSpace(), s.command = r.getCommand(), 
        [ "UID", "AUTHENTICATE" ].indexOf((s.command || "").toUpperCase()) >= 0 && (r.getSpace(), 
        s.command += " " + r.getElement(e.command())), r.remainder.trim().length && (r.getSpace(), 
        s.attributes = r.getAttributes()), r.humanReadable && (s.attributes = (s.attributes || []).concat({
            type: "TEXT",
            value: r.humanReadable
        })), s;
    };
}), function(e, t) {
    "function" == typeof define && define.amd ? define("imap-handler/src/imap-compiler", [ "imap-formal-syntax" ], t) : "object" == typeof exports ? module.exports = t(require("./imap-formal-syntax")) : e.imapCompiler = t(e.imapFormalSyntax);
}(this, function(e) {
    return function(t, n, o) {
        var r, s, i = [], a = (t.tag || "") + (t.command ? " " + t.command : ""), c = function(t) {
            if (("LITERAL" === s || [ "(", "<", "[" ].indexOf(a.substr(-1)) < 0 && a.length) && (a += " "), 
            Array.isArray(t)) return s = "LIST", a += "(", t.forEach(c), a += ")", void 0;
            if (!t && "string" != typeof t && "number" != typeof t) return a += "NIL", void 0;
            if ("string" == typeof t) return a += o && t.length > 20 ? '"(* ' + t.length + 'B string *)"' : JSON.stringify(t), 
            void 0;
            if ("number" == typeof t) return a += Math.round(t) || 0, void 0;
            if (s = t.type, o && t.sensitive) return a += '"(* value hidden *)"', void 0;
            switch (t.type.toUpperCase()) {
              case "LITERAL":
                o ? a += '"(* ' + t.value.length + 'B literal *)"' : (a += t.value ? "{" + t.value.length + "}\r\n" : "{0}\r\n", 
                i.push(a), a = t.value || "");
                break;

              case "STRING":
                a += o && t.value.length > 20 ? '"(* ' + t.value.length + 'B string *)"' : JSON.stringify(t.value || "");
                break;

              case "TEXT":
              case "SEQUENCE":
                a += t.value || "";
                break;

              case "NUMBER":
                a += t.value || 0;
                break;

              case "ATOM":
              case "SECTION":
                r = t.value || "", e.verify("\\" === r.charAt(0) ? r.substr(1) : r, e["ATOM-CHAR"]()) >= 0 && (r = JSON.stringify(r)), 
                a += r, t.section && (a += "[", t.section.forEach(c), a += "]"), t.partial && (a += "<" + t.partial.join(".") + ">");
            }
        };
        return [].concat(t.attributes || []).forEach(c), a.length && i.push(a), n ? i : i.join("");
    };
}), function(e, t) {
    "function" == typeof define && define.amd ? define("imap-handler/src/imap-handler", [ "./imap-parser", "./imap-compiler" ], t) : "object" == typeof exports ? module.exports = t(require("./imap-parser"), require("./imap-compiler")) : e.imapHandler = t(e.imapParser, e.imapCompiler);
}(this, function(e, t) {
    return {
        parser: e,
        compiler: t
    };
}), define("imap-handler", [ "./imap-handler/src/imap-handler" ], function(e) {
    return e;
}), define("axeshim-browserbox", [ "require", "logic" ], function(e) {
    var t = e("logic"), n = t.scope("BrowserBox");
    return {
        debug: function(e, o) {
            t.isCensored || t(n, "debug", {
                msg: o
            });
        },
        log: function(e, o) {
            t(n, "log", {
                msg: o
            });
        },
        warn: function(e, o) {
            t(n, "warn", {
                msg: o
            });
        },
        error: function(e, o) {
            t(n, "error", {
                msg: o
            });
        }
    };
}), function(e, t) {
    "function" == typeof define && define.amd ? define("browserbox-imap", [ "tcp-socket", "imap-handler", "mimefuncs", "axe" ], function(e, n, o, r) {
        return t(e, n, o, r);
    }) : "object" == typeof exports ? module.exports = t(require("tcp-socket"), require("wo-imap-handler"), require("mimefuncs"), require("axe-logger")) : e.BrowserboxImapClient = t(navigator.TCPSocket, e.imapHandler, e.mimefuncs, e.axe);
}(this, function(e, t, n, o) {
    function r(t, n, o) {
        this._TCPSocket = e, this.options = o || {}, this.port = n || (this.options.useSecureTransport ? 993 : 143), 
        this.host = t || "localhost", this.options.useSecureTransport = "useSecureTransport" in this.options ? !!this.options.useSecureTransport : 993 === this.port, 
        this.options.auth = this.options.auth || !1, this.socket = !1, this.destroyed = !1, 
        this.waitDrain = !1, this.secureMode = !!this.options.useSecureTransport, this._connectionReady = !1, 
        this._remainder = "", this._command = "", this._literalRemaining = 0, this._processingServerData = !1, 
        this._serverQueue = [], this._canSend = !1, this._clientQueue = [], this._tagCounter = 0, 
        this._currentCommand = !1, this._globalAcceptUntagged = {}, this._idleTimer = !1, 
        this._socketTimeoutTimer = !1;
    }
    var s = "browserbox IMAP";
    return r.prototype.TIMEOUT_ENTER_IDLE = 1e3, r.prototype.TIMEOUT_SOCKET_LOWER_BOUND = 1e4, 
    r.prototype.TIMEOUT_SOCKET_MULTIPLIER = .1, r.prototype.onerror = function() {}, 
    r.prototype.ondrain = function() {}, r.prototype.onclose = function() {}, r.prototype.onready = function() {}, 
    r.prototype.onidle = function() {}, r.prototype.connect = function() {
        this.socket = this._TCPSocket.open(this.host, this.port, {
            binaryType: "arraybuffer",
            useSecureTransport: this.secureMode,
            ca: this.options.ca,
            tlsWorkerPath: this.options.tlsWorkerPath
        });
        try {
            this.socket.oncert = this.oncert;
        } catch (e) {}
        this.socket.onerror = this._onError.bind(this), this.socket.onopen = this._onOpen.bind(this);
    }, r.prototype.close = function() {
        this.socket && "open" === this.socket.readyState ? this.socket.close() : this._destroy();
    }, r.prototype.upgrade = function(e) {
        return this.secureMode ? e(null, !1) : (this.secureMode = !0, this.socket.upgradeToSecure(), 
        e(null, !0), void 0);
    }, r.prototype.exec = function(e, t, n, o) {
        return "string" == typeof e && (e = {
            command: e
        }), this._addToClientQueue(e, t, n, o), this;
    }, r.prototype.send = function(e) {
        var t = n.toTypedArray(e).buffer, o = this.TIMEOUT_SOCKET_LOWER_BOUND + Math.floor(t.byteLength * this.TIMEOUT_SOCKET_MULTIPLIER);
        clearTimeout(this._socketTimeoutTimer), this._socketTimeoutTimer = setTimeout(this._onTimeout.bind(this), o), 
        this.waitDrain = this.socket.send(t);
    }, r.prototype.setHandler = function(e, t) {
        this._globalAcceptUntagged[(e || "").toString().toUpperCase().trim()] = t;
    }, r.prototype._onError = function(e) {
        this.isError(e) ? this.onerror(e) : e && this.isError(e.data) ? this.onerror(e.data) : this.onerror(new Error(e && e.data && e.data.message || e.data || e || "Error")), 
        this.close();
    }, r.prototype._destroy = function() {
        this._serverQueue = [], this._clientQueue = [], this._currentCommand = !1, clearTimeout(this._idleTimer), 
        clearTimeout(this._socketTimeoutTimer), this.destroyed || (this.destroyed = !0, 
        this.onclose());
    }, r.prototype._onClose = function() {
        this._destroy();
    }, r.prototype._onTimeout = function() {
        var e = new Error(this.options.sessionId + " Socket timed out!");
        o.error(s, e), this._onError(e);
    }, r.prototype._onDrain = function() {
        this.waitDrain = !1, this.ondrain();
    }, r.prototype._onData = function(e) {
        if (e && e.data) {
            clearTimeout(this._socketTimeoutTimer);
            var t, o = n.fromTypedArray(e.data);
            if (this._literalRemaining) {
                if (this._literalRemaining > o.length) return this._literalRemaining -= o.length, 
                this._command += o, void 0;
                this._command += o.substr(0, this._literalRemaining), o = o.substr(this._literalRemaining), 
                this._literalRemaining = 0;
            }
            for (this._remainder = o = this._remainder + o; t = o.match(/(\{(\d+)(\+)?\})?\r?\n/); ) if (t[2]) {
                if (this._remainder = "", this._command += o.substr(0, t.index + t[0].length), this._literalRemaining = Number(t[2]), 
                o = o.substr(t.index + t[0].length), this._literalRemaining > o.length) return this._command += o, 
                this._literalRemaining -= o.length, void 0;
                this._command += o.substr(0, this._literalRemaining), this._remainder = o = o.substr(this._literalRemaining), 
                this._literalRemaining = 0;
            } else this._addToServerQueue(this._command + o.substr(0, t.index)), this._remainder = o = o.substr(t.index + t[0].length), 
            this._command = "";
        }
    }, r.prototype._onOpen = function() {
        o.debug(s, this.options.sessionId + " tcp socket opened"), this.socket.ondata = this._onData.bind(this), 
        this.socket.onclose = this._onClose.bind(this), this.socket.ondrain = this._onDrain.bind(this);
    }, r.prototype._addToServerQueue = function(e) {
        this._serverQueue.push(e), this._processingServerData || (this._processingServerData = !0, 
        this._processServerQueue());
    }, r.prototype._processServerQueue = function() {
        if (!this._serverQueue.length) return this._processingServerData = !1, void 0;
        this._clearIdle();
        var e, n = this._serverQueue.shift();
        try {
            /^\+/.test(n) ? e = {
                tag: "+",
                payload: n.substr(2) || ""
            } : (e = t.parser(n), o.debug(s, this.options.sessionId + " S: " + t.compiler(e, !1, !0)));
        } catch (r) {
            return o.error(s, this.options.sessionId + " error parsing imap response: " + r + "\n" + r.stack + "\nraw:" + n), 
            this._onError(r);
        }
        if ("*" === e.tag && /^\d+$/.test(e.command) && e.attributes && e.attributes.length && "ATOM" === e.attributes[0].type && (e.nr = Number(e.command), 
        e.command = (e.attributes.shift().value || "").toString().toUpperCase().trim()), 
        "+" === e.tag) {
            if (this._currentCommand.data.length) n = this._currentCommand.data.shift(), this.send(n + (this._currentCommand.data.length ? "" : "\r\n")); else if ("function" == typeof this._currentCommand.onplustagged) return this._currentCommand.onplustagged(e, this._processServerQueue.bind(this)), 
            void 0;
            return setTimeout(this._processServerQueue.bind(this), 0), void 0;
        }
        this._processServerResponse(e, function(t) {
            return t ? this._onError(t) : (this._connectionReady ? "*" !== e.tag && (this._canSend = !0, 
            this._sendRequest()) : (this._connectionReady = !0, this.onready(), this._canSend = !0, 
            this._sendRequest()), setTimeout(this._processServerQueue.bind(this), 0), void 0);
        }.bind(this));
    }, r.prototype._processServerResponse = function(e, t) {
        var n = (e && e.command || "").toUpperCase().trim();
        return this._processResponse(e), this._currentCommand ? this._currentCommand.payload && "*" === e.tag && n in this._currentCommand.payload ? (this._currentCommand.payload[n].push(e), 
        t()) : "*" === e.tag && n in this._globalAcceptUntagged ? (this._globalAcceptUntagged[n](e, t), 
        void 0) : e.tag === this._currentCommand.tag ? "function" == typeof this._currentCommand.callback ? (this._currentCommand.payload && Object.keys(this._currentCommand.payload).length && (e.payload = this._currentCommand.payload), 
        this._currentCommand.callback(e, t)) : t() : t() : "*" === e.tag && n in this._globalAcceptUntagged ? this._globalAcceptUntagged[n](e, t) : t();
    }, r.prototype._addToClientQueue = function(e, t, n, o) {
        var r, s = "W" + ++this._tagCounter;
        o || "function" != typeof n || (o = n, n = void 0), o || "function" != typeof t || (o = t, 
        t = void 0), t = [].concat(t || []).map(function(e) {
            return (e || "").toString().toUpperCase().trim();
        }), e.tag = s, r = {
            tag: s,
            request: e,
            payload: t.length ? {} : void 0,
            callback: o
        }, Object.keys(n || {}).forEach(function(e) {
            r[e] = n[e];
        }), t.forEach(function(e) {
            r.payload[e] = [];
        });
        var i = r.ctx ? this._clientQueue.indexOf(r.ctx) : -1;
        i >= 0 ? (r.tag += ".p", r.request.tag += ".p", this._clientQueue.splice(i, 0, r)) : this._clientQueue.push(r), 
        this._canSend && this._sendRequest();
    }, r.prototype._sendRequest = function() {
        if (!this._clientQueue.length) return this._enterIdle();
        this._clearIdle(), this._restartQueue = !1;
        var e = this._clientQueue[0];
        if ("function" == typeof e.precheck) {
            var n = e, r = n.precheck;
            return delete n.precheck, this._restartQueue = !0, r(n, function(e) {
                if (!e) return this._restartQueue && this._sendRequest(), void 0;
                var t, o = this._clientQueue.indexOf(n);
                o >= 0 && (t = this._clientQueue.splice(o, 1)[0]), t && t.callback && t.callback(e, function() {
                    this._canSend = !0, this._sendRequest(), setTimeout(this._processServerQueue.bind(this), 0);
                }.bind(this));
            }.bind(this)), void 0;
        }
        this._canSend = !1, this._currentCommand = this._clientQueue.shift();
        var i = !1;
        try {
            this._currentCommand.data = t.compiler(this._currentCommand.request, !0), i = t.compiler(this._currentCommand.request, !1, !0);
        } catch (a) {
            return o.error(s, this.options.sessionId + " error compiling imap command: " + a + "\nstack trace: " + a.stack + "\nraw:" + this._currentCommand.request), 
            this._onError(a);
        }
        o.debug(s, this.options.sessionId + " C: " + i);
        var c = this._currentCommand.data.shift();
        return this.send(c + (this._currentCommand.data.length ? "" : "\r\n")), this.waitDrain;
    }, r.prototype._enterIdle = function() {
        clearTimeout(this._idleTimer), this._idleTimer = setTimeout(function() {
            this.onidle();
        }.bind(this), this.TIMEOUT_ENTER_IDLE);
    }, r.prototype._clearIdle = function() {
        clearTimeout(this._idleTimer);
    }, r.prototype._processResponse = function(e) {
        var t, n, o = (e && e.command || "").toString().toUpperCase().trim();
        [ "OK", "NO", "BAD", "BYE", "PREAUTH" ].indexOf(o) >= 0 && ((t = e && e.attributes && e.attributes.length && "ATOM" === e.attributes[0].type && e.attributes[0].section && e.attributes[0].section.map(function(e) {
            return e ? Array.isArray(e) ? e.map(function(e) {
                return (e.value || "").toString().trim();
            }) : (e.value || "").toString().toUpperCase().trim() : void 0;
        })) && (n = t && t.shift(), e.code = n, t.length && (t = [].concat(t || []), e[n.toLowerCase()] = 1 === t.length ? t[0] : t)), 
        e && e.attributes && e.attributes.length && "TEXT" === e.attributes[e.attributes.length - 1].type && (e.humanReadable = e.attributes[e.attributes.length - 1].value));
    }, r.prototype.isError = function(e) {
        return !!Object.prototype.toString.call(e).match(/Error\]$/);
    }, r;
}), function(e, t) {
    "function" == typeof define && define.amd ? define("browserbox", [ "browserbox-imap", "utf7", "imap-handler", "mimefuncs", "axe" ], function(e, n, o, r, s) {
        return t(e, n, o, r, s);
    }) : "object" == typeof exports ? module.exports = t(require("./browserbox-imap"), require("wo-utf7"), require("wo-imap-handler"), require("mimefuncs"), require("axe-logger")) : e.BrowserBox = t(e.BrowserboxImapClient, e.utf7, e.imapHandler, e.mimefuncs, e.axe);
}(this, function(e, t, n, o, r) {
    function s(t, n, o) {
        this.options = o || {}, this.options.sessionId = this.options.sessionId || "[" + ++l + "]", 
        this.capability = [], this.serverId = !1, this.state = !1, this.authenticated = !1, 
        this.selectedMailbox = !1, this.client = new e(t, n, this.options), this._enteredIdle = !1, 
        this._idleTimeout = !1, this._init();
    }
    function i(e, t) {
        return function() {
            var n = Array.prototype.slice.call(arguments), o = n.shift();
            o ? t(o) : e.apply(null, n);
        };
    }
    var a = "browserbox", c = [ "\\All", "\\Archive", "\\Drafts", "\\Flagged", "\\Junk", "\\Sent", "\\Trash" ], d = {
        "\\Sent": [ "aika", "bidaliak", "bidalita", "dihantar", "e rometsweng", "e tindami", "elküldött", "elküldöttek", "enviadas", "enviadas", "enviados", "enviats", "envoyés", "ethunyelweyo", "expediate", "ezipuru", "gesendete", "gestuur", "gönderilmiş öğeler", "göndərilənlər", "iberilen", "inviati", "išsiųstieji", "kuthunyelwe", "lasa", "lähetetyt", "messages envoyés", "naipadala", "nalefa", "napadala", "nosūtītās ziņas", "odeslané", "padala", "poslane", "poslano", "poslano", "poslané", "poslato", "saadetud", "saadetud kirjad", "sendt", "sendt", "sent", "sent items", "sent messages", "sända poster", "sänt", "terkirim", "ti fi ranṣẹ", "të dërguara", "verzonden", "vilivyotumwa", "wysłane", "đã gửi", "σταλθέντα", "жиберилген", "жіберілгендер", "изпратени", "илгээсэн", "ирсол шуд", "испратено", "надіслані", "отправленные", "пасланыя", "юборилган", "ուղարկված", "נשלחו", "פריטים שנשלחו", "المرسلة", "بھیجے گئے", "سوزمژہ", "لېګل شوی", "موارد ارسال شده", "पाठविले", "पाठविलेले", "प्रेषित", "भेजा गया", "প্রেরিত", "প্রেরিত", "প্ৰেৰিত", "ਭੇਜੇ", "મોકલેલા", "ପଠାଗଲା", "அனுப்பியவை", "పంపించబడింది", "ಕಳುಹಿಸಲಾದ", "അയച്ചു", "යැවු පණිවුඩ", "ส่งแล้ว", "გაგზავნილი", "የተላኩ", "បាន​ផ្ញើ", "寄件備份", "寄件備份", "已发信息", "送信済みﾒｰﾙ", "발신 메시지", "보낸 편지함" ],
        "\\Trash": [ "articole șterse", "bin", "borttagna objekt", "deleted", "deleted items", "deleted messages", "elementi eliminati", "elementos borrados", "elementos eliminados", "gelöschte objekte", "item dipadam", "itens apagados", "itens excluídos", "mục đã xóa", "odstraněné položky", "pesan terhapus", "poistetut", "praht", "prügikast", "silinmiş öğeler", "slettede beskeder", "slettede elementer", "trash", "törölt elemek", "usunięte wiadomości", "verwijderde items", "vymazané správy", "éléments supprimés", "видалені", "жойылғандар", "удаленные", "פריטים שנמחקו", "العناصر المحذوفة", "موارد حذف شده", "รายการที่ลบ", "已删除邮件", "已刪除項目", "已刪除項目" ],
        "\\Junk": [ "bulk mail", "correo no deseado", "courrier indésirable", "istenmeyen", "istenmeyen e-posta", "junk", "levélszemét", "nevyžiadaná pošta", "nevyžádaná pošta", "no deseado", "posta indesiderata", "pourriel", "roskaposti", "skräppost", "spam", "spam", "spamowanie", "søppelpost", "thư rác", "спам", "דואר זבל", "الرسائل العشوائية", "هرزنامه", "สแปม", "‎垃圾郵件", "垃圾邮件", "垃圾電郵" ],
        "\\Drafts": [ "ba brouillon", "borrador", "borrador", "borradores", "bozze", "brouillons", "bản thảo", "ciorne", "concepten", "draf", "drafts", "drög", "entwürfe", "esborranys", "garalamalar", "ihe edeturu", "iidrafti", "izinhlaka", "juodraščiai", "kladd", "kladder", "koncepty", "koncepty", "konsep", "konsepte", "kopie robocze", "layihələr", "luonnokset", "melnraksti", "meralo", "mesazhe të padërguara", "mga draft", "mustandid", "nacrti", "nacrti", "osnutki", "piszkozatok", "rascunhos", "rasimu", "skice", "taslaklar", "tsararrun saƙonni", "utkast", "vakiraoka", "vázlatok", "zirriborroak", "àwọn àkọpamọ́", "πρόχειρα", "жобалар", "нацрти", "нооргууд", "сиёҳнавис", "хомаки хатлар", "чарнавікі", "чернетки", "чернови", "черновики", "черновиктер", "սևագրեր", "טיוטות", "مسودات", "مسودات", "موسودې", "پیش نویسها", "ڈرافٹ/", "ड्राफ़्ट", "प्रारूप", "খসড়া", "খসড়া", "ড্ৰাফ্ট", "ਡ੍ਰਾਫਟ", "ડ્રાફ્ટસ", "ଡ୍ରାଫ୍ଟ", "வரைவுகள்", "చిత్తు ప్రతులు", "ಕರಡುಗಳು", "കരടുകള്‍", "කෙටුම් පත්", "ฉบับร่าง", "მონახაზები", "ረቂቆች", "សារព្រាង", "下書き", "草稿", "草稿", "草稿", "임시 보관함" ]
    }, u = Object.keys(d), l = 0;
    return s.prototype.STATE_CONNECTING = 1, s.prototype.STATE_NOT_AUTHENTICATED = 2, 
    s.prototype.STATE_AUTHENTICATED = 3, s.prototype.STATE_SELECTED = 4, s.prototype.STATE_LOGOUT = 5, 
    s.prototype.TIMEOUT_CONNECTION = 9e4, s.prototype.TIMEOUT_NOOP = 6e4, s.prototype.TIMEOUT_IDLE = 6e4, 
    s.prototype._init = function() {
        this.client.onerror = function(e) {
            this.onerror(e);
        }.bind(this), this.client.oncert = function(e) {
            this.oncert(e);
        }.bind(this), this.client.onclose = function() {
            clearTimeout(this._connectionTimeout), clearTimeout(this._idleTimeout), this.onclose();
        }.bind(this), this.client.onready = this._onReady.bind(this), this.client.onidle = this._onIdle.bind(this), 
        this.client.setHandler("capability", this._untaggedCapabilityHandler.bind(this)), 
        this.client.setHandler("ok", this._untaggedOkHandler.bind(this)), this.client.setHandler("exists", this._untaggedExistsHandler.bind(this)), 
        this.client.setHandler("expunge", this._untaggedExpungeHandler.bind(this)), this.client.setHandler("fetch", this._untaggedFetchHandler.bind(this));
    }, s.prototype.onclose = function() {}, s.prototype.onauth = function() {}, s.prototype.onupdate = function() {}, 
    s.prototype.oncert = function() {}, s.prototype.onselectmailbox = function() {}, 
    s.prototype.onclosemailbox = function() {}, s.prototype._onClose = function() {
        r.debug(a, this.options.sessionId + " connection closed. goodbye."), this.onclose();
    }, s.prototype._onTimeout = function() {
        clearTimeout(this._connectionTimeout);
        var e = new Error(this.options.sessionId + " Timeout creating connection to the IMAP server");
        r.error(a, e), this.onerror(e), this.client._destroy();
    }, s.prototype._onReady = function() {
        clearTimeout(this._connectionTimeout), r.debug(a, this.options.sessionId + " session: connection established"), 
        this._changeState(this.STATE_NOT_AUTHENTICATED), this.updateCapability(function() {
            this.upgradeConnection(function(e) {
                return e ? (this.onerror(e), this.close(), void 0) : (this.updateId(this.options.id, function() {
                    this.login(this.options.auth, function(e) {
                        return e ? (this.onerror(e), this.close(), void 0) : (this.onauth(), void 0);
                    }.bind(this));
                }.bind(this)), void 0);
            }.bind(this));
        }.bind(this));
    }, s.prototype._onIdle = function() {
        this.authenticated && !this._enteredIdle && (r.debug(a, this.options.sessionId + " client: started idling"), 
        this.enterIdle());
    }, s.prototype.connect = function() {
        r.debug(a, this.options.sessionId + " connecting to " + this.client.host + ":" + this.client.port), 
        this._changeState(this.STATE_CONNECTING), clearTimeout(this._connectionTimeout), 
        this._connectionTimeout = setTimeout(this._onTimeout.bind(this), this.TIMEOUT_CONNECTION), 
        this.client.connect();
    }, s.prototype.close = function(e) {
        var t;
        return e || (t = new Promise(function(t, n) {
            e = i(t, n);
        })), r.debug(a, this.options.sessionId + " closing connection"), this._changeState(this.STATE_LOGOUT), 
        this.exec("LOGOUT", function(t) {
            "function" == typeof e && e(t || null), this.client.close();
        }.bind(this)), t;
    }, s.prototype.exec = function() {
        var e = Array.prototype.slice.call(arguments), t = e.pop();
        "function" != typeof t && (e.push(t), t = void 0), e.push(function(e, n) {
            var o = null;
            e && e.capability && (this.capability = e.capability), this.client.isError(e) ? o = e : [ "NO", "BAD" ].indexOf((e && e.command || "").toString().toUpperCase().trim()) >= 0 && (o = new Error(e.humanReadable || "Error"), 
            e.code && (o.code = e.code)), "function" == typeof t ? t(o, e, n) : n();
        }.bind(this)), this.breakIdle(function() {
            this.client.exec.apply(this.client, e);
        }.bind(this));
    }, s.prototype.enterIdle = function() {
        this._enteredIdle || (this._enteredIdle = this.capability.indexOf("IDLE") >= 0 ? "IDLE" : "NOOP", 
        r.debug(a, this.options.sessionId + " entering idle with " + this._enteredIdle), 
        "NOOP" === this._enteredIdle ? this._idleTimeout = setTimeout(function() {
            this.exec("NOOP");
        }.bind(this), this.TIMEOUT_NOOP) : "IDLE" === this._enteredIdle && (this.client.exec({
            command: "IDLE"
        }, function(e, t) {
            t();
        }.bind(this)), this._idleTimeout = setTimeout(function() {
            r.debug(a, this.options.sessionId + " sending idle DONE"), this.client.send("DONE\r\n"), 
            this._enteredIdle = !1;
        }.bind(this), this.TIMEOUT_IDLE)));
    }, s.prototype.breakIdle = function(e) {
        return this._enteredIdle ? (clearTimeout(this._idleTimeout), "IDLE" === this._enteredIdle && (r.debug(a, this.options.sessionId + " sending idle DONE"), 
        this.client.send("DONE\r\n")), this._enteredIdle = !1, r.debug(a, this.options.sessionId + " idle terminated"), 
        e()) : e();
    }, s.prototype.upgradeConnection = function(e) {
        return this.client.secureMode ? e(null, !1) : (this.capability.indexOf("STARTTLS") < 0 || this.options.ignoreTLS) && !this.options.requireTLS ? e(null, !1) : (this.exec("STARTTLS", function(t, n, o) {
            t ? (e(t), o()) : (this.capability = [], this.client.upgrade(function(t, n) {
                this.updateCapability(function() {
                    e(t, n);
                }), o();
            }.bind(this)));
        }.bind(this)), void 0);
    }, s.prototype.updateCapability = function(e, t) {
        return t || "function" != typeof e || (t = e, e = void 0), !e && this.capability.length ? t(null, !1) : !this.client.secureMode && this.options.requireTLS ? t(null, !1) : (this.exec("CAPABILITY", function(e, n, o) {
            e ? t(e) : t(null, !0), o();
        }), void 0);
    }, s.prototype.listNamespaces = function(e) {
        var t;
        return e || (t = new Promise(function(t, n) {
            e = i(t, n);
        })), this.capability.indexOf("NAMESPACE") < 0 ? (setTimeout(function() {
            e(null, !1);
        }, 0), t) : (this.exec("NAMESPACE", "NAMESPACE", function(t, n, o) {
            t ? e(t) : e(null, this._parseNAMESPACE(n)), o();
        }.bind(this)), t);
    }, s.prototype.login = function(e, t) {
        var n, s = {};
        return e ? (this.capability.indexOf("AUTH=XOAUTH2") >= 0 && e && e.xoauth2 ? (n = {
            command: "AUTHENTICATE",
            attributes: [ {
                type: "ATOM",
                value: "XOAUTH2"
            }, {
                type: "ATOM",
                value: this._buildXOAuth2Token(e.user, e.xoauth2),
                sensitive: !0
            } ]
        }, s.onplustagged = function(e, t) {
            var n;
            if (e && e.payload) try {
                n = JSON.parse(o.base64Decode(e.payload));
            } catch (s) {
                r.error(a, this.options.sessionId + " error parsing XOAUTH2 payload: " + s + "\nstack trace: " + s.stack);
            }
            this.client.send("\r\n"), t();
        }.bind(this)) : n = {
            command: "login",
            attributes: [ {
                type: "STRING",
                value: e.user || ""
            }, {
                type: "STRING",
                value: e.pass || "",
                sensitive: !0
            } ]
        }, this.exec(n, "capability", s, function(e, n, o) {
            var s = !1;
            return e ? (t(e), o()) : (this._changeState(this.STATE_AUTHENTICATED), this.authenticated = !0, 
            n.capability && n.capability.length ? (this.capability = [].concat(n.capability || []), 
            s = !0, r.debug(a, this.options.sessionId + " post-auth capabilites updated: " + this.capability), 
            t(null, !0)) : n.payload && n.payload.CAPABILITY && n.payload.CAPABILITY.length ? (this.capability = [].concat(n.payload.CAPABILITY.pop().attributes || []).map(function(e) {
                return (e.value || "").toString().toUpperCase().trim();
            }), s = !0, r.debug(a, this.options.sessionId + " post-auth capabilites updated: " + this.capability), 
            t(null, !0)) : this.updateCapability(!0, function(e) {
                e ? t(e) : (r.debug(a, this.options.sessionId + " post-auth capabilites updated: " + this.capability), 
                t(null, !0));
            }.bind(this)), o(), void 0);
        }.bind(this)), void 0) : t(new Error("Authentication information not provided"));
    }, s.prototype.updateId = function(e, t) {
        if (this.capability.indexOf("ID") < 0) return t(null, !1);
        var n = [ [] ];
        e ? ("string" == typeof e && (e = {
            name: e
        }), Object.keys(e).forEach(function(t) {
            n[0].push(t), n[0].push(e[t]);
        })) : n[0] = null, this.exec({
            command: "ID",
            attributes: n
        }, "ID", function(e, n, o) {
            if (e) return r.error(a, this.options.sessionId + " error updating server id: " + e + "\n" + e.stack), 
            t(e), o();
            if (!n.payload || !n.payload.ID || !n.payload.ID.length) return t(null, !1), o();
            this.serverId = {};
            var s;
            [].concat([].concat(n.payload.ID.shift().attributes || []).shift() || []).forEach(function(e, t) {
                0 === t % 2 ? s = (e && e.value || "").toString().toLowerCase().trim() : this.serverId[s] = (e && e.value || "").toString();
            }.bind(this)), t(null, this.serverId), o();
        }.bind(this));
    }, s.prototype.listMailboxes = function(e) {
        var t;
        return e || (t = new Promise(function(t, n) {
            e = i(t, n);
        })), this.exec({
            command: "LIST",
            attributes: [ "", "*" ]
        }, "LIST", function(t, n, o) {
            if (t) return e(t), o();
            var s = {
                root: !0,
                children: []
            };
            return n.payload && n.payload.LIST && n.payload.LIST.length ? (n.payload.LIST.forEach(function(e) {
                if (e && e.attributes && !(e.attributes.length < 3)) {
                    var t = this._ensurePath(s, (e.attributes[2].value || "").toString(), (e.attributes[1] ? e.attributes[1].value : "/").toString());
                    t.flags = [].concat(e.attributes[0] || []).map(function(e) {
                        return (e.value || "").toString();
                    }), t.listed = !0, this._checkSpecialUse(t);
                }
            }.bind(this)), this.exec({
                command: "LSUB",
                attributes: [ "", "*" ]
            }, "LSUB", function(t, n, o) {
                return t ? (r.error(a, this.options.sessionId + " error while listing subscribed mailboxes: " + t + "\n" + t.stack), 
                e(null, s), o()) : n.payload && n.payload.LSUB && n.payload.LSUB.length ? (n.payload.LSUB.forEach(function(e) {
                    if (e && e.attributes && !(e.attributes.length < 3)) {
                        var t = this._ensurePath(s, (e.attributes[2].value || "").toString(), (e.attributes[1] ? e.attributes[1].value : "/").toString());
                        [].concat(e.attributes[0] || []).map(function(e) {
                            e = (e.value || "").toString(), (!t.flags || t.flags.indexOf(e) < 0) && (t.flags = [].concat(t.flags || []).concat(e));
                        }), t.subscribed = !0;
                    }
                }.bind(this)), e(null, s), o(), void 0) : (e(null, s), o());
            }.bind(this)), o(), void 0) : (e(null, !1), o());
        }.bind(this)), t;
    }, s.prototype.createMailbox = function(e, n) {
        var o;
        return n || (o = new Promise(function(e, t) {
            n = i(e, t);
        })), this.exec({
            command: "CREATE",
            attributes: [ t.imap.encode(e) ]
        }, function(e, t, o) {
            e && "ALREADYEXISTS" === e.code ? n(null, !0) : n(e, !1), o();
        }), o;
    }, s.prototype.listMessages = function(e, t, n, o) {
        var r;
        o || "function" != typeof n || (o = n, n = void 0), o || "function" != typeof t || (o = t, 
        t = void 0), o || (r = new Promise(function(e, t) {
            o = i(e, t);
        })), t = t || {
            fast: !0
        }, n = n || {};
        var s = this._buildFETCHCommand(e, t, n);
        return this.exec(s, "FETCH", {
            precheck: n.precheck,
            ctx: n.ctx
        }, function(e, t, n) {
            e ? o(e) : o(null, this._parseFETCH(t)), n();
        }.bind(this)), r;
    }, s.prototype.search = function(e, t, n) {
        var o;
        n || "function" != typeof t || (n = t, t = void 0), n || (o = new Promise(function(e, t) {
            n = i(e, t);
        })), t = t || {};
        var r = this._buildSEARCHCommand(e, t);
        return this.exec(r, "SEARCH", {
            precheck: t.precheck,
            ctx: t.ctx
        }, function(e, t, o) {
            e ? n(e) : n(null, this._parseSEARCH(t)), o();
        }.bind(this)), o;
    }, s.prototype.setFlags = function(e, t, n, o) {
        var r;
        o || "function" != typeof n || (o = n, n = void 0), o || (r = new Promise(function(e, t) {
            o = i(e, t);
        })), n = n || {};
        var s = this._buildSTORECommand(e, t, n);
        return this.exec(s, "FETCH", {
            precheck: n.precheck,
            ctx: n.ctx
        }, function(e, t, n) {
            e ? o(e) : o(null, this._parseFETCH(t)), n();
        }.bind(this)), r;
    }, s.prototype.upload = function(e, t, n, o) {
        var r;
        o || "function" != typeof n || (o = n, n = void 0), o || (r = new Promise(function(e, t) {
            o = i(e, t);
        })), n = n || {}, n.flags = n.flags || [ "\\Seen" ];
        var s = n.flags.map(function(e) {
            return {
                type: "atom",
                value: e
            };
        }), a = {
            command: "APPEND"
        };
        return a.attributes = [ {
            type: "atom",
            value: e
        }, s, {
            type: "literal",
            value: t
        } ], this.exec(a, {
            precheck: n.precheck,
            ctx: n.ctx
        }, function(e, t, n) {
            o(e, e ? void 0 : !0), n();
        }.bind(this)), r;
    }, s.prototype.deleteMessages = function(e, t, n) {
        var o;
        return n || "function" != typeof t || (n = t, t = void 0), n || (o = new Promise(function(e, t) {
            n = i(e, t);
        })), t = t || {}, this.setFlags(e, {
            add: "\\Deleted"
        }, t, function(o) {
            return o ? n(o) : (this.exec(t.byUid && this.capability.indexOf("UIDPLUS") >= 0 ? {
                command: "UID EXPUNGE",
                attributes: [ {
                    type: "sequence",
                    value: e
                } ]
            } : "EXPUNGE", function(e, t, o) {
                e ? n(e) : n(null, !0), o();
            }.bind(this)), void 0);
        }.bind(this)), o;
    }, s.prototype.copyMessages = function(e, t, n, o) {
        var r;
        return o || "function" != typeof n || (o = n, n = void 0), o || (r = new Promise(function(e, t) {
            o = i(e, t);
        })), n = n || {}, this.exec({
            command: n.byUid ? "UID COPY" : "COPY",
            attributes: [ {
                type: "sequence",
                value: e
            }, {
                type: "atom",
                value: t
            } ]
        }, {
            precheck: n.precheck,
            ctx: n.ctx
        }, function(e, t, n) {
            e ? o(e) : o(null, t.humanReadable || "COPY completed"), n();
        }.bind(this)), r;
    }, s.prototype.moveMessages = function(e, t, n, o) {
        var r;
        return o || "function" != typeof n || (o = n, n = void 0), o || (r = new Promise(function(e, t) {
            o = i(e, t);
        })), n = n || {}, this.capability.indexOf("MOVE") >= 0 ? this.exec({
            command: n.byUid ? "UID MOVE" : "MOVE",
            attributes: [ {
                type: "sequence",
                value: e
            }, {
                type: "atom",
                value: t
            } ]
        }, [ "OK" ], {
            precheck: n.precheck,
            ctx: n.ctx
        }, function(e, t, n) {
            e ? o(e) : o(null, !0), n();
        }.bind(this)) : this.copyMessages(e, t, n, function(t) {
            return t ? o(t) : (delete n.precheck, this.deleteMessages(e, n, o), void 0);
        }.bind(this)), r;
    }, s.prototype.selectMailbox = function(e, t, n) {
        var o;
        n || "function" != typeof t || (n = t, t = void 0), n || (o = new Promise(function(e, t) {
            n = i(e, t);
        })), t = t || {};
        var r = {
            command: t.readOnly ? "EXAMINE" : "SELECT",
            attributes: [ {
                type: "STRING",
                value: e
            } ]
        };
        return t.condstore && this.capability.indexOf("CONDSTORE") >= 0 && r.attributes.push([ {
            type: "ATOM",
            value: "CONDSTORE"
        } ]), this.exec(r, [ "EXISTS", "FLAGS", "OK" ], {
            precheck: t.precheck,
            ctx: t.ctx
        }, function(t, o, r) {
            if (t) return n(t), r();
            this._changeState(this.STATE_SELECTED), this.selectedMailbox && this.selectedMailbox !== e && this.onclosemailbox(this.selectedMailbox), 
            this.selectedMailbox = e;
            var s = this._parseSELECT(o);
            n(null, s), this.onselectmailbox(e, s), r();
        }.bind(this)), o;
    }, s.prototype.hasCapability = function(e) {
        return this.capability.indexOf((e || "").toString().toUpperCase().trim()) >= 0;
    }, s.prototype._untaggedOkHandler = function(e, t) {
        e && e.capability && (this.capability = e.capability), t();
    }, s.prototype._untaggedCapabilityHandler = function(e, t) {
        this.capability = [].concat(e && e.attributes || []).map(function(e) {
            return (e.value || "").toString().toUpperCase().trim();
        }), t();
    }, s.prototype._untaggedExistsHandler = function(e, t) {
        e && e.hasOwnProperty("nr") && this.onupdate("exists", e.nr), t();
    }, s.prototype._untaggedExpungeHandler = function(e, t) {
        e && e.hasOwnProperty("nr") && this.onupdate("expunge", e.nr), t();
    }, s.prototype._untaggedFetchHandler = function(e, t) {
        this.onupdate("fetch", [].concat(this._parseFETCH({
            payload: {
                FETCH: [ e ]
            }
        }) || []).shift()), t();
    }, s.prototype._parseSELECT = function(e) {
        if (e && e.payload) {
            var t = {
                readOnly: "READ-ONLY" === e.code
            }, n = e.payload.EXISTS && e.payload.EXISTS.pop(), o = e.payload.FLAGS && e.payload.FLAGS.pop(), r = e.payload.OK;
            return n && (t.exists = n.nr || 0), o && o.attributes && o.attributes.length && (t.flags = o.attributes[0].map(function(e) {
                return (e.value || "").toString().trim();
            })), [].concat(r || []).forEach(function(e) {
                switch (e && e.code) {
                  case "PERMANENTFLAGS":
                    t.permanentFlags = [].concat(e.permanentflags || []);
                    break;

                  case "UIDVALIDITY":
                    t.uidValidity = Number(e.uidvalidity) || 0;
                    break;

                  case "UIDNEXT":
                    t.uidNext = Number(e.uidnext) || 0;
                    break;

                  case "HIGHESTMODSEQ":
                    t.highestModseq = e.highestmodseq || "0";
                }
            }), t;
        }
    }, s.prototype._parseNAMESPACE = function(e) {
        var t, n = !1, o = function(e) {
            return e ? [].concat(e || []).map(function(e) {
                return e && e.length ? {
                    prefix: e[0].value,
                    delimiter: e[1] && e[1].value
                } : !1;
            }) : !1;
        };
        return e.payload && e.payload.NAMESPACE && e.payload.NAMESPACE.length && (t = [].concat(e.payload.NAMESPACE.pop().attributes || [])).length && (n = {
            personal: o(t[0]),
            users: o(t[1]),
            shared: o(t[2])
        }), n;
    }, s.prototype._buildFETCHCommand = function(e, t, o) {
        var r = {
            command: o.byUid ? "UID FETCH" : "FETCH",
            attributes: [ {
                type: "SEQUENCE",
                value: e
            } ]
        }, s = [];
        return [].concat(t || []).forEach(function(e) {
            var t;
            if (e = (e || "").toString().toUpperCase().trim(), /^\w+$/.test(e)) s.push({
                type: "ATOM",
                value: e
            }); else if (e) try {
                t = n.parser("* Z " + e), s = s.concat(t.attributes || []);
            } catch (o) {
                s.push({
                    type: "ATOM",
                    value: e
                });
            }
        }), 1 === s.length && (s = s.pop()), r.attributes.push(s), o.changedSince && r.attributes.push([ {
            type: "ATOM",
            value: "CHANGEDSINCE"
        }, {
            type: "ATOM",
            value: o.changedSince
        } ]), r;
    }, s.prototype._parseFETCH = function(e) {
        var t;
        return e && e.payload && e.payload.FETCH && e.payload.FETCH.length ? t = [].concat(e.payload.FETCH || []).map(function(e) {
            var t, o, r, s = [].concat([].concat(e.attributes || [])[0] || []), i = {
                "#": e.nr
            };
            for (t = 0, o = s.length; o > t; t++) 0 !== t % 2 ? i[r] = this._parseFetchValue(r, s[t]) : r = n.compiler({
                attributes: [ s[t] ]
            }).toLowerCase().replace(/<\d+>$/, "");
            return i;
        }.bind(this)) : [];
    }, s.prototype._parseFetchValue = function(e, t) {
        if (!t) return null;
        if (!Array.isArray(t)) {
            switch (e) {
              case "uid":
              case "rfc822.size":
                return Number(t.value) || 0;

              case "modseq":
                return t.value || "0";
            }
            return t.value;
        }
        switch (e) {
          case "flags":
            t = [].concat(t).map(function(e) {
                return e.value || "";
            });
            break;

          case "envelope":
            t = this._parseENVELOPE([].concat(t || []));
            break;

          case "bodystructure":
            t = this._parseBODYSTRUCTURE([].concat(t || []));
            break;

          case "modseq":
            t = (t.shift() || {}).value || "0";
        }
        return t;
    }, s.prototype._parseENVELOPE = function(e) {
        var t = function(e) {
            return [].concat(e || []).map(function(e) {
                return {
                    name: o.mimeWordsDecode(e[0] && e[0].value || ""),
                    address: (e[2] && e[2].value || "") + "@" + (e[3] && e[3].value || "")
                };
            });
        }, n = {};
        return e[0] && e[0].value && (n.date = e[0].value), e[1] && e[1].value && (n.subject = o.mimeWordsDecode(e[1] && e[1].value)), 
        e[2] && e[2].length && (n.from = t(e[2])), e[3] && e[3].length && (n.sender = t(e[3])), 
        e[4] && e[4].length && (n["reply-to"] = t(e[4])), e[5] && e[5].length && (n.to = t(e[5])), 
        e[6] && e[6].length && (n.cc = t(e[6])), e[7] && e[7].length && (n.bcc = t(e[7])), 
        e[8] && e[8].value && (n["in-reply-to"] = e[8].value), e[9] && e[9].value && (n["message-id"] = e[9].value), 
        n;
    }, s.prototype._parseBODYSTRUCTURE = function(e) {
        var t = this, n = function(e, r) {
            r = r || [];
            var s, i = {}, a = 0, c = 0;
            if (r.length && (i.part = r.join(".")), Array.isArray(e[0])) {
                for (i.childNodes = []; Array.isArray(e[a]); ) i.childNodes.push(n(e[a], r.concat(++c))), 
                a++;
                i.type = "multipart/" + ((e[a++] || {}).value || "").toString().toLowerCase(), a < e.length - 1 && (e[a] && (i.parameters = {}, 
                [].concat(e[a] || []).forEach(function(e, t) {
                    t % 2 ? i.parameters[s] = o.mimeWordsDecode((e && e.value || "").toString()) : s = (e && e.value || "").toString().toLowerCase();
                })), a++);
            } else i.type = [ ((e[a++] || {}).value || "").toString().toLowerCase(), ((e[a++] || {}).value || "").toString().toLowerCase() ].join("/"), 
            e[a] && (i.parameters = {}, [].concat(e[a] || []).forEach(function(e, t) {
                t % 2 ? i.parameters[s] = o.mimeWordsDecode((e && e.value || "").toString()) : s = (e && e.value || "").toString().toLowerCase();
            })), a++, e[a] && (i.id = ((e[a] || {}).value || "").toString()), a++, e[a] && (i.description = ((e[a] || {}).value || "").toString()), 
            a++, e[a] && (i.encoding = ((e[a] || {}).value || "").toString().toLowerCase()), 
            a++, e[a] && (i.size = Number((e[a] || {}).value || 0) || 0), a++, "message/rfc822" === i.type ? (e[a] && (i.envelope = t._parseENVELOPE([].concat(e[a] || []))), 
            a++, e[a] && (i.childNodes = [ n(e[a], r) ]), a++, e[a] && (i.lineCount = Number((e[a] || {}).value || 0) || 0), 
            a++) : /^text\//.test(i.type) && (e[a] && (i.lineCount = Number((e[a] || {}).value || 0) || 0), 
            a++), a < e.length - 1 && (e[a] && (i.md5 = ((e[a] || {}).value || "").toString().toLowerCase()), 
            a++);
            return a < e.length - 1 && (Array.isArray(e[a]) && e[a].length && (i.disposition = ((e[a][0] || {}).value || "").toString().toLowerCase(), 
            Array.isArray(e[a][1]) && (i.dispositionParameters = {}, [].concat(e[a][1] || []).forEach(function(e, t) {
                t % 2 ? i.dispositionParameters[s] = o.mimeWordsDecode((e && e.value || "").toString()) : s = (e && e.value || "").toString().toLowerCase();
            }))), a++), a < e.length - 1 && (e[a] && (i.language = [].concat(e[a] || []).map(function(e) {
                return (e && e.value || "").toString().toLowerCase();
            })), a++), a < e.length - 1 && (e[a] && (i.location = ((e[a] || {}).value || "").toString()), 
            a++), i;
        };
        return n(e);
    }, s.prototype._buildSEARCHCommand = function(e, t) {
        var n = {
            command: t.byUid ? "UID SEARCH" : "SEARCH"
        }, r = !0, s = function(e) {
            var t = [];
            return Object.keys(e).forEach(function(n) {
                var i = [], a = function(e) {
                    return e.toUTCString().replace(/^\w+, 0?(\d+) (\w+) (\d+).*/, "$1-$2-$3");
                }, c = function(e) {
                    return "number" == typeof e ? {
                        type: "number",
                        value: e
                    } : "string" == typeof e ? /[\u0080-\uFFFF]/.test(e) ? (r = !1, {
                        type: "literal",
                        value: o.fromTypedArray(o.charset.encode(e))
                    }) : {
                        type: "string",
                        value: e
                    } : "[object Date]" === Object.prototype.toString.call(e) ? {
                        type: "atom",
                        value: a(e)
                    } : Array.isArray(e) ? e.map(c) : "object" == typeof e ? s(e) : void 0;
                };
                i.push({
                    type: "atom",
                    value: n.toUpperCase()
                }), [].concat(e[n] || []).forEach(function(e) {
                    switch (n.toLowerCase()) {
                      case "uid":
                        e = {
                            type: "sequence",
                            value: e
                        };
                        break;

                      default:
                        e = c(e);
                    }
                    e && (i = i.concat(e || []));
                }), t = t.concat(i || []);
            }), t;
        };
        return n.attributes = [].concat(s(e || {}) || []), r || (n.attributes.unshift({
            type: "atom",
            value: "UTF-8"
        }), n.attributes.unshift({
            type: "atom",
            value: "CHARSET"
        })), n;
    }, s.prototype._parseSEARCH = function(e) {
        var t = [];
        return e && e.payload && e.payload.SEARCH && e.payload.SEARCH.length ? ([].concat(e.payload.SEARCH || []).forEach(function(e) {
            [].concat(e.attributes || []).forEach(function(e) {
                e = Number(e && e.value || e || 0) || 0, t.indexOf(e) < 0 && t.push(e);
            });
        }.bind(this)), t.sort(function(e, t) {
            return e - t;
        }), t) : [];
    }, s.prototype._buildSTORECommand = function(e, t, n) {
        var o = {
            command: n.byUid ? "UID STORE" : "STORE",
            attributes: [ {
                type: "sequence",
                value: e
            } ]
        }, r = "", s = [];
        return (Array.isArray(t) || "object" != typeof t) && (t = {
            set: t
        }), t.add ? (s = [].concat(t.add || []), r = "+") : t.set ? (r = "", s = [].concat(t.set || [])) : t.remove && (r = "-", 
        s = [].concat(t.remove || [])), o.attributes.push({
            type: "atom",
            value: r + "FLAGS" + (n.silent ? ".SILENT" : "")
        }), o.attributes.push(s.map(function(e) {
            return {
                type: "atom",
                value: e
            };
        })), o;
    }, s.prototype._changeState = function(e) {
        e !== this.state && (r.debug(a, this.options.sessionId + " entering state: " + this.state), 
        this.state === this.STATE_SELECTED && this.selectedMailbox && (this.onclosemailbox(this.selectedMailbox), 
        this.selectedMailbox = !1), this.state = e);
    }, s.prototype._ensurePath = function(e, n, o) {
        var r, s, i, a = n.split(o), c = e;
        for (r = 0; r < a.length; r++) {
            for (i = !1, s = 0; s < c.children.length; s++) if (this._compareMailboxNames(c.children[s].name, t.imap.decode(a[r]))) {
                c = c.children[s], i = !0;
                break;
            }
            i || (c.children.push({
                name: t.imap.decode(a[r]),
                delimiter: o,
                path: a.slice(0, r + 1).join(o),
                children: []
            }), c = c.children[c.children.length - 1]);
        }
        return c;
    }, s.prototype._compareMailboxNames = function(e, t) {
        return ("INBOX" === e.toUpperCase() ? "INBOX" : e) === ("INBOX" === t.toUpperCase() ? "INBOX" : t);
    }, s.prototype._checkSpecialUse = function(e) {
        var t, n;
        if (e.flags) for (t = 0; t < c.length; t++) if (n = c[t], (e.flags || []).indexOf(n) >= 0) return e.specialUse = n, 
        n;
        return this._checkSpecialUseByName(e);
    }, s.prototype._checkSpecialUseByName = function(e) {
        var t, n, o = (e.name || "").toLowerCase().trim();
        for (t = 0; t < u.length; t++) if (n = u[t], d[n].indexOf(o) >= 0) return e.specialUse = n, 
        e.specialUseFlag = n, n;
        return !1;
    }, s.prototype._buildXOAuth2Token = function(e, t) {
        var n = [ "user=" + (e || ""), "auth=Bearer " + t, "", "" ];
        return o.base64.encode(n.join(""));
    }, s;
}), define("oauth", [ "require", "exports", "module", "./errorutils", "./syncbase", "logic", "./date" ], function(e, t) {
    function n(e) {
        return s(d, "renewing-access-token"), new Promise(function(t, n) {
            e._transientLastRenew = i.PERFNOW();
            var a = s.interceptable("oauth:renew-xhr", function() {
                return new XMLHttpRequest({
                    mozSystem: !0
                });
            });
            a.open("POST", e.tokenEndpoint, !0), a.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"), 
            a.timeout = r.CONNECT_TIMEOUT_MS, a.send([ "client_id=", encodeURIComponent(e.clientId), "&client_secret=", encodeURIComponent(e.clientSecret), "&refresh_token=", encodeURIComponent(e.refreshToken), "&grant_type=refresh_token" ].join("")), 
            a.onload = function() {
                if (a.status < 200 || a.status >= 300) {
                    try {
                        var o = JSON.parse(a.responseText);
                    } catch (r) {}
                    s(d, "xhr-fail", {
                        tokenEndpoint: e.tokenEndpoint,
                        status: a.status,
                        errResp: o
                    }), n("needs-oauth-reauth");
                } else try {
                    var u = JSON.parse(a.responseText);
                    if (u && u.access_token) {
                        s(d, "got-access-token", {
                            _accessToken: u.access_token
                        });
                        var l = 1e3 * u.expires_in, h = i.NOW() + Math.max(0, l - c);
                        t({
                            accessToken: u.access_token,
                            expireTimeMS: h
                        });
                    } else s(d, "no-access-token", {
                        data: a.responseText
                    }), n("needs-oauth-reauth");
                } catch (p) {
                    s(d, "bad-json", {
                        error: p,
                        data: a.responseText
                    }), n("needs-oauth-reauth");
                }
            }, a.onerror = function(e) {
                n(o.analyzeException(e));
            }, a.ontimeout = function() {
                n("unresponsive-server");
            };
        });
    }
    var o = e("./errorutils"), r = e("./syncbase"), s = e("logic"), i = e("./date"), a = 18e5, c = 3e4, d = s.scope("Oauth");
    t.isRenewPossible = function(e) {
        var t = e.oauth2, n = t && (t._transientLastRenew || 0), o = i.PERFNOW();
        return t ? !t || n && a > o - n ? !1 : !0 : !1;
    }, t.ensureUpdatedCredentials = function(e, t, o) {
        o && console.log("ensureUpdatedCredentials: force renewing token");
        var r = e.oauth2;
        return r && (!r.accessToken || r.expireTimeMS < i.NOW()) || o ? n(r).then(function(n) {
            r.accessToken = n.accessToken, r.expireTimeMS = n.expireTimeMS, s(d, "credentials-changed", {
                _accessToken: r.accessToken,
                expireTimeMS: r.expireTimeMS
            }), t && t(e);
        }) : (s(d, "credentials-ok"), Promise.resolve(!1));
    };
}), define("imap/client", [ "require", "exports", "module", "browserbox", "browserbox-imap", "imap-handler", "logic", "../syncbase", "../errorutils", "../oauth" ], function(e, t) {
    function n() {}
    function o(e, t) {
        if (!e || !e.response) return null;
        if (e.command && e.command.request && "STARTTLS" === e.command.request.command) return "bad-security";
        var n = t && !!t.options.auth.xoauth2, o = e.response, r = (o.code || "") + (o.humanReadable || "");
        return /Your account is not enabled for IMAP use/.test(r) || /IMAP access is disabled for your domain/.test(r) ? "imap-disabled" : /UNAVAILABLE/.test(r) ? "server-maintenance" : -1 == t.capability.indexOf("LOGINDISABLED") || t.authenticated ? /AUTHENTICATIONFAILED/.test(r) || /Invalid credentials/i.test(r) || /login failed/i.test(r) || /password/.test(r) || !t.authenticated ? n ? "needs-oauth-reauth" : "bad-user-or-pass" : null : "server-maintenance";
    }
    var r = e("browserbox"), s = e("browserbox-imap"), i = e("imap-handler"), a = e("logic"), c = e("../syncbase"), d = e("../errorutils"), u = e("../oauth"), l = window.setTimeout, h = window.clearTimeout;
    t.setTimeoutFunctions = function(e, t) {
        l = e, h = t;
    };
    var p = a.scope("ImapClient");
    t.createImapConnection = function(e, o, s) {
        var i;
        return u.ensureUpdatedCredentials(e, s).then(function() {
            return new Promise(function(t, s) {
                i = new r(o.hostname, o.port, {
                    auth: {
                        user: e.username,
                        pass: e.password,
                        xoauth2: e.oauth2 ? e.oauth2.accessToken : null
                    },
                    id: {
                        vendor: "Mozilla",
                        name: "GaiaMail",
                        version: "0.2",
                        "support-url": "http://mzl.la/file-gaia-email-bug"
                    },
                    useSecureTransport: "ssl" === o.crypto || o.crypto === !0,
                    requireTLS: "starttls" === o.crypto,
                    ignoreTLS: "plain" === o.crypto
                });
                var d = l(function() {
                    i.onerror("unresponsive-server"), i.close();
                }, c.CONNECT_TIMEOUT_MS);
                i.onauth = function() {
                    h(d), a(p, "connected", {
                        connInfo: o
                    }), i.onauth = i.onerror = n, t(i);
                }, i.onerror = function(e) {
                    h(d), s(e);
                }, i.connect();
            });
        }).catch(function(n) {
            var r = g(i, n);
            if (i && i.close(), "needs-oauth-reauth" === r && u.isRenewPossible(e)) return u.ensureUpdatedCredentials(e, s, !0).then(function() {
                return t.createImapConnection(e, o, s);
            });
            throw a(p, "connect-error", {
                error: r
            }), r;
        });
    };
    var f = s.prototype._processResponse;
    s.prototype._processResponse = function(e) {
        f.apply(this, arguments);
        var t = (e && e.command || "").toString().toUpperCase().trim();
        -1 !== [ "NO", "BAD" ].indexOf(t) && (a(p, "protocol-error", {
            humanReadable: e.humanReadable,
            responseCode: e.code,
            commandData: this._currentCommand && this._currentCommand.request && i.compiler(this._currentCommand.request)
        }), this._lastImapError = {
            command: this._currentCommand,
            response: e
        });
    }, s.prototype._onError = function(e) {
        this.isError(e) ? this.onerror(e) : e && this.isError(e.data) ? this.onerror(e.data) : this.onerror(e && e.data && e.data.message || e.data || e || "Error"), 
        this.close();
    };
    var g = t.normalizeImapError = function(e, t) {
        var n = d.analyzeException(t), r = e && o(e.client._lastImapError, e), s = n || r || "unknown";
        return a(p, "normalized-error", {
            error: t,
            errorName: t && t.name,
            errorMessage: t && t.message,
            errorStack: t && t.stack,
            socketLevelError: n,
            protocolLevelError: r,
            reportAs: s
        }), s;
    };
}), define("imap/account", [ "logic", "../a64", "../accountmixins", "../allback", "../errbackoff", "../mailslice", "../searchfilter", "../syncbase", "../util", "../composite/incoming", "./folder", "./jobs", "./client", "../errorutils", "../disaster-recovery", "module", "require", "exports" ], function(e, t, n, o, r, s, i, a, c, d, u, l, h, p, f, g, m, y) {
    function _(t, n, o, s, i, a, c, d) {
        e.defineScope(this, "Account", {
            accountId: o,
            accountType: "imap"
        }), v.apply(this, [ u.ImapFolderSyncer ].concat(Array.slice(arguments))), this._maxConnsAllowed = 3, 
        this._pendingConn = null, this._ownedConns = [], this._demandedConns = [], this._backoffEndpoint = r.createEndpoint("imap:" + this.id, this), 
        this._connInfo = i, d && this._reuseConnection(d), this._jobDriver = new l.ImapJobDriver(this, this._folderInfos.$mutationState), 
        this._TEST_doNotCloseFolder = !1, this.ensureEssentialOfflineFolders();
    }
    c.bsearchForInsert, o.allbackMaker;
    var v = d.CompositeIncomingAccount;
    y.Account = y.ImapAccount = _, _.prototype = Object.create(v.prototype);
    var b = {
        type: "imap",
        supportsServerFolders: !0,
        toString: function() {
            return "[ImapAccount: " + this.id + "]";
        },
        get isGmail() {
            return -1 !== this.meta.capability.indexOf("X-GM-EXT-1");
        },
        get isCoreMailServer() {
            return -1 !== this.meta.capability.indexOf("X-CM-EXT-1");
        },
        get isOutLookServer() {
            return -1 !== this._connInfo.hostname.indexOf("outlook.com");
        },
        get sentMessagesAutomaticallyAppearInSentFolder() {
            return this.isGmail || this.isCoreMailServer || this.isOutLookServer;
        },
        get numActiveConns() {
            return this._ownedConns.length;
        },
        __folderDemandsConnection: function(e, t, n, o, r) {
            if (r && !this.universe.online) return window.setZeroTimeout(o), void 0;
            var s = {
                folderId: e,
                label: t,
                callback: n,
                deathback: o,
                dieOnConnectFailure: Boolean(r)
            };
            this._demandedConns.push(s), this._demandedConns.length > 1 || this._allocateExistingConnection() || this._makeConnectionIfPossible();
        },
        _killDieOnConnectFailureDemands: function() {
            for (var e = 0; e < this._demandedConns.length; e++) {
                var t = this._demandedConns[e];
                t.dieOnConnectFailure && (t.deathback.call(null), this._demandedConns.splice(e--, 1));
            }
        },
        _allocateExistingConnection: function() {
            if (!this._demandedConns.length) return !1;
            for (var t = this._demandedConns[0], n = 0; n < this._ownedConns.length; n++) {
                var o = this._ownedConns[n];
                if (t.folderId && o.folderId === t.folderId && e(this, "folderAlreadyHasConn", {
                    folderId: t.folderId
                }), !o.inUseBy) return o.inUseBy = t, this._demandedConns.shift(), e(this, "reuseConnection", {
                    folderId: t.folderId,
                    label: t.label
                }), t.callback(o.conn), !0;
            }
            return !1;
        },
        allOperationsCompleted: function() {
            this.maybeCloseUnusedConnections();
        },
        maybeCloseUnusedConnections: function() {
            !a.KILL_CONNECTIONS_WHEN_JOBLESS || this._demandedConns.length || this.universe.areServerJobsWaiting(this) || this.closeUnusedConnections();
        },
        closeUnusedConnections: function() {
            for (var t = this._ownedConns.length - 1; t >= 0; t--) {
                var n = this._ownedConns[t];
                n.inUseBy || (console.log("Killing unused IMAP connection."), this._ownedConns.splice(t, 1), 
                n.conn.client.close(), e(this, "deadConnection", {
                    reason: "unused"
                }));
            }
        },
        _makeConnectionIfPossible: function() {
            if (this._ownedConns.length >= this._maxConnsAllowed) return e(this, "maximumConnsNoNew"), 
            void 0;
            if (!this._pendingConn) {
                this._pendingConn = !0;
                var t = this._makeConnection.bind(this);
                this._backoffEndpoint.scheduleConnectAttempt(t);
            }
        },
        _makeConnection: function(t, n, o) {
            this._pendingConn = !0, m([ "./client" ], function(r) {
                e(this, "createConnection", {
                    folderId: n,
                    label: o
                }), r.createImapConnection(this._credentials, this._connInfo, function() {
                    return new Promise(function(e) {
                        this.universe.saveAccountDef(this.compositeAccount.accountDef, null, e);
                    }.bind(this));
                }.bind(this)).then(function(e) {
                    f.associateSocketWithAccount(e.client.socket, this), this._pendingConn = null, this._bindConnectionDeathHandlers(e), 
                    this._backoffEndpoint.noteConnectSuccess(), this._ownedConns.push({
                        conn: e,
                        inUseBy: null
                    }), this._allocateExistingConnection(), this._demandedConns.length && this._makeConnectionIfPossible(), 
                    t && t(null);
                }.bind(this)).catch(function(o) {
                    e(this, "deadConnection", {
                        reason: "connect-error",
                        folderId: n
                    }), p.shouldReportProblem(o) && this.universe.__reportAccountProblem(this.compositeAccount, o, "incoming"), 
                    this._pendingConn = null, t && t(o), p.shouldRetry(o) ? this._backoffEndpoint.noteConnectFailureMaybeRetry(p.wasErrorFromReachableState(o)) ? this._makeConnectionIfPossible() : this._killDieOnConnectFailureDemands() : (this._backoffEndpoint.noteBrokenConnection(), 
                    this._killDieOnConnectFailureDemands());
                }.bind(this));
            }.bind(this));
        },
        _reuseConnection: function(e) {
            f.associateSocketWithAccount(e.client.socket, this), this._ownedConns.push({
                conn: e,
                inUseBy: null
            }), this._bindConnectionDeathHandlers(e);
        },
        _bindConnectionDeathHandlers: function(t) {
            t.breakIdle(function() {
                t.client.TIMEOUT_ENTER_IDLE = a.STALE_CONNECTION_TIMEOUT_MS, t.client.onidle = function() {
                    console.warn("Killing stale IMAP connection."), t.client.close();
                }, t.client._enterIdle();
            }), t.onclose = function() {
                for (var n = 0; n < this._ownedConns.length; n++) {
                    var o = this._ownedConns[n];
                    if (o.conn === t) return e(this, "deadConnection", {
                        reason: "closed",
                        folderId: o.inUseBy && o.inUseBy.folderId
                    }), o.inUseBy && o.inUseBy.deathback && o.inUseBy.deathback(t), o.inUseBy = null, 
                    this._ownedConns.splice(n, 1), void 0;
                }
            }.bind(this), t.onerror = function(n) {
                n = h.normalizeImapError(t, n), e(this, "connectionError", {
                    error: n
                }), console.error("imap:onerror", JSON.stringify({
                    error: n,
                    host: this._connInfo.hostname,
                    port: this._connInfo.port
                }));
            }.bind(this);
        },
        __folderDoneWithConnection: function(t, n, o) {
            for (var r = 0; r < this._ownedConns.length; r++) {
                var s = this._ownedConns[r];
                if (s.conn === t) return o && this._backoffEndpoint(s.inUseBy.folderId), e(this, "releaseConnection", {
                    folderId: s.inUseBy.folderId,
                    label: s.inUseBy.label
                }), s.inUseBy = null, this.maybeCloseUnusedConnections(), void 0;
            }
            e(this, "connectionMismatch");
        },
        _syncFolderList: function(e, t) {
            e.listMailboxes(this._syncFolderComputeDeltas.bind(this, e, t));
        },
        _determineFolderType: function(e, t) {
            var n = (e.flags || []).map(function(e) {
                return e.substr(1).toUpperCase();
            }), o = null;
            if (-1 !== n.indexOf("NOSELECT")) o = "nomail"; else {
                for (var r = 0; r < n.length; r++) switch (n[r]) {
                  case "ALL":
                  case "ALLMAIL":
                  case "ARCHIVE":
                    o = "archive";
                    break;

                  case "DRAFTS":
                    o = "drafts";
                    break;

                  case "FLAGGED":
                    o = "starred";
                    break;

                  case "IMPORTANT":
                    o = "important";
                    break;

                  case "INBOX":
                    o = "inbox";
                    break;

                  case "JUNK":
                    o = "junk";
                    break;

                  case "SENT":
                    o = "sent";
                    break;

                  case "SPAM":
                    o = "junk";
                    break;

                  case "STARRED":
                    o = "starred";
                    break;

                  case "TRASH":
                    o = "trash";
                    break;

                  case "HASCHILDREN":
                  case "HASNOCHILDREN":
                  case "MARKED":
                  case "UNMARKED":
                  case "NOINFERIORS":                }
                if (!o) {
                    var s = this._namespaces.personal[0] && this._namespaces.personal[0].prefix, i = t === s + e.name;
                    if (i || t === e.name) switch (e.name.toUpperCase()) {
                      case "DRAFT":
                      case "DRAFTS":
                        o = "drafts";
                        break;

                      case "INBOX":
                        "INBOX" === t.toUpperCase() && (o = "inbox");
                        break;

                      case "BULK MAIL":
                      case "JUNK":
                      case "SPAM":
                        o = "junk";
                        break;

                      case "SENT":
                        o = "sent";
                        break;

                      case "TRASH":
                        o = "trash";
                        break;

                      case "UNSENT MESSAGES":
                        o = "queue";
                    }
                }
                o || (o = "normal");
            }
            return o;
        },
        _namespaces: {
            personal: {
                prefix: "",
                delimiter: "/"
            },
            provisional: !0
        },
        _syncFolderComputeDeltas: function(t, n, o, r) {
            function i(t, n, o) {
                t.forEach(function(t) {
                    var r;
                    t.name;
                    var s = t.delimiter || "/";
                    0 === t.path.indexOf(s) && (t.path = t.path.slice(s.length));
                    var c = t.path, u = a._determineFolderType(t, c);
                    "inbox" === u && (c = "INBOX"), d.hasOwnProperty(c) ? (r = d[c], r.name = t.name, 
                    r.delim = s, e(l, "folder-sync:existing", {
                        type: u,
                        name: t.name,
                        path: c,
                        delim: s
                    }), d[c] = !0) : (e(l, "folder-sync:add", {
                        type: u,
                        name: t.name,
                        path: c,
                        delim: s
                    }), r = a._learnAboutFolder(t.name, c, o, u, s, n)), t.children && i(t.children, n + 1, r.id);
                });
            }
            var a = this;
            if (o) return n(o), void 0;
            if (a._namespaces.provisional) return t.listNamespaces(function(o, s) {
                !o && s && (a._namespaces = s), a._namespaces.provisional = !1, e(a, "list-namespaces", {
                    namespaces: s
                }), a._syncFolderComputeDeltas(t, n, o, r);
            }), void 0;
            for (var c, d = {}, u = 0; u < this.folders.length; u++) c = this.folders[u], d[c.path] = c;
            var l = e.scope("ImapFolderSync");
            i(r.children, 0, null);
            for (var h in d) c = d[h], c !== !0 && (s.FolderStorage.isTypeLocalOnly(c.type) || (e(l, "delete-dead-folder", {
                folderType: c.type,
                folderId: c.id
            }), this._forgetFolder(c.id)));
            this.ensureEssentialOnlineFolders(), this.normalizeFolderHierarchy(), n(null);
        },
        ensureEssentialOfflineFolders: function() {
            [ "outbox", "localdrafts" ].forEach(function(e) {
                this.getFirstFolderWithType(e) || this._learnAboutFolder(e, e, null, e, "", 0, !0);
            }, this);
        },
        ensureEssentialOnlineFolders: function(e) {
            var t = {
                trash: "Trash",
                sent: "Sent"
            }, n = o.latch();
            for (var r in t) this.getFirstFolderWithType(r) || this.universe.createFolder(this.id, null, t[r], r, !1, n.defer());
            n.then(e);
        },
        normalizeFolderHierarchy: n.normalizeFolderHierarchy,
        saveSentMessage: function(e) {
            this.sentMessagesAutomaticallyAppearInSentFolder || e.withMessageBlob({
                includeBcc: !0
            }, function(e) {
                var t = {
                    messageText: e,
                    flags: [ "\\Seen" ]
                }, n = this.getFirstFolderWithType("sent");
                n && this.universe.appendMessages(n.id, [ t ]);
            }.bind(this));
        },
        shutdown: function(e) {
            function t() {
                0 === --n && e();
            }
            v.prototype.shutdownFolders.call(this), this._backoffEndpoint.shutdown();
            for (var n = this._ownedConns.length, o = 0; o < this._ownedConns.length; o++) {
                var r = this._ownedConns[o];
                if (e) {
                    r.inUseBy = {
                        deathback: t
                    };
                    try {
                        r.conn.client.close();
                    } catch (s) {
                        n--;
                    }
                } else r.conn.client.close();
            }
            !n && e && e();
        },
        checkAccount: function(t) {
            e(this, "checkAccount_begin"), this._makeConnection(function(n) {
                e(this, "checkAccount_end", {
                    error: n
                }), t(n);
            }.bind(this), null, "check");
        },
        accountDeleted: function() {
            this._alive = !1, this.shutdown();
        }
    };
    for (var S in b) Object.defineProperty(_.prototype, S, Object.getOwnPropertyDescriptor(b, S));
}), define("md5", [ "require", "exports", "module" ], function(e, t, n) {
    function o(e) {
        return s(r(i(e)));
    }
    function r(e) {
        return c(d(a(e), 8 * e.length));
    }
    function s(e) {
        try {} catch (t) {
            y = 0;
        }
        for (var n, o = y ? "0123456789ABCDEF" : "0123456789abcdef", r = "", s = 0; s < e.length; s++) n = e.charCodeAt(s), 
        r += o.charAt(15 & n >>> 4) + o.charAt(15 & n);
        return r;
    }
    function i(e) {
        for (var t, n, o = "", r = -1; ++r < e.length; ) t = e.charCodeAt(r), n = r + 1 < e.length ? e.charCodeAt(r + 1) : 0, 
        t >= 55296 && 56319 >= t && n >= 56320 && 57343 >= n && (t = 65536 + ((1023 & t) << 10) + (1023 & n), 
        r++), 127 >= t ? o += String.fromCharCode(t) : 2047 >= t ? o += String.fromCharCode(192 | 31 & t >>> 6, 128 | 63 & t) : 65535 >= t ? o += String.fromCharCode(224 | 15 & t >>> 12, 128 | 63 & t >>> 6, 128 | 63 & t) : 2097151 >= t && (o += String.fromCharCode(240 | 7 & t >>> 18, 128 | 63 & t >>> 12, 128 | 63 & t >>> 6, 128 | 63 & t));
        return o;
    }
    function a(e) {
        for (var t = Array(e.length >> 2), n = 0; n < t.length; n++) t[n] = 0;
        for (var n = 0; n < 8 * e.length; n += 8) t[n >> 5] |= (255 & e.charCodeAt(n / 8)) << n % 32;
        return t;
    }
    function c(e) {
        for (var t = "", n = 0; n < 32 * e.length; n += 8) t += String.fromCharCode(255 & e[n >> 5] >>> n % 32);
        return t;
    }
    function d(e, t) {
        e[t >> 5] |= 128 << t % 32, e[(t + 64 >>> 9 << 4) + 14] = t;
        for (var n = 1732584193, o = -271733879, r = -1732584194, s = 271733878, i = 0; i < e.length; i += 16) {
            var a = n, c = o, d = r, u = s;
            n = l(n, o, r, s, e[i + 0], 7, -680876936), s = l(s, n, o, r, e[i + 1], 12, -389564586), 
            r = l(r, s, n, o, e[i + 2], 17, 606105819), o = l(o, r, s, n, e[i + 3], 22, -1044525330), 
            n = l(n, o, r, s, e[i + 4], 7, -176418897), s = l(s, n, o, r, e[i + 5], 12, 1200080426), 
            r = l(r, s, n, o, e[i + 6], 17, -1473231341), o = l(o, r, s, n, e[i + 7], 22, -45705983), 
            n = l(n, o, r, s, e[i + 8], 7, 1770035416), s = l(s, n, o, r, e[i + 9], 12, -1958414417), 
            r = l(r, s, n, o, e[i + 10], 17, -42063), o = l(o, r, s, n, e[i + 11], 22, -1990404162), 
            n = l(n, o, r, s, e[i + 12], 7, 1804603682), s = l(s, n, o, r, e[i + 13], 12, -40341101), 
            r = l(r, s, n, o, e[i + 14], 17, -1502002290), o = l(o, r, s, n, e[i + 15], 22, 1236535329), 
            n = h(n, o, r, s, e[i + 1], 5, -165796510), s = h(s, n, o, r, e[i + 6], 9, -1069501632), 
            r = h(r, s, n, o, e[i + 11], 14, 643717713), o = h(o, r, s, n, e[i + 0], 20, -373897302), 
            n = h(n, o, r, s, e[i + 5], 5, -701558691), s = h(s, n, o, r, e[i + 10], 9, 38016083), 
            r = h(r, s, n, o, e[i + 15], 14, -660478335), o = h(o, r, s, n, e[i + 4], 20, -405537848), 
            n = h(n, o, r, s, e[i + 9], 5, 568446438), s = h(s, n, o, r, e[i + 14], 9, -1019803690), 
            r = h(r, s, n, o, e[i + 3], 14, -187363961), o = h(o, r, s, n, e[i + 8], 20, 1163531501), 
            n = h(n, o, r, s, e[i + 13], 5, -1444681467), s = h(s, n, o, r, e[i + 2], 9, -51403784), 
            r = h(r, s, n, o, e[i + 7], 14, 1735328473), o = h(o, r, s, n, e[i + 12], 20, -1926607734), 
            n = p(n, o, r, s, e[i + 5], 4, -378558), s = p(s, n, o, r, e[i + 8], 11, -2022574463), 
            r = p(r, s, n, o, e[i + 11], 16, 1839030562), o = p(o, r, s, n, e[i + 14], 23, -35309556), 
            n = p(n, o, r, s, e[i + 1], 4, -1530992060), s = p(s, n, o, r, e[i + 4], 11, 1272893353), 
            r = p(r, s, n, o, e[i + 7], 16, -155497632), o = p(o, r, s, n, e[i + 10], 23, -1094730640), 
            n = p(n, o, r, s, e[i + 13], 4, 681279174), s = p(s, n, o, r, e[i + 0], 11, -358537222), 
            r = p(r, s, n, o, e[i + 3], 16, -722521979), o = p(o, r, s, n, e[i + 6], 23, 76029189), 
            n = p(n, o, r, s, e[i + 9], 4, -640364487), s = p(s, n, o, r, e[i + 12], 11, -421815835), 
            r = p(r, s, n, o, e[i + 15], 16, 530742520), o = p(o, r, s, n, e[i + 2], 23, -995338651), 
            n = f(n, o, r, s, e[i + 0], 6, -198630844), s = f(s, n, o, r, e[i + 7], 10, 1126891415), 
            r = f(r, s, n, o, e[i + 14], 15, -1416354905), o = f(o, r, s, n, e[i + 5], 21, -57434055), 
            n = f(n, o, r, s, e[i + 12], 6, 1700485571), s = f(s, n, o, r, e[i + 3], 10, -1894986606), 
            r = f(r, s, n, o, e[i + 10], 15, -1051523), o = f(o, r, s, n, e[i + 1], 21, -2054922799), 
            n = f(n, o, r, s, e[i + 8], 6, 1873313359), s = f(s, n, o, r, e[i + 15], 10, -30611744), 
            r = f(r, s, n, o, e[i + 6], 15, -1560198380), o = f(o, r, s, n, e[i + 13], 21, 1309151649), 
            n = f(n, o, r, s, e[i + 4], 6, -145523070), s = f(s, n, o, r, e[i + 11], 10, -1120210379), 
            r = f(r, s, n, o, e[i + 2], 15, 718787259), o = f(o, r, s, n, e[i + 9], 21, -343485551), 
            n = g(n, a), o = g(o, c), r = g(r, d), s = g(s, u);
        }
        return Array(n, o, r, s);
    }
    function u(e, t, n, o, r, s) {
        return g(m(g(g(t, e), g(o, s)), r), n);
    }
    function l(e, t, n, o, r, s, i) {
        return u(t & n | ~t & o, e, t, r, s, i);
    }
    function h(e, t, n, o, r, s, i) {
        return u(t & o | n & ~o, e, t, r, s, i);
    }
    function p(e, t, n, o, r, s, i) {
        return u(t ^ n ^ o, e, t, r, s, i);
    }
    function f(e, t, n, o, r, s, i) {
        return u(n ^ (t | ~o), e, t, r, s, i);
    }
    function g(e, t) {
        var n = (65535 & e) + (65535 & t), o = (e >> 16) + (t >> 16) + (n >> 16);
        return o << 16 | 65535 & n;
    }
    function m(e, t) {
        return e << t | e >>> 32 - t;
    }
    n.exports = function(e) {
        return o(e);
    };
    var y = 0;
}), define("pop3/transport", [ "mimefuncs", "exports" ], function(e, t) {
    function n(e, t) {
        var n = new Uint8Array(e.length + t.length);
        return n.set(e, 0), n.set(t, e.length), n;
    }
    function o() {
        this.buffer = new Uint8Array(0), this.unprocessedLines = [];
    }
    function r(e, t) {
        this.lines = e, this.isMultiline = t, this.ok = this.lines[0][0] === u, this.err = !this.ok, 
        this.request = null;
    }
    function s(e, t, n, o) {
        this.command = e, this.args = t, this.expectMultiline = n, this.onresponse = o || null;
    }
    function i() {
        this.parser = new o(), this.onsend = function() {
            throw new Error("You must implement Pop3Protocol.onsend to send data.");
        }, this.unsentRequests = [], this.pipeline = !1, this.pendingRequests = [], this.closed = !1;
    }
    window.setTimeout.bind(window), window.clearTimeout.bind(window);
    var a = "\r".charCodeAt(0), c = "\n".charCodeAt(0), d = ".".charCodeAt(0), u = "+".charCodeAt(0);
    "-".charCodeAt(0), " ".charCodeAt(0);
    var l = new TextEncoder("utf-8", {
        fatal: !1
    });
    o.prototype.push = function(e) {
        for (var t = this.buffer = n(this.buffer, e), o = 0; o < t.length - 1; o++) if (t[o] === a && t[o + 1] === c) {
            var r = o + 1;
            this.unprocessedLines.push(t.subarray(0, r + 1)), t = this.buffer = t.subarray(r + 1), 
            o = -1;
        }
    }, o.prototype.extractResponse = function(e) {
        if (!this.unprocessedLines.length) return null;
        if (this.unprocessedLines[0][0] !== u && (e = !1), e) {
            for (var t = -1, n = 1; n < this.unprocessedLines.length; n++) {
                var o = this.unprocessedLines[n];
                if (3 === o.byteLength && o[0] === d && o[1] === a && o[2] === c) {
                    t = n;
                    break;
                }
            }
            if (-1 === t) return null;
            var s = this.unprocessedLines.splice(0, t + 1);
            s.pop();
            for (var n = 1; t > n; n++) s[n][0] === d && (s[n] = s[n].subarray(1));
            return new r(s, !0);
        }
        return new r([ this.unprocessedLines.shift() ], !1);
    }, r.prototype.getStatusLine = function() {
        return this.getLineAsString(0).replace(/^(\+OK|-ERR) /, "");
    }, r.prototype.getLineAsString = function(t) {
        return e.fromTypedArray(this.lines[t]);
    }, r.prototype.getLinesAsString = function() {
        for (var e = [], t = 0; t < this.lines.length; t++) e.push(this.getLineAsString(t));
        return e;
    }, r.prototype.getDataLines = function() {
        for (var e = [], t = 1; t < this.lines.length; t++) {
            var n = this.getLineAsString(t);
            e.push(n.slice(0, n.length - 2));
        }
        return e;
    }, r.prototype.getDataAsString = function() {
        for (var e = [], t = 1; t < this.lines.length; t++) e.push(this.getLineAsString(t));
        return e.join("");
    }, r.prototype.toString = function() {
        return this.getLinesAsString().join("\r\n");
    }, t.Request = s, s.prototype.toByteArray = function() {
        return l.encode(this.command + (this.args.length ? " " + this.args.join(" ") : "") + "\r\n");
    }, s.prototype._respondWithError = function(e) {
        var t = new r([ l.encode("-ERR " + e + "\r\n") ], !1);
        t.request = this, this.onresponse(t, null);
    }, t.Response = r, t.Pop3Protocol = i, i.prototype.sendRequest = function(e, t, n, o) {
        var r;
        return r = e instanceof s ? e : new s(e, t, n, o), this.closed ? (r._respondWithError("(request sent after connection closed)"), 
        void 0) : (this.pipeline || 0 === this.pendingRequests.length ? (this.onsend(r.toByteArray()), 
        this.pendingRequests.push(r)) : this.unsentRequests.push(r), void 0);
    }, i.prototype.onreceive = function(e) {
        this.parser.push(new Uint8Array(e.data));
        for (var t; ;) {
            var n = this.pendingRequests[0];
            if (t = this.parser.extractResponse(n && n.expectMultiline), !t) break;
            if (!n) {
                console.error("Unsolicited response from server: " + t);
                break;
            }
            t.request = n, this.pendingRequests.shift(), this.unsentRequests.length && this.sendRequest(this.unsentRequests.shift()), 
            n.onresponse && (t.err ? n.onresponse(t, null) : n.onresponse(null, t));
        }
    }, i.prototype.onclose = function() {
        this.closed = !0;
        var e = this.pendingRequests.concat(this.unsentRequests);
        this.pendingRequests = [], this.unsentRequests = [];
        for (var t = 0; t < e.length; t++) {
            var n = e[t];
            n._respondWithError("(connection closed, no response)");
        }
    };
}), define("imap/imapchew", [ "mimefuncs", "../db/mail_rep", "../mailchew", "mimeparser", "exports" ], function(e, t, n, o, r) {
    function s(t) {
        var n = /^([^']*)'([^']*)'(.+)$/.exec(t);
        return n ? e.mimeWordsDecode("=?" + (n[1] || "us-ascii") + "?Q?" + n[3].replace(/%/g, "=") + "?=") : null;
    }
    function i(e) {
        return Array.isArray(e) ? e.map(i) : e && "<" === e[0] ? e.slice(1, -1) : e;
    }
    function a(e, t) {
        return e.headers[t] && e.headers[t][0] || null;
    }
    function c(n) {
        function o(e) {
            var t = e.encoding.toLowerCase();
            return "base64" === t ? Math.floor(57 * e.size / 78) : "quoted-printable" === t ? e.size : e.size;
        }
        function r(n, h) {
            var p, f, g, m = n.type.split("/")[0], y = n.type.split("/")[1];
            if ("multipart" !== m) {
                if (f = n.parameters && n.parameters.name ? e.mimeWordsDecode(n.parameters.name) : n.parameters && n.parameters["name*"] ? s(n.parameters["name*"]) : n.dispositionParameters && n.dispositionParameters.filename ? e.mimeWordsDecode(n.dispositionParameters.filename) : n.dispositionParameters && n.dispositionParameters["filename*"] ? s(n.dispositionParameters["filename*"]) : null, 
                g = n.disposition ? "inline" == n.disposition.toLowerCase() ? "text" === m || n.id ? "inline" : "attachment" : "attachment" == n.disposition.toLowerCase() ? "attachment" : "inline" : "related" === h && n.id && "image" === m ? "inline" : f || "text" !== m && "message" !== m ? "attachment" : "inline", 
                "text" !== m && "image" !== m && "message" !== m && (g = "attachment"), "application" === m && ("pgp-signature" === y || "pkcs7-signature" === y)) return !0;
                var _ = function(e, n) {
                    return t.makeAttachmentPart({
                        name: n || "unnamed-" + ++u,
                        contentId: e.id ? i(e.id) : null,
                        type: e.type.toLowerCase(),
                        part: e.part,
                        encoding: e.encoding && e.encoding.toLowerCase(),
                        sizeEstimate: o(e),
                        file: null
                    });
                }, v = function(e) {
                    return t.makeBodyPart({
                        type: y,
                        part: e.part || "1",
                        sizeEstimate: e.size,
                        amountDownloaded: 0,
                        isDownloaded: 0 === e.size,
                        _partInfo: e.size ? {
                            partID: e.part,
                            type: m,
                            subtype: y,
                            params: d(e.parameters),
                            encoding: e.encoding && e.encoding.toLowerCase()
                        } : null,
                        content: ""
                    });
                };
                if ("attachment" === g) return a.push(_(n, f)), !0;
                switch (m) {
                  case "image":
                    return l.push(_(n, f)), !0;

                  case "text":
                    if ("plain" === y || "html" === y) return c.push(v(n)), !0;
                }
                return !1;
            }
            switch (y) {
              case "alternative":
                for (p = n.childNodes.length - 1; p >= 0; p--) {
                    var b = n.childNodes[p], S = b.type.split("/")[0], T = b.type.split("/")[1];
                    switch (S) {
                      case "text":
                        break;

                      case "multipart":
                        if (r(b)) return !0;
                        break;

                      default:
                        continue;
                    }
                    switch (T) {
                      case "html":
                      case "plain":
                        if (r(b), y) return !0;
                    }
                }
                return !1;

              case "mixed":
              case "signed":
              case "related":
              case "report":
                for (p = 0; p < n.childNodes.length; p++) r(n.childNodes[p], y);
                return !0;

              default:
                return console.warn("Ignoring multipart type:", y), !1;
            }
        }
        var a = [], c = [], u = 0, l = [];
        return r(n.bodystructure), {
            bodyReps: c,
            attachments: a,
            relatedParts: l
        };
    }
    function d(e) {
        if (Array.isArray(e)) return e.map(d);
        if (e && "object" == typeof e) {
            if ("value" in e) return e.value;
            var t = {};
            for (var n in e) t[n] = d(e[n]);
            return t;
        }
        return e && "object" == typeof e ? e : void 0 !== e ? e : null;
    }
    r.chewHeaderAndBodyStructure = function(e, n, r) {
        var s = c(e);
        e.date = e.internaldate && f(e.internaldate), e.headers = {};
        for (var u in e) if (/header\.fields/.test(u)) {
            var l = new o();
            l.write(e[u] + "\r\n"), l.end(), e.headers = l.node.headers;
            break;
        }
        var h = d(a(e, "from")), p = d(a(e, "references"));
        return {
            header: t.makeHeaderInfo({
                id: r,
                srvid: e.uid,
                suid: n + "/" + r,
                guid: i(d(a(e, "message-id"))),
                author: h && h[0] || {
                    address: "missing-address@example.com"
                },
                to: d(a(e, "to")),
                cc: d(a(e, "cc")),
                bcc: d(a(e, "bcc")),
                replyTo: d(a(e, "reply-to")),
                date: e.date,
                flags: e.flags || [],
                hasAttachments: s.attachments.length > 0,
                subject: d(a(e, "subject")),
                snippet: null
            }),
            bodyInfo: t.makeBodyInfo({
                date: e.date,
                size: 0,
                attachments: s.attachments,
                relatedParts: s.relatedParts,
                references: p ? i(p.split(/\s+/)) : null,
                bodyReps: s.bodyReps
            })
        };
    }, r.updateMessageWithFetch = function(e, t, o, r) {
        var s = t.bodyReps[o.bodyRepIndex];
        (!o.bytes || r.bytesFetched < o.bytes[1]) && (s.isDownloaded = !0, s._partInfo = null), 
        !s.isDownloaded && r.buffer && (s._partInfo.pendingBuffer = r.buffer), s.amountDownloaded += r.bytesFetched;
        var i = n.processMessageContent(r.text, s.type, s.isDownloaded, o.createSnippet);
        o.createSnippet && (e.snippet = i.snippet), s.isDownloaded && (s.content = i.content);
    }, r.selectSnippetBodyRep = function(e, t) {
        if (e.snippet) return -1;
        for (var n = t.bodyReps, o = n.length, s = 0; o > s; s++) if (r.canBodyRepFillSnippet(n[s])) return s;
        return -1;
    }, r.canBodyRepFillSnippet = function(e) {
        return e && "plain" === e.type || "html" === e.type;
    }, r.calculateBytesToDownloadForImapBodyDisplay = function(e) {
        var t = 0;
        return e.bodyReps.forEach(function(e) {
            e.isDownloaded || (t += e.sizeEstimate - e.amountDownloaded);
        }), e.relatedParts.forEach(function(e) {
            e.file || (t += e.sizeEstimate);
        }), t;
    };
    var u = /^( ?\d|\d{2})-(.{3})-(\d{4}) (\d{2}):(\d{2}):(\d{2})(?: ([+-]\d{4}))?$/, l = 36e5, h = 6e4, p = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ], f = r.parseImapDateTime = function(e) {
        var t = u.exec(e);
        if (!t) throw new Error("Not a good IMAP date-time: " + e);
        var n = parseInt(t[1], 10), o = p.indexOf(t[2]), r = parseInt(t[3], 10), s = parseInt(t[4], 10), i = parseInt(t[5], 10), a = parseInt(t[6], 10), c = Date.UTC(r, o, n, s, i, a), d = t[7] ? parseInt(t[7], 10) : 0, f = Math.floor(d / 100), g = d % 100;
        return c -= f * l + g * h;
    };
    r.formatImapDateTime = function(e) {
        var t;
        return t = (e.getDate() < 10 ? " " : "") + e.getDate() + "-" + p[e.getMonth()] + "-" + e.getFullYear() + " " + ("0" + e.getHours()).slice(-2) + ":" + ("0" + e.getMinutes()).slice(-2) + ":" + ("0" + e.getSeconds()).slice(-2) + (e.getTimezoneOffset() > 0 ? " -" : " +") + ("0" + Math.abs(e.getTimezoneOffset()) / 60).slice(-2) + ("0" + Math.abs(e.getTimezoneOffset()) % 60).slice(-2);
    };
}), define("pop3/mime_mapper", [], function() {
    return {
        _typeToExtensionMap: {
            "image/jpeg": "jpg",
            "image/png": "png",
            "image/gif": "gif",
            "image/bmp": "bmp",
            "audio/mpeg": "mp3",
            "audio/mp4": "m4a",
            "audio/ogg": "ogg",
            "audio/webm": "webm",
            "audio/3gpp": "3gp",
            "audio/amr": "amr",
            "video/mp4": "mp4",
            "video/mpeg": "mpg",
            "video/ogg": "ogg",
            "video/webm": "webm",
            "video/3gpp": "3gp",
            "application/vcard": "vcf",
            "text/vcard": "vcf",
            "text/x-vcard": "vcf"
        },
        _extensionToTypeMap: {
            jpg: "image/jpeg",
            jpeg: "image/jpeg",
            jpe: "image/jpeg",
            png: "image/png",
            gif: "image/gif",
            bmp: "image/bmp",
            mp3: "audio/mpeg",
            m4a: "audio/mp4",
            m4b: "audio/mp4",
            m4p: "audio/mp4",
            m4r: "audio/mp4",
            aac: "audio/aac",
            opus: "audio/ogg",
            amr: "audio/amr",
            mp4: "video/mp4",
            mpeg: "video/mpeg",
            mpg: "video/mpeg",
            ogv: "video/ogg",
            ogx: "video/ogg",
            webm: "video/webm",
            "3gp": "video/3gpp",
            ogg: "video/ogg",
            vcf: "text/vcard"
        },
        _parseExtension: function(e) {
            var t = e.split(".");
            return t.length > 1 ? t.pop() : "";
        },
        isSupportedType: function(e) {
            return e in this._typeToExtensionMap;
        },
        isSupportedExtension: function(e) {
            return e in this._extensionToTypeMap;
        },
        isFilenameMatchesType: function(e, t) {
            var n = this._parseExtension(e), o = this.guessTypeFromExtension(n);
            return o == t;
        },
        guessExtensionFromType: function(e) {
            return this._typeToExtensionMap[e];
        },
        guessTypeFromExtension: function(e) {
            return this._extensionToTypeMap[e];
        },
        guessTypeFromFileProperties: function(e, t) {
            var n = this._parseExtension(e), o = this.isSupportedType(t) ? t : this.guessTypeFromExtension(n);
            return o || "";
        },
        ensureFilenameMatchesType: function(e, t) {
            if (!this.isFilenameMatchesType(e, t)) {
                var n = this.guessExtensionFromType(t);
                n && (e += "." + n);
            }
            return e;
        }
    };
}), define("pop3/pop3", [ "module", "exports", "logic", "tcp-socket", "md5", "./transport", "mimeparser", "imap/imapchew", "syncbase", "date", "mimefuncs", "./mime_mapper", "allback" ], function(e, t, n, o, r, s, i, a, c, d, u, l, h) {
    function p(e, t, n) {
        var o = e.headers[t];
        return o && o[0] ? o[0].value : n || null;
    }
    function f(e, t) {
        var n = e.headers[t];
        return n && n[0] ? n[0].params || {} : {};
    }
    function g(e, t, n, o) {
        var r = {};
        r.part = t || "1", r.type = e.contentType.value, r.parameters = f(e, "content-type");
        var s = p(e, "content-disposition");
        if (s && (r.disposition = s, r.dispositionParameters = f(e, "content-disposition")), 
        r.id = p(e, "content-id"), r.encoding = "binary", r.size = e.content && e.content.length || 0, 
        r.description = null, r.lines = null, r.md5 = null, r.childNodes = [], null != e.content || /^multipart\//.test(r.type) || (e.content = new Uint8Array()), 
        null != e.content && (n[r.part] = e.content, o === e && (n.partial = r.part)), e._childNodes.length) for (var i = 0; i < e._childNodes.length; i++) {
            var a = e._childNodes[i];
            r.childNodes.push(g(a, r.part + "." + (i + 1), n, o));
        }
        return r;
    }
    var m = window.setTimeout.bind(window), y = window.clearTimeout.bind(window);
    t.setTimeoutFunctions = function(e, t) {
        m = e, y = t;
    };
    var _ = t.Pop3Client = function(e, t) {
        if (this.options = e = e || {}, e.host = e.host || null, e.username = e.username || null, 
        e.password = e.password || null, e.port = e.port || null, e.crypto = e.crypto || !1, 
        e.connTimeout = e.connTimeout || 3e4, e.debug = e.debug || !1, e.authMethods = [ "apop", "sasl", "user-pass" ], 
        n.defineScope(this, "Pop3Client"), e.preferredAuthMethod) {
            var r = e.authMethods.indexOf(e.preferredAuthMethod);
            -1 !== r && e.authMethods.splice(r, 1), e.authMethods.unshift(e.preferredAuthMethod);
        }
        if (e.crypto === !0 ? e.crypto = "ssl" : e.crypto || (e.crypto = "plain"), !e.port && (e.port = {
            plain: 110,
            starttls: 110,
            ssl: 995
        }[e.crypto], !e.port)) throw new Error("Invalid crypto option for Pop3Client: " + e.crypto);
        this.state = "disconnected", this.authMethod = null, this.idToUidl = {}, this.uidlToId = {}, 
        this.idToSize = {}, this._messageList = null, this._greetingLine = null, this.protocol = new s.Pop3Protocol(), 
        this.socket = o.open(e.host, e.port, {
            useSecureTransport: "ssl" === e.crypto || e.crypto === !0
        });
        var i = m(function() {
            this.state = "disconnected", i && (y(i), i = null), t && t({
                scope: "connection",
                request: null,
                name: "unresponsive-server",
                message: "Could not connect to " + e.host + ":" + e.port + " with " + e.crypto + " encryption."
            });
        }.bind(this), e.connTimeout);
        this.socket.ondata = this.protocol.onreceive.bind(this.protocol), this.protocol.onsend = this.socket.send.bind(this.socket), 
        this.socket.onopen = function() {
            console.log("pop3:onopen"), i && (y(i), i = null), this.state = "greeting";
        }.bind(this), this.socket.onerror = function(e) {
            var n = e && e.data || e;
            console.log("pop3:onerror", n), i && (y(i), i = null), t && t({
                scope: "connection",
                request: null,
                name: "unresponsive-server",
                message: "Socket exception: " + JSON.stringify(n),
                exception: n
            });
        }.bind(this), this.onclose = null, this.socket.onclose = function() {
            console.log("pop3:onclose"), this.protocol.onclose(), this.close(), this.onclose && this.onclose();
        }.bind(this), this.protocol.pendingRequests.push(new s.Request(null, [], !1, function(e, n) {
            return e ? (t && t({
                scope: "connection",
                request: null,
                name: "unresponsive-server",
                message: e.getStatusLine(),
                response: e
            }), void 0) : (this._greetingLine = n.getLineAsString(0), this._maybeUpgradeConnection(function(e) {
                return e ? (t && t(e), void 0) : (this._thenAuthorize(function(e) {
                    e || (this.state = "ready"), t && t(e);
                }), void 0);
            }.bind(this)), void 0);
        }.bind(this)));
    };
    _.prototype.close = _.prototype.die = function() {
        "disconnected" !== this.state && (this.state = "disconnected", this.socket.close());
    }, _.prototype._getCapabilities = function() {
        this.protocol.sendRequest("CAPA", [], !0, function(e, t) {
            if (e) this.capabilities = {}; else for (var n = t.getDataLines(), o = 0; o < n.length; o++) {
                var r = n[o].split(" ");
                this.capabilities[r[0]] = r.slice(1);
            }
        }.bind(this));
    }, _.prototype._maybeUpgradeConnection = function(e) {
        "starttls" === this.options.crypto ? (this.state = "starttls", this.protocol.sendRequest("STLS", [], !1, function(t) {
            return t ? (e && e({
                scope: "connection",
                request: t.request,
                name: "bad-security",
                message: t.getStatusLine(),
                response: t
            }), void 0) : (this.socket.upgradeToSecure(), e(), void 0);
        }.bind(this))) : e();
    }, _.prototype._thenAuthorize = function(e) {
        this.state = "authorization", this.authMethod = this.options.authMethods.shift();
        var t, n = this.options.username, o = this.options.password;
        switch (this.authMethod) {
          case "apop":
            var s = /<.*?>/.exec(this._greetingLine || ""), i = s && s[0];
            i ? (t = r(i + o).toLowerCase(), this.protocol.sendRequest("APOP", [ n, t ], !1, function(t) {
                t ? (this._greetingLine = null, this._thenAuthorize(e)) : e();
            }.bind(this))) : this._thenAuthorize(e);
            break;

          case "sasl":
            t = btoa(n + "\0" + n + "\0" + o), this.protocol.sendRequest("AUTH", [ "PLAIN", t ], !1, function(t) {
                t ? this._thenAuthorize(e) : e();
            }.bind(this));
            break;

          case "user-pass":
          default:
            this.protocol.sendRequest("USER", [ n ], !1, function(t) {
                return t ? (e && e({
                    scope: "authentication",
                    request: t.request,
                    name: "bad-user-or-pass",
                    message: t.getStatusLine(),
                    response: t
                }), void 0) : (this.protocol.sendRequest("PASS", [ o ], !1, function(t) {
                    return t ? (e && e({
                        scope: "authentication",
                        request: null,
                        name: "bad-user-or-pass",
                        message: t.getStatusLine(),
                        response: t
                    }), void 0) : (e(), void 0);
                }.bind(this)), void 0);
            }.bind(this));
        }
    }, _.prototype.quit = function(e) {
        this.state = "disconnected", this.protocol.sendRequest("QUIT", [], !1, function(t) {
            this.close(), t ? e && e({
                scope: "mailbox",
                request: t.request,
                name: "server-problem",
                message: t.getStatusLine(),
                response: t
            }) : e && e();
        }.bind(this));
    }, _.prototype._loadMessageList = function(e) {
        return this._messageList ? (e(null, this._messageList), void 0) : (this.protocol.sendRequest("UIDL", [], !0, function(t, n) {
            if (t) return e && e({
                scope: "mailbox",
                request: t.request,
                name: "server-problem",
                message: t.getStatusLine(),
                response: t
            }), void 0;
            for (var o = n.getDataLines(), r = 0; r < o.length; r++) {
                var s = o[r].split(" "), i = s[0], a = s[1];
                this.idToUidl[i] = a, this.uidlToId[a] = i;
            }
        }.bind(this)), this.protocol.sendRequest("LIST", [], !0, function(t, n) {
            if (t) return e && e({
                scope: "mailbox",
                request: t.request,
                name: "server-problem",
                message: t.getStatusLine(),
                response: t
            }), void 0;
            for (var o = n.getDataLines(), r = [], s = 0; s < o.length; s++) {
                var i = o[s].split(" "), a = i[0], c = parseInt(i[1], 10);
                this.idToSize[a] = c, r.unshift({
                    uidl: this.idToUidl[a],
                    size: c,
                    number: a
                });
            }
            this._messageList = r, e && e(null, r);
        }.bind(this)), void 0);
    }, _.prototype.listMessages = function(e, t) {
        var n = e.filter, o = e.progress, r = e.checkpointInterval || null, s = e.maxMessages || 1/0, i = e.checkpoint, a = [];
        this._loadMessageList(function(e, c) {
            if (e) return t && t(e), void 0;
            for (var d = 0, u = 0, l = [], p = 0, f = 0; f < c.length; f++) {
                var g = c[f];
                !n || n(g.uidl) ? l.length < s ? (d += g.size, l.push(g)) : a.push(g) : p++;
            }
            console.log("POP3: listMessages found " + l.length + " new, " + a.length + " overflow, and " + p + " seen messages. New UIDLs:"), 
            l.forEach(function(e) {
                console.log("POP3: " + e.size + " bytes: " + e.uidl);
            });
            var m = l.length;
            r || (r = m);
            var y = null, _ = function() {
                if (console.log("POP3: Next batch. Messages left: " + l.length), !l.length || this.protocol.closed) return console.log("POP3: Sync complete. " + m + " messages synced, " + a.length + " overflow messages."), 
                t && t(y, m, a), void 0;
                var e = l.splice(0, r), n = h.latch();
                e.forEach(function(e) {
                    var t = n.defer(e.number);
                    this.downloadPartialMessageByNumber(e.number, function(n, r) {
                        u += e.size, n ? y || (y = n) : o && o({
                            totalBytes: d,
                            bytesFetched: u,
                            size: e.size,
                            message: r
                        }), t(n);
                    });
                }.bind(this)), n.then(function(e) {
                    var t = !1;
                    for (var n in e) if (console.log("result", n, e[n]), !e[n][0]) {
                        t = !0;
                        break;
                    }
                    i && t ? (console.log("POP3: Checkpoint."), i(_)) : _();
                });
            }.bind(this);
            _();
        }.bind(this));
    }, _.prototype.downloadMessageByUidl = function(e, t) {
        this._loadMessageList(function(n) {
            n ? t && t(n) : this.downloadMessageByNumber(this.uidlToId[e], t);
        }.bind(this));
    }, _.prototype.downloadPartialMessageByNumber = function(e, t) {
        var n = Math.floor(c.POP3_SNIPPET_SIZE_GOAL / 80);
        this.protocol.sendRequest("TOP", [ e, n ], !0, function(n, o) {
            if (n) return t && t({
                scope: "message",
                request: n.request,
                name: "server-problem",
                message: n.getStatusLine(),
                response: n
            }), void 0;
            var r = this.idToSize[e], s = o.getDataAsString(), i = !r || s.length < r;
            t(null, this.parseMime(s, i, e));
        }.bind(this));
    }, _.prototype.downloadMessageByNumber = function(e, t) {
        this.protocol.sendRequest("RETR", [ e ], !0, function(n, o) {
            return n ? (t && t({
                scope: "message",
                request: n.request,
                name: "server-problem",
                message: n.getStatusLine(),
                response: n
            }), void 0) : (t(null, this.parseMime(o.getDataAsString(), !1, e)), void 0);
        }.bind(this));
    }, _.parseMime = function(e) {
        return _.prototype.parseMime.call(this, e);
    }, _.prototype.parseMime = function(e, t, n) {
        var o, r = new i();
        for (r.write(u.charset.encode(e, "utf-8")), r.end(), o = r.node; o._currentChild && o !== o._currentChild; ) o = o._currentChild;
        var s, l, h = r.node, f = t ? o : null, m = n && this.idToSize[n] || e.length, y = p(h, "date"), _ = d.NOW();
        y ? (l = Date.parse(y), (isNaN(l) || l > _) && (l = _)) : l = _;
        var v = [];
        for (var b in h.headers) v.push(b + ": " + h.headers[b][0].initial + "\r\n");
        var S = {}, T = {
            uid: n && this.idToUidl[n],
            "header.fields[]": v.join(""),
            internaldate: l && a.formatImapDateTime(new Date(l)),
            flags: [],
            bodystructure: g(h, "1", S, f)
        }, w = a.chewHeaderAndBodyStructure(T, null, null), I = a.selectSnippetBodyRep(w.header, w.bodyInfo), E = {}, C = 0, A = S.partial;
        for (var k in S) "partial" !== k && k !== A && (C += S[k].length, E[k] = S[k].length);
        A && (E[A] = m - C);
        for (var M = 0; M < w.bodyInfo.bodyReps.length; M++) {
            var x = w.bodyInfo.bodyReps[M];
            s = u.charset.decode(S[x.part], "utf-8");
            var N = {
                bytes: A === x.part ? [ -1, -1 ] : null,
                bodyRepIndex: M,
                createSnippet: M === I
            };
            if (null != s) {
                x.size = E[x.part];
                var O = {
                    bytesFetched: s.length,
                    text: s
                };
                a.updateMessageWithFetch(w.header, w.bodyInfo, N, O);
            }
        }
        for (var M = 0; M < w.bodyInfo.relatedParts.length; M++) {
            var D = w.bodyInfo.relatedParts[M];
            D.sizeEstimate = E[D.part], s = S[D.part], null != s && A !== D.part && (D.file = new Blob([ s ], {
                type: D.type
            }));
        }
        for (var M = 0; M < w.bodyInfo.attachments.length; M++) {
            var F = w.bodyInfo.attachments[M];
            s = S[F.part], F.sizeEstimate = E[F.part], null != s && A !== F.part && (F.file = new Blob([ s ], {
                type: F.type
            }));
        }
        return t && !w.header.hasAttachments && (p(h, "x-ms-has-attach") || /multipart\/mixed/.test(h.contentType.value) || m > c.POP3_INFER_ATTACHMENTS_SIZE) && (w.header.hasAttachments = !0), 
        w.bodyInfo.bodyReps.push({
            type: "fake",
            part: "fake",
            sizeEstimate: 0,
            amountDownloaded: 0,
            isDownloaded: !t,
            content: null,
            size: 0
        }), w.header.bytesToDownloadForBodyDisplay = t ? m : 0, w;
    };
}), define("pop3/sync", [ "logic", "../util", "module", "require", "exports", "../mailchew", "../syncbase", "../date", "../jobmixins", "../allback", "./pop3" ], function(e, t, n, o, r, s, i, a, c, d) {
    function u(t, n) {
        this.account = t, this.storage = n, e.defineScope(this, "Pop3FolderSyncer", {
            accountId: t.id,
            folderId: n.folderId
        }), this.isInbox = "inbox" === n.folderMeta.type;
    }
    function l(e, t, n, r) {
        return function() {
            var s = Array.slice(arguments);
            o([], function() {
                var o = function() {
                    return this.isInbox ? (this.account.withConnection(function(e, n, o) {
                        var i = s[t];
                        e ? i && i(e) : (s[t] = function(e) {
                            o(), i && i(e);
                        }, r.apply(this, [ n ].concat(s)));
                    }.bind(this), n), void 0) : (r.apply(this, [ null ].concat(s)), void 0);
                }.bind(this);
                e && this.account._conn && "disconnected" !== this.account._conn.state ? this.account._conn.quit(o) : o();
            }.bind(this));
        };
    }
    function h(e) {
        for (var t = [], n = 0; e > n; n++) t.push(n);
        return t;
    }
    var p = 1;
    r.Pop3FolderSyncer = u, u.prototype = {
        syncable: !0,
        get canGrowSync() {
            return this.isInbox;
        },
        downloadBodies: l(!1, 2, "downloadBodies", function(e, t, n, o) {
            var r = d.latch();
            this.storage;
            for (var s = 0; s < t.length; s++) t[s] && null == t[s].snippet && this.downloadBodyReps(t[s], n, r.defer(s));
            r.then(function(e) {
                var n = null;
                for (var r in e) n = e[r][0];
                o(n, t.length);
            });
        }),
        downloadBodyReps: l(!1, 2, "downloadBodyReps", function(e, t, n, o) {
            n instanceof Function && (o = n, n = {}), console.log("POP3: Downloading bodyReps for UIDL " + t.srvid), 
            e.downloadMessageByUidl(t.srvid, function(e, n) {
                if (e) return o(e), void 0;
                t.bytesToDownloadForBodyDisplay = n.header.bytesToDownloadForBodyDisplay, console.log("POP3: Storing message " + t.srvid + " with " + t.bytesToDownloadForBodyDisplay + " bytesToDownload.");
                var r = n.bodyInfo.attachments.length > 0;
                this.storeMessage(t, n.bodyInfo, {
                    flush: r
                }, function() {
                    o && o(null, n.bodyInfo, r);
                });
            }.bind(this));
        }),
        downloadMessageAttachments: function(e, t, n) {
            console.log("POP3: ERROR: downloadMessageAttachments called and POP3 shouldn't do that."), 
            n(null, null);
        },
        storeMessage: function(e, t, n, o) {
            o = o || function() {};
            var r = {
                changeDetails: {}
            }, s = this.getMessageIdForUidl(e.srvid);
            null == e.id && (e.id = null == s ? this.storage._issueNewHeaderId() : s, e.suid = this.storage.folderId + "/" + e.id, 
            e.guid = e.guid || e.srvid);
            for (var i = d.latch(), a = this, u = 0; u < t.attachments.length; u++) {
                var l = t.attachments[u];
                if (l.file instanceof Blob) {
                    console.log("Saving attachment", l.file);
                    var p = !0;
                    c.saveToDeviceStorage(a, l.file, "sdcard", p, l.name, l, i.defer());
                }
            }
            i.then(function() {
                if (i = d.latch(), null == s) a.storeMessageUidlForMessageId(e.srvid, e.id), a.storage.addMessageHeader(e, t, i.defer()), 
                a.storage.addMessageBody(e, t, i.defer()); else {
                    a.storage.updateMessageHeader(e.date, e.id, !0, e, t, i.defer()), r.changeDetails.attachments = h(t.attachments.length), 
                    r.changeDetails.bodyReps = h(t.bodyReps.length);
                    var c = {};
                    n.flush && (c.flushBecause = "blobs"), a.storage.updateMessageBody(e, t, c, r, i.defer());
                }
                i.then(function() {
                    o(null, t);
                });
            });
        },
        get inboxMeta() {
            return this.inboxMeta = this.account.getFolderMetaForFolderId(this.account.getFirstFolderWithType("inbox").id);
        },
        getMessageIdForUidl: function(e) {
            return null == e ? null : (this.inboxMeta.uidlMap = this.inboxMeta.uidlMap || {}, 
            this.inboxMeta.uidlMap[e]);
        },
        storeMessageUidlForMessageId: function(e, t) {
            this.inboxMeta.uidlMap = this.inboxMeta.uidlMap || {}, this.inboxMeta.uidlMap[e] = t, 
            this.inboxMeta.overflowMap && delete this.inboxMeta.overflowMap[e];
        },
        storeOverflowMessageUidl: function(e, t) {
            this.inboxMeta.overflowMap = this.inboxMeta.overflowMap || {}, this.inboxMeta.overflowMap[e] = {
                size: t
            };
        },
        hasOverflowMessages: function() {
            if (!this.inboxMeta.overflowMap) return !1;
            for (var e in this.inboxMeta.overflowMap) return !0;
            return !1;
        },
        isUidlInOverflowMap: function(e) {
            return this.inboxMeta.overflowMap ? !!this.inboxMeta.overflowMap[e] : !1;
        },
        initialSync: function(e, t, n, o, r) {
            n("sync", !0), this.sync("initial", e, o, r);
        },
        refreshSync: function(e, t, n, o, r, s, i) {
            this.sync("refresh", e, s, i);
        },
        _performTestAdditionsAndDeletions: function(e) {
            var t = this.storage.folderMeta, n = d.latch(), o = !1;
            return t._TEST_pendingHeaderDeletes && (t._TEST_pendingHeaderDeletes.forEach(function(e) {
                o = !0, this.storage.deleteMessageHeaderAndBody(e.suid, e.date, n.defer());
            }, this), t._TEST_pendingHeaderDeletes = null), t._TEST_pendingAdds && (t._TEST_pendingAdds.forEach(function(e) {
                o = !0, this.storeMessage(e.header, e.bodyInfo, {}, n.defer());
            }, this), t._TEST_pendingAdds = null), n.then(function() {
                e();
            }), o;
        },
        growSync: function(e, t, n, o, r, s) {
            return t === p && this.hasOverflowMessages() ? (this.sync("grow", e, r, s), !0) : !1;
        },
        allConsumersDead: function() {},
        shutdown: function() {},
        sync: l(!0, 2, "sync", function(t, n, o, r, s) {
            var c = this;
            e(c, "sync:begin", {
                syncType: n
            });
            var u, l = !1, h = function(t) {
                return l ? (e(c, "sync:duplicateDone", {
                    syncType: n,
                    err: t
                }), void 0) : (e(c, "sync:end", {
                    syncType: n,
                    err: t
                }), l = !0, r(t ? "unknown" : null), void 0);
            };
            u = "grow" !== n ? function(e) {
                return null == c.getMessageIdForUidl(e) && !c.isUidlInOverflowMap(e);
            } : this.isUidlInOverflowMap.bind(this);
            var p, f = 0, g = 0, m = d.latch();
            if (this.isInbox) {
                p = !0, e(this, "sync_begin");
                var y = m.defer(), _ = !1;
                t.onclose = function() {
                    _ || (_ = !0, window.setTimeout(function() {
                        window.setTimeout(function() {
                            h("closed");
                        }, 0);
                    }, 0));
                }, t.listMessages({
                    filter: u,
                    checkpointInterval: i.POP3_SAVE_STATE_EVERY_N_MESSAGES,
                    maxMessages: i.POP3_MAX_MESSAGES_PER_SYNC,
                    checkpoint: function(e) {
                        this.account.__checkpointSyncCompleted(e, "syncBatch");
                    }.bind(this),
                    progress: function(e) {
                        var t = e.totalBytes, n = e.message, o = m.defer();
                        this.storeMessage(n.header, n.bodyInfo, {}, function() {
                            f += e.size, g++, s(.1 + .7 * f / t), o();
                        });
                    }.bind(this)
                }, function(n, o, r) {
                    return _ = !0, t.quit(), n ? (h(n), void 0) : (r.length && (r.forEach(function(e) {
                        this.storeOverflowMessageUidl(e.uidl, e.size);
                    }, this), e(this, "overflowMessages", {
                        count: r.length
                    })), y(), void 0);
                }.bind(this));
            } else o.desiredHeaders = this._TEST_pendingAdds && this._TEST_pendingAdds.length, 
            p = this._performTestAdditionsAndDeletions(m.defer());
            m.then(function() {
                this.storage.markSyncRange(i.OLDEST_SYNC_DATE + a.DAY_MILLIS + 1, a.NOW(), "XXX", a.NOW()), 
                this.hasOverflowMessages() || this.storage.markSyncedToDawnOfTime(), this.isInbox && e(this, "sync_end"), 
                p ? this.account.__checkpointSyncCompleted(v, "syncComplete") : v();
            }.bind(this));
            var v = function() {
                "initial" === n ? (this.storage._curSyncSlice.ignoreHeaders = !1, this.storage._curSyncSlice.waitingOnData = "db", 
                this.storage.getMessagesInImapDateRange(i.OLDEST_SYNC_DATE, null, i.INITIAL_FILL_SIZE, i.INITIAL_FILL_SIZE, this.storage.onFetchDBHeaders.bind(this.storage, this.storage._curSyncSlice, !1, h, null))) : h(null);
            }.bind(this);
        })
    };
}), define("pop3/jobs", [ "module", "exports", "logic", "../allback", "mix", "../jobmixins", "../drafts/jobs", "./pop3" ], function(e, t, n, o, r, s, i) {
    function a(e, t) {
        this.account = e, this.resilientServerIds = !0, this._heldMutexReleasers = [], n.defineScope(this, "Pop3JobDriver", {
            accountId: e.id
        }), this._stateDelta = {}, this._state = t, t.hasOwnProperty("suidToServerId") || (t.suidToServerId = {}, 
        t.moveMap = {});
    }
    t.Pop3JobDriver = a, a.prototype = {
        _accessFolderForMutation: function(e, t, o, r, s) {
            var i = this.account.getFolderStorageForFolderId(e);
            i.runMutexed(s, function(e) {
                this._heldMutexReleasers.push(e);
                try {
                    o(i.folderSyncer, i);
                } catch (t) {
                    n(this, "callbackErr", {
                        ex: t
                    });
                }
            }.bind(this));
        },
        local_do_createFolder: function(e, t) {
            var n, o, r = null, s = 0;
            if (e.parentFolderId) {
                if (!this.account._folderInfos.hasOwnProperty(e.parentFolderId)) throw new Error("No such folder: " + e.parentFolderId);
                var i = this.account._folderInfos[e.parentFolderId];
                o = i.$meta.delim, n = i.$meta.path + o, r = i.$meta.id, s = i.depth + 1;
            } else n = "", o = "/";
            if (n += "string" == typeof e.folderName ? e.folderName : e.folderName.join(o), 
            e.containOnlyOtherFolders && (n += o), this.account.getFolderByPath(n)) t(null); else {
                var a = self.account._learnAboutFolder(e.folderName, n, r, "normal", o, s);
                t(null, a);
            }
        },
        local_do_purgeExcessMessages: function(e, t) {
            this._accessFolderForMutation(e.folderId, !1, function(e, n) {
                n.purgeExcessMessages(function(e) {
                    t(null, null, e > 0);
                });
            }, null, "purgeExcessMessages");
        },
        local_do_saveSentDraft: function(e, t) {
            this._accessFolderForMutation(e.folderId, !1, function(n, r) {
                var s = o.latch();
                r.addMessageHeader(e.headerInfo, e.bodyInfo, s.defer()), r.addMessageBody(e.headerInfo, e.bodyInfo, s.defer()), 
                s.then(function() {
                    t(null, null, !0);
                });
            }, null, "saveSentDraft");
        },
        do_syncFolderList: function(e, t) {
            this.account.meta.lastFolderSyncAt = Date.now(), t(null);
        },
        do_modtags: function(e, t) {
            t(null);
        },
        undo_modtags: function(e, t) {
            t(null);
        },
        local_do_modtags: s.local_do_modtags,
        local_undo_modtags: s.local_undo_modtags,
        local_do_move: s.local_do_move,
        local_undo_move: s.local_undo_move,
        local_do_delete: s.local_do_delete,
        local_undo_delete: s.local_undo_delete,
        local_do_downloadBodies: s.local_do_downloadBodies,
        do_downloadBodies: s.do_downloadBodies,
        check_downloadBodies: s.check_downloadBodies,
        check_downloadBodyReps: s.check_downloadBodyReps,
        do_downloadBodyReps: s.do_downloadBodyReps,
        local_do_downloadBodyReps: s.local_do_downloadBodyReps,
        local_do_sendOutboxMessages: s.local_do_sendOutboxMessages,
        do_sendOutboxMessages: s.do_sendOutboxMessages,
        check_sendOutboxMessages: s.check_sendOutboxMessages,
        local_undo_sendOutboxMessages: s.local_undo_sendOutboxMessages,
        undo_sendOutboxMessages: s.undo_sendOutboxMessages,
        local_do_setOutboxSyncEnabled: s.local_do_setOutboxSyncEnabled,
        local_do_upgradeDB: s.local_do_upgradeDB,
        postJobCleanup: s.postJobCleanup,
        allJobsDone: s.allJobsDone,
        _partitionAndAccessFoldersSequentially: s._partitionAndAccessFoldersSequentially
    }, r(a.prototype, i.draftsMixins);
}), define("pop3/account", [ "logic", "../errbackoff", "../composite/incoming", "./sync", "../errorutils", "./jobs", "../drafts/draft_rep", "../disaster-recovery", "module", "require", "exports" ], function(e, t, n, o, r, s, i, a, c, d, u) {
    function l(n, r, i, c, d, u, l, p) {
        e.defineScope(this, "Account", {
            accountId: i,
            accountType: "pop3"
        }), h.apply(this, [ o.Pop3FolderSyncer ].concat(Array.slice(arguments))), this._conn = null, 
        this._pendingConnectionRequests = [], this._backoffEndpoint = t.createEndpoint("pop3:" + this.id, this), 
        p && (a.associateSocketWithAccount(p.socket, this), this._conn = p), this.ensureEssentialOfflineFolders(), 
        this._jobDriver = new s.Pop3JobDriver(this, this._folderInfos.$mutationState);
    }
    var h = n.CompositeIncomingAccount;
    u.Account = u.Pop3Account = l, l.prototype = Object.create(h.prototype);
    var p = {
        type: "pop3",
        supportsServerFolders: !1,
        toString: function() {
            return "[Pop3Account: " + this.id + "]";
        },
        withConnection: function(e, t) {
            this._pendingConnectionRequests.push(e);
            var n = function() {
                var e = this._pendingConnectionRequests.shift();
                if (e) {
                    var o = function(t) {
                        t ? (e(t), n()) : e(null, this._conn, n);
                    }.bind(this);
                    this._conn && "disconnected" !== this._conn.state ? o() : this._makeConnection(o, t);
                }
            }.bind(this);
            1 === this._pendingConnectionRequests.length && n();
        },
        __folderDoneWithConnection: function() {},
        _makeConnection: function(t, n) {
            this._conn = !0, d([ "./pop3", "./probe" ], function(o, s) {
                e(this, "createConnection", {
                    label: n
                });
                var i = {
                    host: this._connInfo.hostname,
                    port: this._connInfo.port,
                    crypto: this._connInfo.crypto,
                    preferredAuthMethod: this._connInfo.preferredAuthMethod,
                    username: this._credentials.username,
                    password: this._credentials.password
                }, c = this._conn = new o.Pop3Client(i, function(e) {
                    e ? (console.error("Connect error:", e.name, "formal:", e, "on", this._connInfo.hostname, this._connInfo.port), 
                    e = s.normalizePop3Error(e), r.shouldReportProblem(e) && this.universe.__reportAccountProblem(this.compositeAccount, e, "incoming"), 
                    t && t(e, null), c.close(), r.shouldRetry(e) ? this._backoffEndpoint.noteConnectFailureMaybeRetry(r.wasErrorFromReachableState(e)) ? this._backoffEndpoint.scheduleConnectAttempt(this._makeConnection.bind(this)) : this._backoffEndpoint.noteBrokenConnection() : this._backoffEndpoint.noteBrokenConnection()) : (this._backoffEndpoint.noteConnectSuccess(), 
                    t && t(null, c));
                }.bind(this));
                a.associateSocketWithAccount(c.socket, this);
            }.bind(this));
        },
        saveSentMessage: function(e) {
            var t = this.getFirstFolderWithType("sent");
            if (t) {
                var n = this.getFolderStorageForFolderId(t.id), o = n._issueNewHeaderId(), r = n.folderId + "/" + o, s = i.cloneDraftMessageForSentFolderWithoutAttachments(e.header, e.body, {
                    id: o,
                    suid: r
                });
                this.universe.saveSentDraft(t.id, s.header, s.body);
            }
        },
        deleteFolder: function(t, n) {
            if (!this._folderInfos.hasOwnProperty(t)) throw new Error("No such folder: " + t);
            var o = this._folderInfos[t].$meta;
            e(self, "deleteFolder", {
                path: o.path
            }), self._forgetFolder(t), n && n(null, o);
        },
        shutdown: function(e) {
            h.prototype.shutdownFolders.call(this), this._backoffEndpoint.shutdown(), this._conn && this._conn.close && this._conn.close(), 
            e && e();
        },
        checkAccount: function(t) {
            null !== this._conn && ("disconnected" !== this._conn.state && this._conn.close(), 
            this._conn = null), e(this, "checkAccount_begin"), this.withConnection(function(n) {
                e(this, "checkAccount_end", {
                    error: n
                }), t(n);
            }.bind(this), "checkAccount");
        },
        ensureEssentialOfflineFolders: function() {
            [ "sent", "localdrafts", "trash", "outbox" ].forEach(function(e) {
                this.getFirstFolderWithType(e) || this._learnAboutFolder(e, e, null, e, "", 0, !0);
            }, this);
        },
        ensureEssentialOnlineFolders: function(e) {
            e && e();
        },
        accountDeleted: function() {
            this._alive = !1, this.shutdown();
        }
    };
    for (var f in p) Object.defineProperty(l.prototype, f, Object.getOwnPropertyDescriptor(p, f));
}), define("axeshim-smtpclient", [ "require", "logic" ], function(e) {
    var t = e("logic"), n = t.scope("SmtpClient");
    return {
        debug: function(e, o) {
            t.isCensored || t(n, "debug", {
                msg: o
            });
        },
        log: function(e, o) {
            t(n, "log", {
                msg: o
            });
        },
        warn: function(e, o) {
            t(n, "warn", {
                msg: o
            });
        },
        error: function(e, o) {
            t(n, "error", {
                msg: o
            });
        }
    };
}), function(e, t) {
    "function" == typeof define && define.amd ? define("ext/smtpclient/src/smtpclient-response-parser", t) : "object" == typeof exports ? module.exports = t() : e.SmtpClientResponseParser = t();
}(this, function() {
    var e = function() {
        this._remainder = "", this._block = {
            data: [],
            lines: [],
            statusCode: null
        }, this.destroyed = !1;
    };
    return e.prototype.onerror = function() {}, e.prototype.ondata = function() {}, 
    e.prototype.onend = function() {}, e.prototype.send = function(e) {
        if (this.destroyed) return this.onerror(new Error('This parser has already been closed, "write" is prohibited'));
        var t = (this._remainder + (e || "")).split(/\r?\n/);
        this._remainder = t.pop();
        for (var n = 0, o = t.length; o > n; n++) this._processLine(t[n]);
    }, e.prototype.end = function(e) {
        return this.destroyed ? this.onerror(new Error('This parser has already been closed, "end" is prohibited')) : (e && this.send(e), 
        this._remainder && this._processLine(this._remainder), this.destroyed = !0, this.onend(), 
        void 0);
    }, e.prototype._processLine = function(e) {
        var t, n;
        if (e.trim()) if (this._block.lines.push(e), t = e.match(/^(\d{3})([\- ])(?:(\d+\.\d+\.\d+)(?: ))?(.*)/)) {
            if (this._block.data.push(t[4]), "-" === t[2]) return this._block.statusCode && this._block.statusCode !== Number(t[1]) ? this.onerror("Invalid status code " + t[1] + " for multi line response (" + this._block.statusCode + " expected)") : this._block.statusCode || (this._block.statusCode = Number(t[1])), 
            void 0;
            n = {
                statusCode: Number(t[1]) || 0,
                enhancedStatus: t[3] || null,
                data: this._block.data.join("\n"),
                line: this._block.lines.join("\n")
            }, n.success = n.statusCode >= 200 && n.statusCode < 300, this.ondata(n), this._block = {
                data: [],
                lines: [],
                statusCode: null
            }, this._block.statusCode = null;
        } else this.onerror(new Error('Invalid SMTP response "' + e + '"')), this.ondata({
            success: !1,
            statusCode: this._block.statusCode || null,
            enhancedStatus: null,
            data: [ e ].join("\n"),
            line: this._block.lines.join("\n")
        }), this._block = {
            data: [],
            lines: [],
            statusCode: null
        };
    }, e;
}), function(e, t) {
    var n;
    "function" == typeof define && define.amd ? define("ext/smtpclient/src/smtpclient", [ "tcp-socket", "stringencoding", "axe", "./smtpclient-response-parser" ], function(e, n, o, r) {
        return t(e, n.TextEncoder, n.TextDecoder, o, r, window.btoa);
    }) : "object" == typeof exports && "undefined" != typeof navigator ? (n = require("wo-stringencoding"), 
    module.exports = t(require("tcp-socket"), n.TextEncoder, n.TextDecoder, require("axe-logger"), require("./smtpclient-response-parser"), btoa)) : "object" == typeof exports ? (n = require("wo-stringencoding"), 
    module.exports = t(require("tcp-socket"), n.TextEncoder, n.TextDecoder, require("axe-logger"), require("./smtpclient-response-parser"), function(e) {
        var t = require("buffer").Buffer;
        return new t(e, "binary").toString("base64");
    })) : (navigator.TCPSocket = navigator.TCPSocket || navigator.mozTCPSocket, e.SmtpClient = t(navigator.TCPSocket, e.TextEncoder, e.TextDecoder, e.axe, e.SmtpClientResponseParser, window.btoa));
}(this, function(e, t, n, o, r, s) {
    function i(t, n, o) {
        this._TCPSocket = e, this.options = o || {}, this.port = n || (this.options.useSecureTransport ? 465 : 25), 
        this.host = t || "localhost", this.options.useSecureTransport = "useSecureTransport" in this.options ? !!this.options.useSecureTransport : 465 === this.port, 
        this.options.auth = this.options.auth || !1, this.options.name = this.options.name || !1, 
        this.socket = !1, this.destroyed = !1, this.maxAllowedSize = 0, this.waitDrain = !1, 
        this._parser = new r(), this._authenticatedAs = null, this._supportedAuth = [], 
        this._dataMode = !1, this._lastDataBytes = "", this._envelope = null, this._currentAction = null, 
        this._secureMode = !!this.options.useSecureTransport;
    }
    var a = "SMTP Client";
    return i.prototype.onerror = function() {}, i.prototype.ondrain = function() {}, 
    i.prototype.onclose = function() {}, i.prototype.onidle = function() {}, i.prototype.onready = function() {}, 
    i.prototype.ondone = function() {}, i.prototype.connect = function() {
        if (!this.options.name && "getHostname" in this._TCPSocket && "function" == typeof this._TCPSocket.getHostname) return this._TCPSocket.getHostname(function(e, t) {
            this.options.name = t || "localhost", this.connect();
        }.bind(this)), void 0;
        this.options.name || (this.options.name = "localhost"), this.socket = this._TCPSocket.open(this.host, this.port, {
            binaryType: "arraybuffer",
            useSecureTransport: this._secureMode,
            ca: this.options.ca,
            tlsWorkerPath: this.options.tlsWorkerPath
        });
        try {
            this.socket.oncert = this.oncert;
        } catch (e) {}
        this.socket.onerror = this._onError.bind(this), this.socket.onopen = this._onOpen.bind(this);
    }, i.prototype.suspend = function() {
        this.socket && "open" === this.socket.readyState && this.socket.suspend();
    }, i.prototype.resume = function() {
        this.socket && "open" === this.socket.readyState && this.socket.resume();
    }, i.prototype.quit = function() {
        o.debug(a, "Sending QUIT..."), this._sendCommand("QUIT"), this._currentAction = this.close;
    }, i.prototype.reset = function(e) {
        this.options.auth = e || this.options.auth, o.debug(a, "Sending RSET..."), this._sendCommand("RSET"), 
        this._currentAction = this._actionRSET;
    }, i.prototype.close = function() {
        o.debug(a, "Closing connection..."), this.socket && "open" === this.socket.readyState ? this.socket.close() : this._destroy();
    }, i.prototype.useEnvelope = function(e) {
        this._envelope = e || {}, this._envelope.from = [].concat(this._envelope.from || "anonymous@" + this.options.name)[0], 
        this._envelope.to = [].concat(this._envelope.to || []), this._envelope.rcptQueue = [].concat(this._envelope.to), 
        this._envelope.rcptFailed = [], this._envelope.responseQueue = [], this._currentAction = this._actionMAIL, 
        o.debug(a, "Sending MAIL FROM..."), this._sendCommand("MAIL FROM:<" + this._envelope.from + ">");
    }, i.prototype.send = function(e) {
        return this._dataMode ? this._sendString(e) : !0;
    }, i.prototype.end = function(e) {
        return this._dataMode ? (e && e.length && this.send(e), this._currentAction = this._actionStream, 
        this.waitDrain = "\r\n" === this._lastDataBytes ? this.socket.send(new Uint8Array([ 46, 13, 10 ]).buffer) : "\r" === this._lastDataBytes.substr(-1) ? this.socket.send(new Uint8Array([ 10, 46, 13, 10 ]).buffer) : this.socket.send(new Uint8Array([ 13, 10, 46, 13, 10 ]).buffer), 
        this._dataMode = !1, this.waitDrain) : !0;
    }, i.prototype._onOpen = function() {
        this.socket.ondata = this._onData.bind(this), this.socket.onclose = this._onClose.bind(this), 
        this.socket.ondrain = this._onDrain.bind(this), this._parser.ondata = this._onCommand.bind(this), 
        this._currentAction = this._actionGreeting;
    }, i.prototype._onData = function(e) {
        var t = new n("UTF-8").decode(new Uint8Array(e.data));
        o.debug(a, "SERVER: " + t), this._parser.send(t);
    }, i.prototype._onDrain = function() {
        this.waitDrain = !1, this.ondrain();
    }, i.prototype._onError = function(e) {
        e instanceof Error && e.message ? (o.error(a, e), this.onerror(e)) : e && e.data instanceof Error ? (o.error(a, e.data), 
        this.onerror(e.data)) : (o.error(a, new Error(e && e.data && e.data.message || e.data || e || "Error")), 
        this.onerror(new Error(e && e.data && e.data.message || e.data || e || "Error"))), 
        this.close();
    }, i.prototype._onClose = function() {
        o.debug(a, "Socket closed."), this._destroy();
    }, i.prototype._onCommand = function(e) {
        "function" == typeof this._currentAction && this._currentAction.call(this, e);
    }, i.prototype._destroy = function() {
        this.destroyed || (this.destroyed = !0, this.onclose());
    }, i.prototype._sendString = function(e) {
        return this.options.disableEscaping || (e = e.replace(/\n\./g, "\n.."), "\n" !== this._lastDataBytes.substr(-1) && this._lastDataBytes || "." !== e.charAt(0) || (e = "." + e)), 
        e.length > 2 ? this._lastDataBytes = e.substr(-2) : 1 === e.length && (this._lastDataBytes = this._lastDataBytes.substr(-1) + e), 
        o.debug(a, "Sending " + e.length + " bytes of payload"), this.waitDrain = this.socket.send(new t("UTF-8").encode(e).buffer), 
        this.waitDrain;
    }, i.prototype._sendCommand = function(e) {
        this.waitDrain = this.socket.send(new t("UTF-8").encode(e + ("\r\n" !== e.substr(-2) ? "\r\n" : "")).buffer);
    }, i.prototype._authenticateUser = function() {
        if (!this.options.auth) return this._currentAction = this._actionIdle, this.onidle(), 
        void 0;
        var e;
        switch (!this.options.authMethod && this.options.auth.xoauth2 && (this.options.authMethod = "XOAUTH2"), 
        e = this.options.authMethod ? this.options.authMethod.toUpperCase().trim() : (this._supportedAuth[0] || "PLAIN").toUpperCase().trim()) {
          case "LOGIN":
            return o.debug(a, "Authentication via AUTH LOGIN"), this._currentAction = this._actionAUTH_LOGIN_USER, 
            this._sendCommand("AUTH LOGIN"), void 0;

          case "PLAIN":
            return o.debug(a, "Authentication via AUTH PLAIN"), this._currentAction = this._actionAUTHComplete, 
            this._sendCommand("AUTH PLAIN " + s(unescape(encodeURIComponent("\0" + this.options.auth.user + "\0" + this.options.auth.pass)))), 
            void 0;

          case "XOAUTH2":
            return o.debug(a, "Authentication via AUTH XOAUTH2"), this._currentAction = this._actionAUTH_XOAUTH2, 
            this._sendCommand("AUTH XOAUTH2 " + this._buildXOAuth2Token(this.options.auth.user, this.options.auth.xoauth2)), 
            void 0;
        }
        this._onError(new Error("Unknown authentication method " + e));
    }, i.prototype._actionGreeting = function(e) {
        return 220 !== e.statusCode ? (this._onError(new Error("Invalid greeting: " + e.data)), 
        void 0) : (this.options.lmtp ? (o.debug(a, "Sending LHLO " + this.options.name), 
        this._currentAction = this._actionLHLO, this._sendCommand("LHLO " + this.options.name)) : (o.debug(a, "Sending EHLO " + this.options.name), 
        this._currentAction = this._actionEHLO, this._sendCommand("EHLO " + this.options.name)), 
        void 0);
    }, i.prototype._actionLHLO = function(e) {
        return e.success ? (this._actionEHLO(e), void 0) : (o.error(a, "LHLO not successful"), 
        this._onError(new Error(e.data)), void 0);
    }, i.prototype._actionEHLO = function(e) {
        var t;
        if (!e.success) {
            if (!this._secureMode && this.options.requireTLS) {
                var n = "STARTTLS not supported without EHLO";
                return o.error(a, n), this._onError(new Error(n)), void 0;
            }
            return o.warn(a, "EHLO not successful, trying HELO " + this.options.name), this._currentAction = this._actionHELO, 
            this._sendCommand("HELO " + this.options.name), void 0;
        }
        return e.line.match(/AUTH(?:\s+[^\n]*\s+|\s+)PLAIN/i) && (o.debug(a, "Server supports AUTH PLAIN"), 
        this._supportedAuth.push("PLAIN")), e.line.match(/AUTH(?:\s+[^\n]*\s+|\s+)LOGIN/i) && (o.debug(a, "Server supports AUTH LOGIN"), 
        this._supportedAuth.push("LOGIN")), e.line.match(/AUTH(?:\s+[^\n]*\s+|\s+)XOAUTH2/i) && (o.debug(a, "Server supports AUTH XOAUTH2"), 
        this._supportedAuth.push("XOAUTH2")), (t = e.line.match(/SIZE (\d+)/i)) && Number(t[1]) && (this._maxAllowedSize = Number(t[1]), 
        o.debug(a, "Maximum allowd message size: " + this._maxAllowedSize)), !this._secureMode && (e.line.match(/[ \-]STARTTLS\s?$/im) && !this.options.ignoreTLS || this.options.requireTLS) ? (this._currentAction = this._actionSTARTTLS, 
        this._sendCommand("STARTTLS"), void 0) : (this._authenticateUser.call(this), void 0);
    }, i.prototype._actionSTARTTLS = function(e) {
        return e.success ? (this._secureMode = !0, this.socket.upgradeToSecure(), this._currentAction = this._actionEHLO, 
        this._sendCommand("EHLO " + this.options.name), void 0) : (o.error(a, "STARTTLS not successful"), 
        this._onError(new Error(e.data)), void 0);
    }, i.prototype._actionHELO = function(e) {
        return e.success ? (this._authenticateUser.call(this), void 0) : (o.error(a, "HELO not successful"), 
        this._onError(new Error(e.data)), void 0);
    }, i.prototype._actionAUTH_LOGIN_USER = function(e) {
        return 334 !== e.statusCode || "VXNlcm5hbWU6" !== e.data ? (o.error(a, "AUTH LOGIN USER not successful: " + e.data), 
        this._onError(new Error('Invalid login sequence while waiting for "334 VXNlcm5hbWU6 ": ' + e.data)), 
        void 0) : (o.debug(a, "AUTH LOGIN USER successful"), this._currentAction = this._actionAUTH_LOGIN_PASS, 
        this._sendCommand(s(unescape(encodeURIComponent(this.options.auth.user)))), void 0);
    }, i.prototype._actionAUTH_LOGIN_PASS = function(e) {
        return 334 !== e.statusCode || "UGFzc3dvcmQ6" !== e.data ? (o.error(a, "AUTH LOGIN PASS not successful: " + e.data), 
        this._onError(new Error('Invalid login sequence while waiting for "334 UGFzc3dvcmQ6 ": ' + e.data)), 
        void 0) : (o.debug(a, "AUTH LOGIN PASS successful"), this._currentAction = this._actionAUTHComplete, 
        this._sendCommand(s(unescape(encodeURIComponent(this.options.auth.pass)))), void 0);
    }, i.prototype._actionAUTH_XOAUTH2 = function(e) {
        e.success ? this._actionAUTHComplete(e) : (o.warn(a, "Error during AUTH XOAUTH2, sending empty response"), 
        this._sendCommand(""), this._currentAction = this._actionAUTHComplete);
    }, i.prototype._actionAUTHComplete = function(e) {
        return e.success ? (o.debug(a, "Authentication successful."), this._authenticatedAs = this.options.auth.user, 
        this._currentAction = this._actionIdle, this.onidle(), void 0) : (o.debug(a, "Authentication failed: " + e.data), 
        this._onError(new Error(e.data)), void 0);
    }, i.prototype._actionIdle = function(e) {
        return e.statusCode > 300 ? (this._onError(new Error(e.line)), void 0) : (this._onError(new Error(e.data)), 
        void 0);
    }, i.prototype._actionMAIL = function(e) {
        return e.success ? (this._envelope.rcptQueue.length ? (o.debug(a, "MAIL FROM successful, proceeding with " + this._envelope.rcptQueue.length + " recipients"), 
        o.debug(a, "Adding recipient..."), this._envelope.curRecipient = this._envelope.rcptQueue.shift(), 
        this._currentAction = this._actionRCPT, this._sendCommand("RCPT TO:<" + this._envelope.curRecipient + ">")) : this._onError(new Error("Can't send mail - no recipients defined")), 
        void 0) : (o.debug(a, "MAIL FROM unsuccessful: " + e.data), this._onError(new Error(e.data)), 
        void 0);
    }, i.prototype._actionRCPT = function(e) {
        if (e.success ? this._envelope.responseQueue.push(this._envelope.curRecipient) : (o.warn(a, "RCPT TO failed for: " + this._envelope.curRecipient), 
        this._envelope.rcptFailed.push(this._envelope.curRecipient)), this._envelope.rcptQueue.length) o.debug(a, "Adding recipient..."), 
        this._envelope.curRecipient = this._envelope.rcptQueue.shift(), this._currentAction = this._actionRCPT, 
        this._sendCommand("RCPT TO:<" + this._envelope.curRecipient + ">"); else {
            if (!(this._envelope.rcptFailed.length < this._envelope.to.length)) return this._onError(new Error("Can't send mail - all recipients were rejected")), 
            this._currentAction = this._actionIdle, void 0;
            this._currentAction = this._actionDATA, o.debug(a, "RCPT TO done, proceeding with payload"), 
            this._sendCommand("DATA");
        }
    }, i.prototype._actionRSET = function(e) {
        return e.success ? (this._authenticatedAs = null, this._authenticateUser.call(this), 
        void 0) : (o.error(a, "RSET unsuccessful " + e.data), this._onError(new Error(e.data)), 
        void 0);
    }, i.prototype._actionDATA = function(e) {
        return [ 250, 354 ].indexOf(e.statusCode) < 0 ? (o.error(a, "DATA unsuccessful " + e.data), 
        this._onError(new Error(e.data)), void 0) : (this._dataMode = !0, this._currentAction = this._actionIdle, 
        this.onready(this._envelope.rcptFailed), void 0);
    }, i.prototype._actionStream = function(e) {
        var t;
        if (this.options.lmtp) {
            if (t = this._envelope.responseQueue.shift(), e.success ? o.error(a, "Local delivery to " + t + " succeeded.") : (o.error(a, "Local delivery to " + t + " failed."), 
            this._envelope.rcptFailed.push(t)), this._envelope.responseQueue.length) return this._currentAction = this._actionStream, 
            void 0;
            this._currentAction = this._actionIdle, this.ondone(!0);
        } else e.success ? o.debug(a, "Message sent successfully.") : o.error(a, "Message sending failed."), 
        this._currentAction = this._actionIdle, this.ondone(!!e.success);
        this._currentAction === this._actionIdle && (o.debug(a, "Idling while waiting for new connections..."), 
        this.onidle());
    }, i.prototype._buildXOAuth2Token = function(e, t) {
        var n = [ "user=" + (e || ""), "auth=Bearer " + t, "", "" ];
        return s(unescape(encodeURIComponent(n.join(""))));
    }, i;
}), define("smtpclient", [ "ext/smtpclient/src/smtpclient" ], function(e) {
    return e;
}), define("smtp/client", [ "require", "exports", "module", "logic", "smtpclient", "../syncbase", "../oauth" ], function(e, t) {
    var n = e("logic"), o = e("smtpclient"), r = e("../syncbase"), s = e("../oauth"), i = window.setTimeout, a = window.clearTimeout;
    t.setTimeoutFunctions = function(e, t) {
        i = e, a = t;
    };
    var c = n.scope("SmtpClient");
    t.createSmtpConnection = function(e, d, l) {
        var h;
        return s.ensureUpdatedCredentials(e, l).then(function() {
            return new Promise(function(t, s) {
                function u() {
                    p && (a(p), p = null);
                }
                var l = {
                    user: void 0 !== e.outgoingUsername ? e.outgoingUsername : e.username,
                    pass: void 0 !== e.outgoingPassword ? e.outgoingPassword : e.password,
                    xoauth2: e.oauth2 ? e.oauth2.accessToken : null
                };
                n(c, "connect", {
                    _auth: l,
                    usingOauth2: !!e.oauth2,
                    connInfo: d
                }), h = new o(d.hostname, d.port, {
                    auth: l,
                    useSecureTransport: "ssl" === d.crypto || d.crypto === !0,
                    requireTLS: "starttls" === d.crypto,
                    ignoreTLS: "plain" === d.crypto
                });
                var p = i(function() {
                    h.onerror("unresponsive-server"), h.close();
                }, r.CONNECT_TIMEOUT_MS);
                h.onidle = function() {
                    u(), n(c, "connected", d), h.onidle = h.onclose = h.onerror = function() {}, t(h);
                }, h.onerror = function(e) {
                    u(), s(e);
                }, h.onclose = function() {
                    u(), s("server-maybe-offline");
                }, h.connect();
            });
        }).catch(function(o) {
            var r = u(h, o, !1);
            if (h && h.close(), "needs-oauth-reauth" === r && s.isRenewPossible(e)) return s.ensureUpdatedCredentials(e, l, !0).then(function() {
                return t.createImapConnection(e, d, l);
            });
            throw n(c, "connect-error", {
                error: r,
                connInfo: d
            }), r;
        });
    };
    var d = o.prototype._onCommand;
    o.prototype._onCommand = function(e) {
        e.statusCode && !e.success && (this._lastSmtpError = e), d.apply(this, arguments);
    }, o.prototype._onError = function(e) {
        e instanceof Error && e.message ? this.onerror(e) : e && e.data instanceof Error ? this.onerror(e.data) : this.onerror(e && e.data && e.data.message || e.data || e || "Error"), 
        this.close();
    };
    var u = t.analyzeSmtpError = function(e, t, o) {
        var r = t;
        (r && !r.statusCode && "Error" === r.name || !r) && (r = e && e._lastSmtpError || null), 
        r || (r = "null-error");
        var s = e && !!e.options.auth.xoauth2, i = "unknown";
        if (r.statusCode) if (e._currentAction === e._actionSTARTTLS || e._currentAction === e._actionEHLO) i = "bad-security"; else switch (r.statusCode) {
          case 535:
            i = s ? "needs-oauth-reauth" : "bad-user-or-pass";
            break;

          case 534:
            i = "bad-user-or-pass";
            break;

          case 501:
            i = o ? "bad-message" : "server-maybe-offline";
            break;

          case 550:
          case 551:
          case 553:
          case 554:
            i = "bad-address";
            break;

          case 500:
            i = "server-problem";
            break;

          default:
            i = o ? "bad-message" : "unknown";
        } else "ConnectionRefusedError" === r.name ? i = "unresponsive-server" : /^Security/.test(r.name) ? i = "bad-security" : "string" == typeof r && (i = r);
        return n(c, "analyzed-error", {
            statusCode: r.statusCode,
            enhancedStatus: r.enhancedStatus,
            rawError: t,
            rawErrorName: t && t.name,
            rawErrorMessage: t && t.message,
            rawErrorStack: t && t.stack,
            normalizedError: i,
            errorName: r.name,
            errorMessage: r.message,
            errorData: r.data,
            wasSending: o
        }), i;
    };
}), define("smtp/account", [ "require", "logic", "./client", "../disaster-recovery" ], function(e) {
    function t(e, t, o, r, s) {
        this.universe = e, n.defineScope(this, "Account", {
            accountId: o,
            accountType: "smtp"
        }), this.compositeAccount = t, this.accountId = o, this.credentials = r, this.connInfo = s, 
        this._activeConnections = [];
    }
    var n = e("logic"), o = e("./client"), r = e("../disaster-recovery");
    return t.prototype = {
        type: "smtp",
        toString: function() {
            return "[SmtpAccount: " + this.id + "]";
        },
        get numActiveConns() {
            return this._activeConnections.length;
        },
        shutdown: function() {},
        accountDeleted: function() {
            this.shutdown();
        },
        sendMessage: function(e, t) {
            var o = this;
            this.establishConnection({
                sendEnvelope: function(t) {
                    var r = e.getEnvelope();
                    n(o, "sendEnvelope", {
                        _envelope: r
                    }), t.useEnvelope(r);
                },
                onProgress: function() {
                    e.renewSmartWakeLock("SMTP XHR Progress");
                },
                sendMessage: function(t) {
                    n(o, "building-blob"), e.withMessageBlob({
                        includeBcc: !1,
                        smtp: !0
                    }, function(e) {
                        n(o, "sending-blob", {
                            size: e.size
                        }), t.socket.send(e), t._lastDataBytes = "\r\n", t.end();
                    });
                },
                onSendComplete: function() {
                    n(o, "smtp:sent"), t(null);
                },
                onError: function(e, r) {
                    n(o, "smtp:error", {
                        error: e,
                        badAddresses: r
                    }), t(e, r);
                }
            });
        },
        checkAccount: function(e) {
            var t = !1;
            this.establishConnection({
                sendEnvelope: function(n, o) {
                    t = !0, o(), e();
                },
                sendMessage: function() {},
                onSendComplete: function() {},
                onError: function(t) {
                    "bad-user-or-pass" === t && this.universe.__reportAccountProblem(this.compositeAccount, t, "outgoing"), 
                    e(t);
                }.bind(this)
            });
        },
        establishConnection: function(e) {
            var t, s = this, i = !1;
            o.createSmtpConnection(this.credentials, this.connInfo, function() {
                return new Promise(function(e) {
                    this.universe.saveAccountDef(this.compositeAccount.accountDef, null, e);
                }.bind(this));
            }.bind(this)).then(function(a) {
                t = a, r.associateSocketWithAccount(t.socket, this), this._activeConnections.push(t);
                var c = t.socket.ondrain;
                t.socket.ondrain = function() {
                    c && c.call(t.socket), e.onProgress && e.onProgress();
                }, e.sendEnvelope(t, t.close.bind(t)), t.onready = function(o) {
                    n(s, "onready"), o.length ? (t.close(), n(s, "bad-recipients", {
                        badRecipients: o
                    }), e.onError("bad-recipient", o)) : (i = !0, e.sendMessage(t));
                }, t.ondone = function(r) {
                    if (t.close(), r) n(s, "sent"), e.onSendComplete(t); else {
                        n(s, "send-failed");
                        var a = o.analyzeSmtpError(t, null, i);
                        e.onError(a, null);
                    }
                }, t.onerror = function(n) {
                    t.close(), n = o.analyzeSmtpError(t, n, i), e.onError(n, null);
                }, t.onclose = function() {
                    n(s, "onclose");
                    var e = this._activeConnections.indexOf(t);
                    -1 !== e ? this._activeConnections.splice(e, 1) : n(s, "dead-unknown-connection");
                }.bind(this);
            }.bind(this)).catch(function(n) {
                n = o.analyzeSmtpError(t, n, i), e.onError(n);
            });
        }
    }, {
        Account: t,
        SmtpAccount: t
    };
}), define("composite/account", [ "logic", "../accountcommon", "../a64", "../accountmixins", "../imap/account", "../pop3/account", "../smtp/account", "../allback", "exports" ], function(e, t, n, o, r, s, i, a, c) {
    function d(t, n, r, s, i) {
        this.universe = t, this.id = n.id, this.accountDef = n, e.defineScope(this, "Account", {
            accountId: this.id
        }), this._enabled = !0, this.problems = [], n.credentials && n.credentials.oauth2 && (n.credentials.oauth2._transientLastRenew = 0), 
        this.identities = n.identities, u.hasOwnProperty(n.receiveType) || e(this, "badAccountType", {
            type: n.receiveType
        }), u.hasOwnProperty(n.sendType) || e(this, "badAccountType", {
            type: n.sendType
        }), this._receivePiece = new u[n.receiveType](t, this, n.id, n.credentials, n.receiveConnInfo, r, s, i), 
        this._sendPiece = new u[n.sendType](t, this, n.id, n.credentials, n.sendConnInfo, s), 
        this.folders = this._receivePiece.folders, this.meta = this._receivePiece.meta, 
        this.mutations = this._receivePiece.mutations, o.accountConstructorMixin.call(this, this._receivePiece, this._sendPiece);
    }
    var u = {
        imap: r.ImapAccount,
        pop3: s.Pop3Account,
        smtp: i.SmtpAccount
    };
    c.Account = c.CompositeAccount = d, d.prototype = {
        toString: function() {
            return "[CompositeAccount: " + this.id + "]";
        },
        get supportsServerFolders() {
            return this._receivePiece.supportsServerFolders;
        },
        toBridgeWire: function() {
            return {
                id: this.accountDef.id,
                name: this.accountDef.name,
                label: this.accountDef.label,
                type: this.accountDef.type,
                defaultPriority: this.accountDef.defaultPriority,
                enabled: this.enabled,
                problems: this.problems,
                syncRange: this.accountDef.syncRange,
                syncInterval: this.accountDef.syncInterval,
                notifyOnNew: this.accountDef.notifyOnNew,
                playSoundOnSend: this.accountDef.playSoundOnSend,
                identities: this.identities,
                credentials: {
                    username: this.accountDef.credentials.username,
                    outgoingUsername: this.accountDef.credentials.outgoingUsername,
                    oauth2: this.accountDef.credentials.oauth2,
                    password: this.accountDef.credentials.password
                },
                servers: [ {
                    type: this.accountDef.receiveType,
                    connInfo: this.accountDef.receiveConnInfo,
                    activeConns: this._receivePiece.numActiveConns || 0
                }, {
                    type: this.accountDef.sendType,
                    connInfo: this.accountDef.sendConnInfo,
                    activeConns: this._sendPiece.numActiveConns || 0
                } ]
            };
        },
        toBridgeFolder: function() {
            return {
                id: this.accountDef.id,
                name: this.accountDef.name,
                path: this.accountDef.name,
                type: "account"
            };
        },
        get enabled() {
            return this._enabled;
        },
        set enabled(e) {
            this._enabled = this._receivePiece.enabled = e;
        },
        saveAccountState: function(e, t, n) {
            return this._receivePiece.saveAccountState(e, t, n);
        },
        get _saveAccountIsImminent() {
            return this.__saveAccountIsImminent;
        },
        set _saveAccountIsImminent(e) {
            this.___saveAccountIsImminent = this._receivePiece._saveAccountIsImminent = e;
        },
        runAfterSaves: function(e) {
            return this._receivePiece.runAfterSaves(e);
        },
        allOperationsCompleted: function() {
            this._receivePiece.allOperationsCompleted && this._receivePiece.allOperationsCompleted();
        },
        checkAccount: function(e) {
            var t = a.latch();
            this._receivePiece.checkAccount(t.defer("incoming")), this._sendPiece.checkAccount(t.defer("outgoing")), 
            t.then(function(t) {
                e(t.incoming[0], t.outgoing[0]);
            });
        },
        shutdown: function(e) {
            this._sendPiece.shutdown(), this._receivePiece.shutdown(e);
        },
        accountDeleted: function() {
            this._sendPiece.accountDeleted(), this._receivePiece.accountDeleted();
        },
        deleteFolder: function(e, t) {
            return this._receivePiece.deleteFolder(e, t);
        },
        sliceFolderMessages: function(e, t) {
            return this._receivePiece.sliceFolderMessages(e, t);
        },
        sortFolderMessages: function(e, t, n) {
            return this._receivePiece.sortFolderMessages(e, t, n);
        },
        searchFolderMessages: function(e, t, n, o) {
            return this._receivePiece.searchFolderMessages(e, t, n, o);
        },
        syncFolderList: function(e) {
            return this._receivePiece.syncFolderList(e);
        },
        sendMessage: function(e, t) {
            return this._sendPiece.sendMessage(e, function(n, o) {
                n || this._receivePiece.saveSentMessage(e), t(n, o, null);
            }.bind(this));
        },
        getFolderStorageForFolderId: function(e) {
            return this._receivePiece.getFolderStorageForFolderId(e);
        },
        getFolderMetaForFolderId: function(e) {
            return this._receivePiece.getFolderMetaForFolderId(e);
        },
        runOp: function(e, t, n) {
            return this._receivePiece.runOp(e, t, n);
        },
        ensureEssentialOnlineFolders: function(e) {
            return this._receivePiece.ensureEssentialOnlineFolders(e);
        },
        getFirstFolderWithType: o.getFirstFolderWithType,
        upgradeFolderStoragesIfNeeded: function() {
            for (var e in this._receivePiece._folderStorages) {
                var t = this._receivePiece._folderStorages[e];
                t.upgradeIfNeeded();
            }
        }
    };
}), define("composite/configurator", [ "logic", "../accountcommon", "../a64", "../allback", "./account", "../date", "require", "exports" ], function(e, t, n, o, r, s, i, a) {
    o.allbackMaker, a.account = r, a.configurator = {
        tryToCreateAccount: function(e, t, n, o) {
            var r, a, c, d;
            if (n) {
                d = "imap+smtp" === n.type ? "imap" : "pop3";
                var u = null;
                u = void 0 !== t.outgoingPassword ? t.outgoingPassword : t.password, r = {
                    username: n.incoming.username,
                    password: t.password,
                    outgoingUsername: n.outgoing.username,
                    outgoingPassword: u
                }, n.oauth2Tokens && (r.oauth2 = {
                    authEndpoint: n.oauth2Settings.authEndpoint,
                    tokenEndpoint: n.oauth2Settings.tokenEndpoint,
                    scope: n.oauth2Settings.scope,
                    clientId: n.oauth2Secrets.clientId,
                    clientSecret: n.oauth2Secrets.clientSecret,
                    refreshToken: n.oauth2Tokens.refreshToken,
                    accessToken: n.oauth2Tokens.accessToken,
                    expireTimeMS: n.oauth2Tokens.expireTimeMS,
                    _transientLastRenew: s.PERFNOW()
                }), a = {
                    hostname: n.incoming.hostname,
                    port: n.incoming.port,
                    crypto: "string" == typeof n.incoming.socketType ? n.incoming.socketType.toLowerCase() : n.incoming.socketType
                }, "pop3" === d && (a.preferredAuthMethod = null), c = {
                    emailAddress: t.emailAddress,
                    hostname: n.outgoing.hostname,
                    port: n.outgoing.port,
                    crypto: "string" == typeof n.outgoing.socketType ? n.outgoing.socketType.toLowerCase() : n.outgoing.socketType
                };
            }
            var l = new Promise(function(e, t) {
                "imap" === d ? i([ "../imap/probe" ], function(n) {
                    n.probeAccount(r, a).then(e, t);
                }) : i([ "../pop3/probe" ], function(n) {
                    n.probeAccount(r, a).then(e, t);
                });
            }), h = new Promise(function(e, t) {
                i([ "../smtp/probe" ], function(n) {
                    n.probeAccount(r, c).then(e, t);
                });
            });
            Promise.all([ l, h ]).then(function(n) {
                var s, i = n[0].conn;
                "imap" === d ? s = this._defineImapAccount : "pop3" === d && (a.preferredAuthMethod = i.authMethod, 
                s = this._definePop3Account), s.call(this, e, t, r, a, c, i, o);
            }.bind(this)).catch(function(e) {
                l.then(function(t) {
                    t.conn.close(), o(e, null, {
                        server: c.hostname
                    });
                }).catch(function(e) {
                    o(e, null, {
                        server: a.hostname
                    });
                });
            });
        },
        recreateAccount: function(e, o, r, s) {
            var i = r.def, a = {
                username: i.credentials.username,
                password: i.credentials.password,
                outgoingUsername: i.credentials.outgoingUsername,
                outgoingPassword: i.credentials.outgoingPassword,
                authMechanism: i.credentials.authMechanism,
                oauth2: i.credentials.oauth2
            }, c = n.encodeInt(e.config.nextAccountNum++), d = i.type || "imap+smtp", u = {
                id: c,
                name: i.name,
                label: i.label,
                type: d,
                receiveType: d.split("+")[0],
                sendType: "smtp",
                syncRange: i.syncRange,
                syncInterval: i.syncInterval || 0,
                notifyOnNew: i.hasOwnProperty("notifyOnNew") ? i.notifyOnNew : !0,
                playSoundOnSend: i.hasOwnProperty("playSoundOnSend") ? i.playSoundOnSend : !0,
                credentials: a,
                receiveConnInfo: {
                    hostname: i.receiveConnInfo.hostname,
                    port: i.receiveConnInfo.port,
                    crypto: i.receiveConnInfo.crypto,
                    preferredAuthMethod: i.receiveConnInfo.preferredAuthMethod || null
                },
                sendConnInfo: {
                    hostname: i.sendConnInfo.hostname,
                    port: i.sendConnInfo.port,
                    crypto: i.sendConnInfo.crypto
                },
                identities: t.recreateIdentities(e, c, i.identities)
            };
            this._loadAccount(e, u, r.folderInfo, null, function(e) {
                s(null, e, null);
            });
        },
        _defineImapAccount: function(e, t, o, r, i, a, c) {
            var d = n.encodeInt(e.config.nextAccountNum++), u = {
                id: d,
                name: t.accountName || t.emailAddress,
                label: t.label || t.emailAddress.split("@")[1].split(".")[0],
                defaultPriority: s.NOW(),
                type: "imap+smtp",
                receiveType: "imap",
                sendType: "smtp",
                syncRange: "auto",
                syncInterval: t.syncInterval || 0,
                notifyOnNew: t.hasOwnProperty("notifyOnNew") ? t.notifyOnNew : !0,
                playSoundOnSend: t.hasOwnProperty("playSoundOnSend") ? t.playSoundOnSend : !0,
                credentials: o,
                receiveConnInfo: r,
                sendConnInfo: i,
                identities: [ {
                    id: d + "/" + n.encodeInt(e.config.nextIdentityNum++),
                    name: t.displayName,
                    address: t.emailAddress,
                    replyTo: null,
                    signature: null,
                    signatureEnabled: !1
                } ]
            };
            this._loadAccount(e, u, null, a, function(e) {
                c(null, e, null);
            });
        },
        _definePop3Account: function(e, t, o, r, i, a, c) {
            var d = n.encodeInt(e.config.nextAccountNum++), u = {
                id: d,
                name: t.accountName || t.emailAddress,
                label: t.label || t.emailAddress.split("@")[1].split(".")[0],
                defaultPriority: s.NOW(),
                type: "pop3+smtp",
                receiveType: "pop3",
                sendType: "smtp",
                syncRange: "auto",
                syncInterval: t.syncInterval || 0,
                notifyOnNew: t.hasOwnProperty("notifyOnNew") ? t.notifyOnNew : !0,
                playSoundOnSend: t.hasOwnProperty("playSoundOnSend") ? t.playSoundOnSend : !0,
                credentials: o,
                receiveConnInfo: r,
                sendConnInfo: i,
                identities: [ {
                    id: d + "/" + n.encodeInt(e.config.nextIdentityNum++),
                    name: t.displayName,
                    address: t.emailAddress,
                    replyTo: null,
                    signature: null,
                    signatureEnabled: !1
                } ]
            };
            this._loadAccount(e, u, null, a, function(e) {
                c(null, e, null);
            });
        },
        _loadAccount: function(e, t, n, o, r) {
            var s = e.getNeedCancelAccount();
            if (s && s === t.name) return console.log("Cancel setup this account(account name: " + t.name + ")!!!"), 
            e.resetNeedCancelAccount(), void 0;
            var i;
            i = "imap" === t.receiveType ? {
                $meta: {
                    nextFolderNum: 0,
                    nextMutationNum: 0,
                    lastFolderSyncAt: 0,
                    capability: n && n.$meta.capability || o.capability
                },
                $mutations: [],
                $mutationState: {}
            } : {
                $meta: {
                    nextFolderNum: 0,
                    nextMutationNum: 0,
                    lastFolderSyncAt: 0
                },
                $mutations: [],
                $mutationState: {}
            }, e.saveAccountDef(t, i), e._loadAccount(t, i, o, r);
        }
    };
});