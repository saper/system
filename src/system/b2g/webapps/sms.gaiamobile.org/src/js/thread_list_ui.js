/* -*- Mode: js; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- /
/* vim: set shiftwidth=2 tabstop=2 autoindent cindent expandtab: */

/*global Template, Utils, Threads, Contacts, Threads,
         WaitingScreen, MessageManager, TimeHeaders,
         Drafts, Thread, ThreadUI, OptionMenu, ActivityPicker,
         PerformanceTestingHelper, StickyHeader, Navigation,
         InterInstanceEventDispatcher,
         SelectionHandler,
         Settings,
         LazyLoader
*/
/*exported ThreadListUI */
(function(exports) {
'use strict';

const privateMembers = new WeakMap();

  var skNewMessage = {
    l10nId: 'new-message',
    priority: 1,
    method: function() {
      Navigation.toPanel('composer');
    }
  };

  var skDeleteThread = {
    l10nId: 'delete-thread',
    priority: 5,
    method: function() {
      Utils.speedPressPrevent(function() {
        ThreadListUI.deleteCurrentThread();
      });
    }
  };

  var skCreateNewContact = {
    l10nId: 'create-new-contact',
    priority: 5,
    method: function() {
      if (ThreadListUI.currentThread) {
        var thread = null;
        var tel = null;
        var threadid = ThreadListUI.currentThread.dataset.threadId;
        if (ThreadListUI.currentThread.classList.contains('draft')) {
          thread = Drafts.get(threadid);
          tel = thread.recipients[0];
        } else {
          thread = Threads.get(threadid);
          tel = thread.participants[0];
        }

        var opt = {
          tel: tel
        };
        ActivityPicker.createNewContact(opt, () => {
          ThreadListUI.updateSKs();
        });
      }
    }
  };

  var skAddToContact = {
    l10nId: 'add-to-contact',
    priority: 5,
    method: function() {
      if (ThreadListUI.currentThread) {
        var thread = null;
        var tel = null;
        var threadid = ThreadListUI.currentThread.dataset.threadId;
        if (ThreadListUI.currentThread.classList.contains('draft')) {
          thread = Drafts.get(threadid);
          tel = thread.recipients[0];
        } else {
          thread = Threads.get(threadid);
          tel = thread.participants[0];
        }

        var opt = {
          tel: tel
        };
        ActivityPicker.addToExistingContact(opt, () => {
          ThreadListUI.updateSKs();
        });
      }
    }
  };

  var skSelectThread = {
    l10nId: 'select-thread',
    priority: 5,
    method: function() {
      ThreadListUI.startEdit();
    }
  };

  var skSettings = {
    l10nId: 'settings',
    priority: 5,
    method: function() {
      Navigation.toPanel("settings-view");
    }
  };

  var skSelectAll = {
    l10nId: 'select-all',
    priority: 1,
    method: function() {
      ThreadListUI.clickCheckUncheckAllButton();
    }
  };

  var skDeselectAll = {
    l10nId: 'deselect-all',
    priority: 1,
    method: function() {
      ThreadListUI.clickCheckUncheckAllButton();
    }
  };

  var skDelete = {
    l10nId: 'delete',
    priority: 3,
    method: function() {
      ThreadListUI.delete();
    }
  };

  var skSelect = {
    l10nId: 'select',
    priority: 2
  };

  var skCancel = {
    l10nId: 'cancel',
    priority: 1,
    method: function() {
      ActivityHandler.leaveActivity();
    }
  };

  var skDeSelect = {
    l10nId: 'deselect',
    priority: 2
  };

  var skSearch = {
    l10nId: 'search',
    priority: 5,
    method: function() {
      Navigation.toPanel('search-message-view');
    }
  };

  var option_menu_edit_normal = [
    skSelectAll, skSelect
  ];

  var option_menu_edit_choose = [
    skSelectAll, skSelect, skDelete
  ];

  var option_menu_edit_deselect_choose = [
    skSelectAll, skDeSelect, skDelete
  ];

  var option_menu_edit_deselect_choosedall = [
    skDeselectAll, skDeSelect, skDelete
  ];

  var option_menu_no_thread = [
    skNewMessage, skSettings
  ];

  var option_menu_thead_from_contact = [
    skNewMessage, skSelect, skDeleteThread, skSelectThread,
    skSearch, skSettings
  ];

  var option_menu_thread_from_unkown = [
    skNewMessage, skSelect, skDeleteThread, skCreateNewContact,
    skAddToContact, skSelectThread, skSearch, skSettings
  ];

  var option_menu_activity_threadlist = [
    skCancel, skSelect
  ];

  var option_menu_activity_threadlist_noMessage = [
    skCancel
  ];

  function createMenu(actions) {
    if (!actions) return;
    var params = {
      header: {l10nId: 'options'},
      items: actions
    };
    if (exports.option) {
      exports.option.initSoftKeyPanel(params);
    } else {
      exports.option = new SoftkeyPanel(params);
    }
    exports.option.show();

    removeSoftKeyCache();
  }

  // Need delete the cache to confirm the page switch more smooth.
  function removeSoftKeyCache() {
    var softKeyCache = document.getElementById('cachedSoftkeyPanel');
    if (softKeyCache) {
      var softKeyCacheNodes = softKeyCache.children;
      for(var i = 0; i < softKeyCacheNodes.length; i++) {
        softKeyCacheNodes[i].querySelector('.sk-button').innerHTML = '';
      }
    }
  }

  function createBdiNode(content) {
    var bdi = document.createElement('bdi');
    bdi.textContent = content;
    return bdi;
  }

var ThreadListUI = {
  readyDeferred: Utils.Promise.defer(),

  draftLinks: null,
  draftRegistry: null,
  DRAFT_SAVED_DURATION: 2000,
  FIRST_PANEL_THREAD_COUNT: 9, // counted on a Peak

  // Used to track timeouts
  timeouts: {
    onDraftSaved: null
  },

  // Used to track the current number of rendered
  // threads. Updated in ThreadListUI.renderThreads
  count: 0,

  // Set to |true| when in edit mode
  inEditMode: false,

  currentThread: null,

  isSwitchCase: false,

  dialogShown: false,

  init: function thlui_init() {
    this.tmpl = {
      thread: Template('messages-thread-tmpl')
    };

    // TODO: https://bugzilla.mozilla.org/show_bug.cgi?id=854413
    [
      'container', 'no-messages',
      'check-uncheck-all-button','composer-link',
      'delete-button', 'edit-header','options-button',
      'edit-mode', 'edit-form', 'draft-saved-banner'
    ].forEach(function(id) {
      this[Utils.camelCase(id)] = document.getElementById('threads-' + id);
    }, this);

    this.mainWrapper = document.getElementById('main-wrapper');

    this.noResultMessage = document.getElementById('no-result-message');

    this.container.addEventListener(
      'click', this
    );

    window.addEventListener('index-changed', this);
    window.addEventListener('keydown', this);

    this.editForm.addEventListener(
      'submit', this
    );

    navigator.mozContacts.addEventListener(
      'contactchange',
      this.updateContactsInfo.bind(this)
    );

    this.draftLinks = new Map();
    ThreadListUI.draftRegistry = {};

    MessageManager.on('message-sending', this.onMessageSending.bind(this));
    MessageManager.on('message-received', this.onMessageReceived.bind(this));
    MessageManager.on('threads-deleted', this.onThreadsDeleted.bind(this));

    InterInstanceEventDispatcher.on(
      'drafts-changed',
      this.renderDrafts.bind(this, true /* force update */)
    );

    privateMembers.set(this, {
      // Very approximate number of letters that can fit into title for the
      // group thread, "100" is for all paddings, image width and so on,
      // 10 is approximate English char width for current 18px font size
      groupThreadTitleMaxLength: (window.innerWidth - 100) / 10,
      threadSwitchId: 1
    });

    this.sticky = null;
    this.initSks();
  },

  initStickyHeader: function thlui_initStickyHeader() {
    if (!this.sticky) {
      this.sticky =
        new StickyHeader(this.container, document.getElementById('sticky'));
    }
  },

  afterEnter: function thui_afterEnter(args) {
    // XXX, not good to use classList as a judgement, we should
    // add a variable later to mark no messages.
    if (ThreadListUI.noMessages &&
        !ThreadListUI.noMessages.classList.contains('hide')) {
      this.noResultMessage.focus();
    }
    this.updateSKs();
    window.addEventListener('gaia-confirm-open', this.alertOpen);
    window.addEventListener('gaia-confirm-close', this.alertClose);
    window.addEventListener('gaia-confirm-start-close', Utils.onDialogBeginClose);
  },

  beforeLeave: function thlui_beforeLeave() {
    // This should be in afterLeave, but the edit mode interface does not seem
    // to slide correctly. Bug 1009541
    this.cancelEdit();
    window.removeEventListener('gaia-confirm-open', this.alertOpen);
    window.removeEventListener('gaia-confirm-close', this.alertClose);
    window.removeEventListener('gaia-confirm-start-close', Utils.onDialogBeginClose);
  },

  alertOpen: function thui_alertOpen() {
    ThreadListUI.dialogShown = true;
  },

  alertClose: function thui_alertClose() {
    ThreadListUI.dialogShown = false;
    Utils.menuOptionVisible = false;
    ThreadListUI.updateSKs();
  },

  getAllInputs: function thlui_getAllInputs() {
    if (this.container) {
      return Array.prototype.slice.call(
        this.container.querySelectorAll('input[type=checkbox]')
      );
    } else {
      return [];
    }
  },

  setContact: function thlui_setContact(node) {
    // TODO Bug 1014226 will introduce a draftId instead of threadId for
    // drafts, this will allow removing the test with is-draft here.
    var threadOrDraft = node.classList.contains('is-draft') ?
      Drafts.get(node.dataset.threadId) :
      Threads.get(node.dataset.threadId);

    if (!threadOrDraft) {
      throw new Error('Thread node is invalid!');
    }

    var threadNumbers = threadOrDraft.participants || threadOrDraft.recipients;

    var titleContainer = node.querySelector('.threadlist-item-title');
    var title = titleContainer.firstElementChild;

    if (!threadNumbers || !threadNumbers.length) {
      title.setAttribute('data-l10n-id', 'no-recipient');
      return;
    }

    function* updateThreadNode(number) {
      var contact = yield ThreadListUI.findContact(number, { photoURL: true });
      var isContact = !!contact.isContact;

      title.textContent = contact.title || number;

      var photoUrl = node.dataset.photoUrl;
      if (photoUrl) {
        window.URL.revokeObjectURL(photoUrl);
      }

      if (contact.photoURL) {
        node.dataset.photoUrl = contact.photoURL;
      } else if (photoUrl) {
        node.dataset.photoUrl = '';
      }
    }

    function* updateGroupThreadNode(numbers, titleMaxLength) {
      var contactTitle, number;
      var i = 0;
      var threadTitleLength = 0;

      var groupTitle = document.createElement('span');
      var separatorNode = document.createElement('span');
      separatorNode.setAttribute(
        'data-l10n-id',
        'thread-participant-separator'
      );

      while (i < numbers.length && threadTitleLength < titleMaxLength) {
        number = numbers[i++];

        contactTitle = (yield ThreadListUI.findContact(number)).title || number;

        if (threadTitleLength > 0) {
          groupTitle.appendChild(separatorNode.cloneNode(true));
        }
        groupTitle.appendChild(createBdiNode(contactTitle));

        threadTitleLength += contactTitle.length;
      }

      titleContainer.replaceChild(groupTitle, title);
    }

    if (threadNumbers.length === 1) {
      return Utils.Promise.async(updateThreadNode)(threadNumbers[0]);
    }

    return Utils.Promise.async(updateGroupThreadNode)(
      threadNumbers, privateMembers.get(this).groupThreadTitleMaxLength
    );
  },

  findContact: function(number, options) {
    var defer = Utils.Promise.defer();

    Contacts.findByAddress(number, function(contacts) {
      var details = Utils.getContactDetails(number, contacts, options);

      if (!details.isContact) {
        Contacts.addUnknown(number);
      }

      defer.resolve(details);
    });

    return defer.promise;
  },

  handleEvent: function thlui_handleEvent(event) {
    if (!Navigation.isCurrentPanel('thread-list')) {
      // Need prevent the key event when current page is not main page.
      if (event.type === 'keydown' && event.key === 'Backspace') {
        event.preventDefault();
      }
      return;
    }
    var draftId;

    switch (event.type) {
      case 'index-changed':
        if (event.detail.panel === 'thread-list') {
          this.currentThread = event.detail.focusedItem;
          if (!window.option || ((window.option && !window.option.menuVisible) &&
              !ThreadListUI.dialogShown)) {
            this.updateSKs();
          }
        }
        break;

      case 'click':
        // Handle selection in selection module
        if (this.inEditMode || Navigation.isCurrentPanel('settings-view')) {
          return;
        }

        if ((draftId = this.draftLinks.get(event.target))) {
          // TODO: Bug 1010216: remove this
          ThreadUI.draft = Drafts.get(draftId);
        }
        var parent = event.target.parentNode;
        var parentThreadId = parent.dataset.threadId;

        if (parentThreadId) {
          event.preventDefault();
          // TODO Bug 1014226 will introduce a draftId instead of threadId for
          // drafts, this will allow removing the test with is-draft here.
          if (parent.classList.contains('is-draft')) {
            Navigation.toPanel('composer', {
              draftId: +parentThreadId
            });
          } else {
            Navigation.toPanel('thread', {
              id: +parentThreadId
            });
          }
        }

        break;
      case 'submit':
        event.preventDefault();
        break;
      case 'keydown':
        switch (event.key) {
          case 'Backspace':
          case 'BrowserBack':
            if (this.inEditMode) {
              event.preventDefault();
              this.cancelEdit();
              this.updateSKs();
            } else {
              if (Navigation.isCurrentPanel('thread-list')) {
                var el = document.getElementById('option-menu');
                if (!el || !el.classList.contains('visible')) {
                  // Delete cache when we save the new cache.
                  MessageCache.clear('threads-container');
                  // We do not need cache the no message page.
                  if (ThreadListUI.noMessages.classList.contains('hide')) {
                    this.saveCache();
                  }
                }
              }
            }
            break;
        }
        break;
    }
  },

  saveCache: function thui_saveCache() {
    var cacheHtml = document.getElementById('threads-container');
    var codeNode = MessageCache
      .cloneAsInertNodeAvoidingCustomElementHorrors(cacheHtml);
    MessageCache.saveFromNode('threads-container', codeNode);
  },

  launchComposer: function thui_launchComposer(e) {
    // prevent following the link, see also bug 1014219
    e.preventDefault();
    Navigation.toPanel('composer');
  },

  checkInputs: function thlui_checkInputs() {
    var selected = this.selectionHandler.selectedCount;

    if (selected === ThreadListUI.allInputs.length) {
      this.checkUncheckAllButton.setAttribute('data-l10n-id', 'deselect-all');
    } else {
      this.checkUncheckAllButton.setAttribute('data-l10n-id', 'select-all');
    }
    if (selected) {
      navigator.mozL10n.setAttributes(this.editMode, 'selected-threads', {
        n: selected
      });
    } else {
      navigator.mozL10n.setAttributes(this.editMode, 'selected-threads', {
        n: 0
      });
    }
  },

  clickCheckUncheckAllButton: function() {
    this.checkUncheckAllButton.click();
  },

  removeThread: function thlui_removeThread(threadId) {
    var li = document.getElementById('thread-' + threadId);
    var parent, draftId;
    var photoUrl = li && li.dataset.photoUrl;

    // Revoke the contact photo while deletion for avoiding intermittent
    // photo disappear issue.
    if (photoUrl) {
      window.URL.revokeObjectURL(photoUrl);
    }

    if (li) {
      parent = li.parentNode;
      li.remove();
    }

    if ((draftId = this.draftLinks.get(li))) {
      this.draftLinks.delete(li);

      delete this.draftRegistry[draftId];
    }

    // remove the header and the ul for an empty list
    if (parent && !parent.firstElementChild) {
      parent.previousSibling.remove();
      parent.remove();

      this.sticky && this.sticky.refresh();

      // if we have no more elements, set empty classes
      if (!this.container.querySelector('li')) {
        this.setEmpty(true);
      }
    }
  },

  // Since removeThread will revoke list photoUrl at the end of deletion,
  // please make sure url will also be revoked if new delete api remove threads
  // without calling removeThread in the future.
  delete: function thlui_delete() {
    function performDeletion() {
      /* jshint validthis: true */

      var threadIdsToDelete = [],
          messageIdsToDelete = [],
          threadCountToDelete = 0,
          selected = ThreadListUI.selectionHandler.selectedList;

      function exitEditMode() {
        ThreadListUI.cancelEdit();
        WaitingScreen.hide();
        ThreadListUI.updateSKs();
      }

      function onAllThreadMessagesRetrieved() {
        if (!--threadCountToDelete) {
          MessageManager.deleteMessages(messageIdsToDelete);

          threadIdsToDelete.forEach(function(threadId) {
            ThreadListUI.deleteThread(threadId);
          });

          messageIdsToDelete = threadIdsToDelete = null;

          exitEditMode();
        }
      }

      function onThreadMessageRetrieved(message) {
        messageIdsToDelete.push(message.id);
        return true;
      }

      WaitingScreen.show();
      if (exports.option) {
        exports.option.hide();
      }

      threadIdsToDelete = selected.reduce(function(list, value) {
        // Coerce the threadId back to a number MobileMessageFilter and all
        // other platform APIs expect this value to be a number.
        var threadId = +value;
        var isDraft = typeof Threads.get(threadId) === 'undefined';

        if (isDraft) {
          Drafts.delete(Drafts.get(threadId));
          ThreadListUI.removeThread(threadId);
        } else {
          list.push(threadId);
        }

        return list;
      }, []);

      // That means that we've just removed some drafts
      if (threadIdsToDelete.length !== selected.length) {
        Drafts.store();
      }

      if (!threadIdsToDelete.length) {
        exitEditMode();
        return;
      }

      threadCountToDelete = threadIdsToDelete.length;

      threadIdsToDelete.forEach(function(threadId) {
        MessageManager.getMessages({
          // Filter and request all messages with this threadId
          filter: { threadId: threadId },
          each: onThreadMessageRetrieved,
          end: onAllThreadMessagesRetrieved
        });
      });
    }

    Utils.confirmAlert('confirmation-title',
                       {id: 'deleteThreads-confirmation2',
                        args: {n: this.selectionHandler.selectedCount}},
                       'cancel', null, null, null, 'delete', deleteCallback);
    function deleteCallback() {
      performDeletion();
    }
  },

  setEmpty: function thlui_setEmpty(empty) {
    var addWhenEmpty = empty ? 'add' : 'remove';
    var removeWhenEmpty = empty ? 'remove' : 'add';

    ThreadListUI.noMessages.classList[removeWhenEmpty]('hide');
    ThreadListUI.container.classList[addWhenEmpty]('hide');

    // We should not update sks and focus if lunch from activity
    // and last page is not thread list.
    if (empty &&
        (!Startup.isActivity || Navigation.isCurrentPanel('thread-list'))) {
      this.noResultMessage.focus();
    }
  },

  initSks: function() {
    if (!Startup.useCache) {
      createMenu(option_menu_no_thread);
    }
  },

  updateSKs: function() {
    var threadid = null;
    if (document.getElementById('loading').classList.contains('show-loading')) {
      return;
    }
    if (Utils.menuOptionVisible) {
      return;
    }
    if (ThreadListUI.currentThread) {
      threadid = ThreadListUI.currentThread.dataset.threadId;
    }
    if (Startup.isActivity && Navigation.isCurrentPanel('thread-list')) {
      if (ThreadListUI.noMessages.classList.contains('hide')) {
        createMenu(option_menu_activity_threadlist);
      } else {
        this.noResultMessage.querySelector('p').classList.add('hide');
        createMenu(option_menu_activity_threadlist_noMessage);
      }
      return;
    }
    if (ThreadListUI.inEditMode) {
      var selected = this.selectionHandler.selectedCount;
      if (selected === ThreadListUI.allInputs.length) {
        createMenu(option_menu_edit_deselect_choosedall);
      } else if (selected > 0) {
        if (document.activeElement.querySelectorAll('.thread-checked').length !== 0 ||
            document.activeElement.classList.contains('thread-checked')) {
          createMenu(option_menu_edit_deselect_choose);
        } else {
          createMenu(option_menu_edit_choose);
        }
      } else {
        createMenu(option_menu_edit_normal);
      }
    } else {
      if (ThreadListUI.noMessages.classList.contains('hide')) {
        if (threadid) {
          createMenu(option_menu_thread_from_unkown);
          var tel;
          var thread;
          if (ThreadListUI.currentThread.classList.contains('draft')) {
            thread = Drafts.get(threadid);
            if (thread) {
              tel = thread.recipients[0];
            }
          } else {
            thread = Threads.get(threadid);
            if (thread) {
              tel = thread.participants[0];
            }
          }
          if (thread && tel) {
            Contacts.findByPhoneNumber(tel, (contact)=> {
              if (contact && contact.length) {
                createMenu(option_menu_thead_from_contact);
              }
            });
          }
        }
      } else {
        createMenu(option_menu_no_thread);
      }
    }
  },

  showOptions: function thlui_options() {
    var params = {
      items: [{
        l10nId: 'settings',
        method: function oSettings() {
          Navigation.toPanel('settings-view');
        }
      },{ // Last item is the Cancel button
        l10nId: 'cancel',
        incomplete: true
      }]
    };

    // Add delete option when list is not empty
    if (ThreadListUI.noMessages.classList.contains('hide')) {
      params.items.unshift({
        l10nId: 'selectThreads-label',
        method: this.startEdit.bind(this)
      });
    }

    new OptionMenu(params).show();
  },

  startEdit: function thlui_edit() {
    function editModeSetup() {
      /*jshint validthis:true */
      this.inEditMode = true;
      this.selectionHandler.cleanForm();
      this.mainWrapper.classList.toggle('edit');
    }

    if (!this.selectionHandler) {
      LazyLoader.load('js/selection_handler.js', () => {
        this.selectionHandler = new SelectionHandler({
          // Elements
          container: this.container,
          checkUncheckAllButton: this.checkUncheckAllButton,
          // Methods
          checkInputs: this.checkInputs.bind(this),
          getAllInputs: this.getAllInputs.bind(this),
          isInEditMode: this.isInEditMode.bind(this),
          updateSKs: this.updateSKs.bind(this)
        });
        editModeSetup.call(this);
      });
    } else {
      editModeSetup.call(this);
    }
  },

  isInEditMode: function thlui_isInEditMode() {
    return this.inEditMode;
  },

  cancelEdit: function thlui_cancelEdit() {
    this.inEditMode = false;
    this.mainWrapper.classList.remove('edit');
  },

  renderDrafts: function thlui_renderDrafts(force) {
    // Request and render all threads with drafts
    // or thread-less drafts.
    return Drafts.request(force).then(() => {
      Drafts.forEach(function(draft, threadId) {
        if (threadId) {
          // Find draft-containing threads that have already been rendered
          // and update them so they mark themselves appropriately
          var el = document.getElementById('thread-' + threadId);
          if (el) {
            this.updateThread(Threads.get(threadId));
          }
        } else {
          // Safely assume there is a threadless draft
          this.setEmpty(false);

          // If there is currently no list item rendered for this
          // draft, then proceed.
          if (!this.draftRegistry[draft.id]) {
            this.appendThread(
              Thread.create(draft)
            );
          }
        }
      }, this);

      this.sticky && this.sticky.refresh();
    });
  },

  renderCacheDrafts: function thlui_renderCacheDrafts() {
    return Drafts.request().then(function () {
      Drafts.forEach(function(draft, threadId) {
        if (draft) {
          ThreadListUI.setEmpty(false);
          var node = document.getElementById('thread-' + draft.id);
          var draftCache = node.querySelector('a');
          ThreadListUI.setContact(node);
          ThreadListUI.draftLinks.set(draftCache, draft.id);
        }
      }, this);
    });
  },

  prepareRendering: function thlui_prepareRendering() {
    if (!Startup.useCache) {
      this.container.innerHTML = '';
      this.renderDrafts();
    } else {
      this.renderCacheDrafts();
    }
  },

  startRendering: function thlui_startRenderingThreads() {
    this.setEmpty(false);
  },

  finalizeRendering: function thlui_finalizeRendering(empty) {
    if (empty && !Startup.useCache) {
      this.setEmpty(true);
    }

    if (!empty) {
      TimeHeaders.updateAll('header[data-time-update]');
    }

    this.sticky && this.sticky.refresh();

    // We should not update sks and focus if lunch from activity
    // and last page is not thread list.
    if (!Startup.isActivity || Navigation.isCurrentPanel('thread-list')) {
      this.updateSKs();
    }
  },

  ensureReadAheadSetting: function thlui_ensureReadAheadSettting() {
    Settings.setReadAheadThreadRetrieval(this.FIRST_PANEL_THREAD_COUNT);
  },

  renderThreads: function thlui_renderThreads(firstViewDoneCb) {
    window.performance.mark('willRenderThreads');
    PerformanceTestingHelper.dispatch('will-render-threads');

    var hasThreads = false;
    var firstPanelCount = this.FIRST_PANEL_THREAD_COUNT;

    this.prepareRendering();

    var firstViewDone = function firstViewDone() {
      this.initStickyHeader();

      if (typeof firstViewDoneCb === 'function') {
        firstViewDoneCb();
      }
    }.bind(this);

    function onRenderThread(thread) {
      /* jshint validthis: true */
      // Register all threads to the Threads object.
      Threads.set(thread.id, thread);

      // If one of the requested threads is also the currently displayed thread,
      // update the header immediately
      // TODO: Revise necessity of this code in bug 1050823
      if (Navigation.isCurrentPanel('thread', { id: thread.id })) {
        ThreadUI.updateHeaderData();
      }

      if (!hasThreads) {
        hasThreads = true;
        this.startRendering();
      }

      if (!Startup.useCache) {
        this.appendThread(thread);
      } else {
        // Update some status for cache, maybe there are better way
        // to update them. Use it first.
        this.updateCacheContact(thread);
        this.updateCacheContainerId(thread);
        this.updateCacheUnreadStatus(thread);
      }

      if (--firstPanelCount === 0) {
        // dispatch visually-complete and content-interactive when rendered
        // threads could fill up the top of the visiable area
        firstViewDone();
      }
    }

    function onThreadsRendered() {
      /* jshint validthis: true */

      /* We set the view as empty only if there's no threads and no drafts,
       * this is done to prevent races between renering threads and drafts. */
      this.finalizeRendering(!(hasThreads || Drafts.size));

      if (firstPanelCount > 0) {
        // dispatch visually-complete and content-interactive when rendering
        // ended but threads could not fill up the top of the visiable area
        firstViewDone();
      }
    }

    function onDone() {
      /* jshint validthis: true */

      this.readyDeferred.resolve();

      this.ensureReadAheadSetting();
      // Delete cache when we save the new cache.
      MessageCache.clear('threads-container');
      // We do not need cache the no message page.
      if (ThreadListUI.noMessages.classList.contains('hide')) {
        this.saveCache();
      }
    }

    MessageManager.getThreads({
      each: onRenderThread.bind(this),
      end: onThreadsRendered.bind(this),
      done: onDone.bind(this)
    });

    return this.readyDeferred.promise;
  },

  updateCacheContact: function thui_updateCacheContact(thread) {
    function searchContacts(DBNumber, threadDOM) {
      Contacts.findByAddress(DBNumber, function(contacts) {
        var details = Utils.getContactDetails(DBNumber, contacts);
        if (threadDOM) {
          if (details.isContact && details.title !== null) {
            threadDOM.textContent = details.title;
          } else {
            threadDOM.textContent = DBNumber;
          }
        }
      });
    }

    var bdiNodes = [];
    var detailsDOM = document.getElementById('thread-' + thread.id);
    if (detailsDOM) {
      var detailsDOMName = detailsDOM.querySelector('.threadlist-item-title');
      var detailsDOMBdi = detailsDOMName.querySelectorAll('bdi');
      for (var index = 0; index < detailsDOMBdi.length; index++) {
        bdiNodes.push(detailsDOMBdi[index]);
      }

      for (var i = 0; i < thread.participants.length; i++) {
        var number = thread.participants[i];
        var threadListNode = bdiNodes[i];
        if (threadListNode) {
          searchContacts(number, threadListNode);
        } else {
          searchContacts(number, null);
        }
      }
    }
  },

  // We should update the container id if the time zone change.
  updateCacheContainerId: function thui_updateCacheThreadId(thread) {
    var timestamp = +thread.timestamp;
    var drafts = Drafts.byThreadId(thread.id);
    if (drafts.length) {
      timestamp = Math.max(drafts.latest.timestamp, timestamp);
    }
    var dayDate = Utils.getDayDate(timestamp);
    var threadNode = document.getElementById('thread-' + thread.id);
    if (threadNode) {
      threadNode.parentNode.id = 'threadsContainer_' + dayDate;
    }
  },

  updateCacheUnreadStatus: function thui_updateCacheUnreadStatus(thread) {
    var readState = (thread.unreadCount === 0) ? 'read' : 'unread';
    this.mark(thread.id, readState);
  },

  createThread: function thlui_createThread(record) {
    // Create DOM element
    var li = document.createElement('li');
    var timestamp = +record.timestamp;
    var type = record.lastMessageType;
    var participants = record.participants;
    var number = participants[0];
    var id = record.id;
    var bodyHTML = record.body;
    var thread = Threads.get(id);
    var draft, draftId;
    var iconLabel = '';

    // A new conversation "is" a draft
    var isDraft = typeof thread === 'undefined';

    // A an existing conversation "has" a draft
    // (or it doesn't, depending on the value
    // returned by thread.hasDrafts)
    var hasDrafts = isDraft ? false : thread.hasDrafts;

    if (hasDrafts) {
      draft = Drafts.byThreadId(thread.id).latest;
      timestamp = Math.max(draft.timestamp, timestamp);
      // If the draft is newer than the message, update
      // the body with the draft content's first string.
      if (draft.timestamp >= record.timestamp) {
        bodyHTML = draft.content.find(function(content) {
          if (typeof content === 'string') {
            return true;
          }
        });
        type = draft.type;
      }
    }

    bodyHTML = Template.escape(bodyHTML || '');

    li.id = 'thread-' + id;
    li.dataset.threadId = id;
    li.dataset.time = timestamp;
    li.dataset.lastMessageType = type;
    li.classList.add('threadlist-item');
    li.classList.add('navigable');

    if (hasDrafts || isDraft) {
      // Set the "draft" visual indication
      li.classList.add('draft');

      if (hasDrafts) {
        li.classList.add('has-draft');
        iconLabel = 'has-draft';
      } else {
        li.classList.add('is-draft');
        iconLabel = 'is-draft';
      }


      draftId = hasDrafts ? draft.id : record.id;

      // Used in renderDrafts as an efficient mechanism
      // for checking whether a draft of a specific ID
      // has been rendered.
      this.draftRegistry[draftId] = true;
    }

    if (record.unreadCount > 0) {
      li.classList.add('unread');
      iconLabel = 'unread-thread';
    }

    // Render markup with thread data
    li.innerHTML = this.tmpl.thread.interpolate({
      hash: isDraft ? '#composer' : '#thread=' + id,
      mode: isDraft ? 'drafts' : 'threads',
      id: isDraft ? draftId : id,
      number: number,
      bodyHTML: bodyHTML,
      timestamp: String(timestamp),
      iconLabel: iconLabel
    }, {
      safe: ['id', 'bodyHTML']
    });

    TimeHeaders.update(li.querySelector('time'));

    if (draftId) {
      // Used in handleEvent to set the ThreadUI.draft object
      this.draftLinks.set(
        li.querySelector('a'), draftId
      );
    }

    return li;
  },

  deleteCurrentThread: function() {
    var messageIdsToDelete = [];
    var threadId = null;
    if (ThreadListUI.currentThread) {
      threadId = ThreadListUI.currentThread.dataset.threadId;
    }

    function onAllThreadMessagesRetrieved() {
      MessageManager.deleteMessages(messageIdsToDelete);
      ThreadListUI.deleteThread(threadId);
      messageIdsToDelete = null;
      ThreadListUI.currentThread = null;
      WaitingScreen.hide();
    }

    function onThreadMessageRetrieved(message) {
      messageIdsToDelete.push(message.id);
      return true;
    }

    function performDelete() {
      WaitingScreen.show();
      if (exports.option) {
        exports.option.hide();
      }
      var isDraft = typeof Threads.get(threadId) === 'undefined';
      if (isDraft) {
        Drafts.delete(Drafts.get(threadId));
        ThreadListUI.removeThread(threadId);
        Drafts.store();
        WaitingScreen.hide();
        ThreadListUI.currentThread = null;
      } else {
        MessageManager.getMessages({
          filter: {threadId: threadId},
          each: onThreadMessageRetrieved,
          end: onAllThreadMessagesRetrieved
        });
      }
    }

    function deleteCallback() {
      performDelete();
    }

    if (threadId) {
      Utils.confirmAlert('confirmation-title',
                         'delete-current-thread?',
                         'cancel', null, null, null,
                         'delete', deleteCallback);
    }
    Utils.menuOptionVisible = false;
  },

  deleteThread: function(threadId) {
    // Threads.delete will handle deleting
    // any Draft objects associated with the
    // specified threadId.
    Threads.delete(threadId);

    // Cleanup the DOM
    this.removeThread(threadId);

    // Remove notification if exist
    Utils.closeNotificationsForThread(threadId);

    // Delete cache when we save the new cache.
    MessageCache.clear('threads-container');
    // We do not need cache the no message page.
    if (ThreadListUI.noMessages.classList.contains('hide')) {
      this.saveCache();
    }
  },

  insertThreadContainer:
    function thlui_insertThreadContainer(group, timestamp) {
    // We look for placing the group in the right place.
    var headers = ThreadListUI.container.getElementsByTagName('header');
    var groupFound = false;
    if (headers.length > 0 &&
      group.childNodes[1].id == 'threadsContainer_cellbroadcast') {
      ThreadListUI.container.insertBefore(group, headers[0].parentNode);
      return;
    }

    console.log("chengyan insertThreadContainer timestamp " + timestamp);

    for (var i = 0; i < headers.length; i++) {
      console.log("chengyan insertThreadContainer headers[i].dataset.time " + headers[i].dataset.time);
      if (headers[i].dataset.lastMessageType != 'cellbroadcast' &&
        timestamp >= headers[i].dataset.time) {
    /*for (var i = 0; i < headers.length; i++) {
      if (timestamp >= headers[i].dataset.time) {*/
        groupFound = true;
        ThreadListUI.container.insertBefore(group, headers[i].parentNode);
        break;
      }
    }
    console.log("chengyan insertThreadContainer groupFound " + groupFound);
    if (!groupFound) {
      ThreadListUI.container.appendChild(group);
    }
  },

  updateThread: function thlui_updateThread(record, options) {
    var thread = Thread.create(record, options);
    var threadUINode = document.getElementById('thread-' + thread.id);
    var threadUITime = threadUINode ? +threadUINode.dataset.time : NaN;
    var recordTime = +thread.timestamp;
    if (options && options.timestamp) {
      recordTime = options.timestamp;
    }
    // For legitimate in-memory thread objects, update the stored
    // Thread instance with the newest data. This check prevents
    // draft objects from inadvertently creating bogus thread
    // objects.
    if (Threads.has(thread.id)) {
      Threads.set(thread.id, thread);
    }

    // Edge case: if we just received a message that is older than the latest
    // one in the thread, we only need to update the 'unread' status.
    var newMessageReceived = options && options.unread;
    if (newMessageReceived && threadUITime > recordTime) {
      this.mark(thread.id, 'unread');
      MessageCache.clear('threads-container');
      this.saveCache();
      return;
    }

    // If we just deleted messages in a thread but kept the last message
    // unchanged, we don't need to update the thread UI.
    var messagesDeleted = options && options.deleted;
    if (messagesDeleted && threadUITime === recordTime) {
      return;
    }

    // General case: update the thread UI.
    if (threadUINode) {
      // remove the current thread node in order to place the new one properly
      this.removeThread(thread.id);
    }

    this.setEmpty(false);
    if (this.appendThread(thread)) {
      this.sticky && this.sticky.refresh();
    }

    // Delete cache when we save the new cache.
    MessageCache.clear('threads-container');
    // We do not need cache the no message page.
    if (ThreadListUI.noMessages.classList.contains('hide')) {
      this.saveCache();
    }
  },

  _canSwitchBetweenThreads: function() {
    return (Navigation.isCurrentPanel('thread')
            && ((!Compose.isFocused() && !Compose.isSubjectFocused()) && !window.option.menuVisible));
  },

  _switchPrevNextThread: function(shift) {
    var currentId = Threads.currentId,
        threads = [];

    if (!Compose.isEmpty()
        || (!!ThreadUI.recipients && ThreadUI.recipients.length !== 0)) {
      ThreadUI.saveDraft({
        'preserve': true,
        'autoSave': true
      });
    } else {
      ThreadUI.discardDraft();
    }

    Threads.forEach( (v,k) => threads.push( k ) );
    this.isSwitchCase = true;
    var destId = threads[ (threads.indexOf( currentId ) + shift + threads.length) % threads.length ];
    // We need backup the thread id to confirm there is not confuse
    // when more than 5 messages switch thread.
    ThreadUI.threadIdBackUp = destId;
    Navigation.toPanel('thread', { id: destId });
  },

  switchPreviousThread: function thlui_switchPreviousThread() {
    this._switchPrevNextThread(-privateMembers.get(this).threadSwitchId);
  },

  switchNextThread: function thlui_switchNextThread() {
    this._switchPrevNextThread(+privateMembers.get(this).threadSwitchId);
  },

  onMessageSending: function thlui_onMessageSending(e) {
    this.updateThread(e.message);
  },

  onMessageReceived: function thlui_onMessageReceived(e) {
    // If user currently in the same thread, then mark thread as read
    var markAsRead = Navigation.isCurrentPanel('thread', {
      id: e.message.threadId
    });

    this.updateThread(e.message, { unread: !markAsRead });
  },

  onThreadsDeleted: function thlui_onThreadDeleted(e) {
    e.ids.forEach(function(threadId) {
      if (Threads.has(threadId)) {
        this.deleteThread(threadId);
      }
    }, this);
    var count = e.ids.length;
    if (count) {
      Toaster.showToast({
        messageL10nId: 'deleted-threads',
        messageL10nArgs: {n: count},
        latency: 2000
      });
    }
  },

  /**
   * Append a thread to the global threads container. Creates a time container
   * (i.e. for a day or some other time period) for this thread if it doesn't
   * exist already.
   *
   * @return Boolean true if a time container was created, false otherwise
   */
  appendThread: function thlui_appendThread(thread) {
    if (navigator.mozL10n.readyState !== 'complete') {
      navigator.mozL10n.once(this.appendThread.bind(this, thread));
      return;
    }

    var timestamp = +thread.timestamp;
    var drafts = Drafts.byThreadId(thread.id);
    var firstThreadInContainer = false;

    if (drafts.length) {
      timestamp = Math.max(drafts.latest.timestamp, timestamp);
    }

    // We create the DOM element of the thread
    var node = this.createThread(thread);
    console.log("chengyan insert thread body " + thread.body);

    // Update info given a number
    this.setContact(node);

    // Is there any container already?
    var threadsContainerID = 'threadsContainer_' + Utils.getDayDate(timestamp);
    var threadsContainer = document.getElementById(threadsContainerID);
    // If there is no container we create & insert it to the DOM
    if (!threadsContainer) {
      // We create the wrapper with a 'header' & 'ul'
      var threadsContainerWrapper =
        ThreadListUI.createThreadContainer(timestamp);
      // Update threadsContainer with the new value
      threadsContainer = threadsContainerWrapper.childNodes[1];
      // Place our new content in the DOM
      ThreadListUI.insertThreadContainer(threadsContainerWrapper, timestamp);
      // We had to create a container, so this will be the first thread in it.
      firstThreadInContainer = true;
    }

    console.log("chengyan timestamp appendThread " + timestamp);
    // Where have I to place the new thread?
    var threads = threadsContainer.getElementsByTagName('li');
    var threadFound = false;
    for (var i = 0, l = threads.length; i < l; i++) {
      console.log("chengyan threads[i].dataset.time "  + threads[i].dataset.time);
      if (timestamp > threads[i].dataset.time) {
        threadFound = true;
        threadsContainer.insertBefore(node, threads[i]);
        break;
      }
    }

    console.log("chengyan threadFound " + threadFound);
    if (!threadFound) {
      threadsContainer.appendChild(node);
    }

    if (this.inEditMode) {
      // Remove the new added thread id from the selection handler
      this.selectionHandler.unselect(thread.id);

      this.checkInputs();
    }
    return firstThreadInContainer;
  },

  // Adds a new grouping header if necessary (today, tomorrow, ...)
  createThreadContainer: function thlui_createThreadContainer(timestamp) {
    var threadContainer = document.createElement('div');
    // Create Header DOM Element
    var headerDOM = document.createElement('header');

    // The id is used by the sticky header code as the -moz-element target.
    headerDOM.id = 'header_' + timestamp;
    headerDOM.className = 'h3';

    // Append 'time-update' state
    headerDOM.dataset.timeUpdate = 'repeat';
    headerDOM.dataset.time = timestamp;
    headerDOM.dataset.dateOnly = true;

    // Create UL DOM Element
    var threadsContainerDOM = document.createElement('ul');
    threadsContainerDOM.id = 'threadsContainer_' +
                                Utils.getDayDate(timestamp);
/*      // Add text
      headerDOM.innerHTML = Utils.getHeaderDate(timestamp);
      console.log("chengyan Utils.getHeaderDate(timestamp) " + Utils.getHeaderDate(timestamp));
    } else {
      threadsContainerDOM.id = 'threadsContainer_cellbroadcast';
      headerDOM.innerHTML = 'WEA Messages';
    }*/
    // Add text
    headerDOM.innerHTML = Utils.getHeaderDate(timestamp);

    // Add to DOM all elements
    threadContainer.appendChild(headerDOM);
    threadContainer.appendChild(threadsContainerDOM);
    return threadContainer;
  },

  // Method for updating all contact info after creating a contact
  updateContactsInfo: function thlui_updateContactsInfo() {
    Contacts.clearUnknown();
    // Prevents cases where updateContactsInfo method is called
    // before ThreadListUI.container exists (as observed by errors
    // in the js console)
    if (!this.container) {
      return;
    }
    // Retrieve all 'li' elements
    var threads = this.container.getElementsByTagName('li');

    [].forEach.call(threads, this.setContact.bind(this));
  },

  mark: function thlui_mark(id, current) {
    var li = document.getElementById('thread-' + id);
    var remove = 'read';

    if (current === 'read') {
      remove = 'unread';
    }

    if (li) {
      li.classList.remove(remove);
      li.classList.add(current);
    }
  },

  onDraftSaved: function thlui_onDraftSaved() {
    Toaster.showToast({
      messageL10nId: 'message-draft-saved',
      latency: this.DRAFT_SAVED_DURATION
    });
  },
  onDraftDiscarded: function thlui_onDraftDiscarded() {
    Toaster.showToast({
      messageL10nId: 'draft-discard-content',
      latency: this.DRAFT_SAVED_DURATION
    });
  },

  whenReady: function() {
    return this.readyDeferred.promise;
  }
};

Object.defineProperty(ThreadListUI, 'allInputs', {
  get: function() {
    return this.getAllInputs();
  }
});

exports.ThreadListUI = ThreadListUI;

}(this));
