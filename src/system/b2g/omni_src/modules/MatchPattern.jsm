"use strict";const Cu=Components.utils;Cu.import("resource://gre/modules/XPCOMUtils.jsm");XPCOMUtils.defineLazyModuleGetter(this,"NetUtil","resource://gre/modules/NetUtil.jsm");this.EXPORTED_SYMBOLS=["MatchPattern","MatchGlobs"];const PERMITTED_SCHEMES=["http","https","file","ftp","app","data"];const PERMITTED_SCHEMES_REGEXP=PERMITTED_SCHEMES.join("|");function globToRegexp(pat,allowQuestion){pat=pat.replace(/[.+^${}()|[\]\\]/g,"\\$&");if(allowQuestion){pat=pat.replace(/\?/g,".");}else{pat=pat.replace(/\?/g,"\\?");}
pat=pat.replace(/\*/g,".*");return new RegExp("^"+pat+"$");}

function SingleMatchPattern(pat){if(pat=="<all_urls>"){this.schemes=PERMITTED_SCHEMES;this.host="*";this.path=new RegExp(".*");}else if(!pat){this.schemes=[];}else{let re=new RegExp(`^(${PERMITTED_SCHEMES_REGEXP}|\\*)://(\\*|\\*\\.[^*/]+|[^*/]+|)(/.*)$`);let match=re.exec(pat);if(!match){Cu.reportError(`Invalid match pattern: '${pat}'`);this.schemes=[];return;}
if(match[1]=="*"){this.schemes=["http","https"];}else{this.schemes=[match[1]];}
this.host=match[2];this.path=globToRegexp(match[3],false);if(this.host==""&&this.schemes[0]!="file"){Cu.reportError(`Invalid match pattern: '${pat}'`);this.schemes=[];return;}}}
SingleMatchPattern.prototype={matches(uri,ignorePath=false){if(!this.schemes.includes(uri.scheme)){return false;}
if(this.host=="*"){}else if(this.host[0]=="*"){let suffix=this.host.substr(2);if(uri.host!=suffix&&!uri.host.endsWith("."+suffix)){return false;}}else if(this.host!=uri.host){return false;}
if(!ignorePath&&!this.path.test(uri.path)){return false;}
return true;},};this.MatchPattern=function(pat){this.pat=pat;if(!pat){this.matchers=[];}else if(pat instanceof String||typeof(pat)=="string"){this.matchers=[new SingleMatchPattern(pat)];}else{this.matchers=pat.map(p=>new SingleMatchPattern(p));}};MatchPattern.prototype={matches(uri){for(let matcher of this.matchers){if(matcher.matches(uri)){return true;}}
return false;},matchesIgnoringPath(uri){for(let matcher of this.matchers){if(matcher.matches(uri,true)){return true;}}
return false;},
matchesCookie(cookie){let secureURI=NetUtil.newURI(`https://${cookie.rawHost}/`);if(this.matchesIgnoringPath(secureURI)){return true;}
let plainURI=NetUtil.newURI(`http://${cookie.rawHost}/`);if(!cookie.isSecure&&this.matchesIgnoringPath(plainURI)){return true;}
if(!cookie.isDomain){return false;}




let{host,isSecure}=cookie;for(let matcher of this.matchers){let schemes=matcher.schemes;if(schemes.includes("https")||(!isSecure&&schemes.includes("http"))){if(matcher.host.endsWith(host)){return true;}}}
return false;},serialize(){return this.pat;},};this.MatchGlobs=function(globs){this.original=globs;if(globs){this.regexps=Array.from(globs,(glob)=>globToRegexp(glob,true));}else{this.regexps=[];}};MatchGlobs.prototype={matches(str){return this.regexps.some(regexp=>regexp.test(str));},serialize(){return this.original;},};