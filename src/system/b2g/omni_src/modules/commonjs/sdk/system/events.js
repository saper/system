'use strict';module.metadata={'stability':'unstable'};const{Cc,Ci,Cu}=require('chrome');const{Unknown}=require('../platform/xpcom');const{Class}=require('../core/heritage');const{ns}=require('../core/namespace');const observerService=Cc['@mozilla.org/observer-service;1'].getService(Ci.nsIObserverService);const{addObserver,removeObserver,notifyObservers}=observerService;const{ShimWaiver}=Cu.import("resource://gre/modules/ShimWaiver.jsm");const addObserverNoShim=ShimWaiver.getProperty(observerService,"addObserver");const removeObserverNoShim=ShimWaiver.getProperty(observerService,"removeObserver");const notifyObserversNoShim=ShimWaiver.getProperty(observerService,"notifyObservers");const unloadSubject=require('@loader/unload');const Subject=Class({extends:Unknown,initialize:function initialize(object){



this.wrappedJSObject={observersModuleSubjectWrapper:true,object:object};},getScriptableHelper:function(){},getInterfaces:function(){}});function emit(type,event,shimmed=false){


 let subject=event&&typeof event==='object'&&'subject'in event?Subject(event.subject):null;let data=event&&typeof event==='object'?('data'in event?event.data:null):
event;if(shimmed){notifyObservers(subject,type,data);}else{notifyObserversNoShim(subject,type,data);}}
exports.emit=emit;const Observer=Class({extends:Unknown,initialize:function initialize(listener){this.listener=listener;},interfaces:['nsIObserver','nsISupportsWeakReference'],observe:function(subject,topic,data){


if(subject&&typeof(subject)=='object'&&('wrappedJSObject'in subject)&&('observersModuleSubjectWrapper'in subject.wrappedJSObject))
subject=subject.wrappedJSObject.object;try{this.listener({type:topic,subject:subject,data:data});}
catch(error){console.exception(error);}}});const subscribers=ns();function on(type,listener,strong,shimmed=false){
let weak=!strong;let observers=subscribers(listener);
if(!(type in observers)){let observer=Observer(listener);observers[type]=observer;if(shimmed){addObserver(observer,type,weak);}else{addObserverNoShim(observer,type,weak);} 
let ref=Cu.getWeakReference(observer);weakRefs.set(observer,ref);stillAlive.set(ref,type);wasShimmed.set(ref,shimmed);}}
exports.on=on;function once(type,listener,shimmed=false){


on(type,listener,shimmed);on(type,function cleanup(){off(type,listener,shimmed);off(type,cleanup,shimmed);},true,shimmed);}
exports.once=once;function off(type,listener,shimmed=false){let observers=subscribers(listener);
if(type in observers){let observer=observers[type];delete observers[type];if(shimmed){removeObserver(observer,type);}else{removeObserverNoShim(observer,type);}
stillAlive.delete(weakRefs.get(observer));wasShimmed.delete(weakRefs.get(observer));}}
exports.off=off;var weakRefs=new WeakMap();var stillAlive=new Map();var wasShimmed=new Map();on('sdk:loader:destroy',function onunload({subject,data:reason}){ if(subject.wrappedJSObject===unloadSubject){off('sdk:loader:destroy',onunload,false); if(reason==='shutdown')
return;stillAlive.forEach((type,ref)=>{let observer=ref.get();if(observer){if(wasShimmed.get(ref)){removeObserver(observer,type);}else{removeObserverNoShim(observer,type);}}})}
},true,false);