"use strict";const{Cc,Ci,Cu,Cr}=require("chrome");const Services=require("Services");Cu.import("resource://gre/modules/XPCOMUtils.jsm");loader.lazyRequireGetter(this,"NetworkHelper","devtools/shared/webconsole/network-helper");loader.lazyRequireGetter(this,"DevToolsUtils","devtools/shared/DevToolsUtils");loader.lazyImporter(this,"NetUtil","resource://gre/modules/NetUtil.jsm");loader.lazyServiceGetter(this,"gActivityDistributor","@mozilla.org/network/http-activity-distributor;1","nsIHttpActivityDistributor");
const PR_UINT32_MAX=4294967295;const HTTP_MOVED_PERMANENTLY=301;const HTTP_FOUND=302;const HTTP_SEE_OTHER=303;const HTTP_TEMPORARY_REDIRECT=307;const RESPONSE_BODY_LIMIT=1048576;function NetworkResponseListener(owner,httpActivity){this.owner=owner;this.receivedData="";this.httpActivity=httpActivity;this.bodySize=0;let channel=this.httpActivity.channel;this._wrappedNotificationCallbacks=channel.notificationCallbacks;channel.notificationCallbacks=this;}
exports.NetworkResponseListener=NetworkResponseListener;NetworkResponseListener.prototype={QueryInterface:XPCOMUtils.generateQI([Ci.nsIStreamListener,Ci.nsIInputStreamCallback,Ci.nsIRequestObserver,Ci.nsIInterfaceRequestor,Ci.nsISupports]), getInterface(iid){if(iid.equals(Ci.nsIProgressEventSink)){return this;}
if(this._wrappedNotificationCallbacks){return this._wrappedNotificationCallbacks.getInterface(iid);}
throw Cr.NS_ERROR_NO_INTERFACE;},_forwardNotification(iid,method,args){if(!this._wrappedNotificationCallbacks){return;}
try{let impl=this._wrappedNotificationCallbacks.getInterface(iid);impl[method].apply(impl,args);}catch(e){if(e.result!=Cr.NS_ERROR_NO_INTERFACE){throw e;}}},_foundOpenResponse:false,_wrappedNotificationCallbacks:null,owner:null,sink:null,httpActivity:null,receivedData:null,bodySize:null,transferredSize:null,request:null,setAsyncListener:function(stream,listener){stream.asyncWait(listener,0,0,Services.tm.mainThread);},onDataAvailable:function(request,context,inputStream,offset,count){this._findOpenResponse();let data=NetUtil.readInputStreamToString(inputStream,count);this.bodySize+=count;if(!this.httpActivity.discardResponseBody&&this.receivedData.length<RESPONSE_BODY_LIMIT){this.receivedData+=NetworkHelper.convertToUnicode(data,request.contentCharset);}},onStartRequest:function(request){if(this.request){return;}
this.request=request;this._getSecurityInfo();this._findOpenResponse();
this.offset=0;




let channel=this.request;if(!this.httpActivity.fromServiceWorker&&channel instanceof Ci.nsIEncodedChannel&&channel.contentEncodings&&!channel.applyConversion){let encodingHeader=channel.getResponseHeader("Content-Encoding");let scs=Cc["@mozilla.org/streamConverters;1"].getService(Ci.nsIStreamConverterService);let encodings=encodingHeader.split(/\s*\t*,\s*\t*/);let nextListener=this;let acceptedEncodings=["gzip","deflate","x-gzip","x-deflate"];for(let i in encodings){ let enc=encodings[i].toLowerCase();if(acceptedEncodings.indexOf(enc)>-1){this.converter=scs.asyncConvertData(enc,"uncompressed",nextListener,null);nextListener=this.converter;}}
if(this.converter){this.converter.onStartRequest(this.request,null);}}
this.setAsyncListener(this.sink.inputStream,this);},_getSecurityInfo:DevToolsUtils.makeInfallible(function(){


let secinfo=this.httpActivity.channel.securityInfo;let info=NetworkHelper.parseSecurityInfo(secinfo,this.httpActivity);this.httpActivity.owner.addSecurityInfo(info);}),onStopRequest:function(){this._findOpenResponse();this.sink.outputStream.close();}, onProgress:function(request,context,progress,progressMax){this.transferredSize=progress;
this._forwardNotification(Ci.nsIProgressEventSink,"onProgress",arguments);},onStatus:function(){this._forwardNotification(Ci.nsIProgressEventSink,"onStatus",arguments);},_findOpenResponse:function(){if(!this.owner||this._foundOpenResponse){return;}
let openResponse=null;for(let id in this.owner.openResponses){let item=this.owner.openResponses[id];if(item.channel===this.httpActivity.channel){openResponse=item;break;}}
if(!openResponse){return;}
this._foundOpenResponse=true;delete this.owner.openResponses[openResponse.id];this.httpActivity.owner.addResponseHeaders(openResponse.headers);this.httpActivity.owner.addResponseCookies(openResponse.cookies);},onStreamClose:function(){if(!this.httpActivity){return;}
this.setAsyncListener(this.sink.inputStream,null);this._findOpenResponse();if(!this.httpActivity.discardResponseBody&&this.receivedData.length){this._onComplete(this.receivedData);}else if(!this.httpActivity.discardResponseBody&&this.httpActivity.responseStatus==304){let charset=this.request.contentCharset||this.httpActivity.charset;NetworkHelper.loadFromCache(this.httpActivity.url,charset,this._onComplete.bind(this));}else{this._onComplete();}},_onComplete:function(data){let response={mimeType:"",text:data||"",};response.size=response.text.length;response.transferredSize=this.transferredSize;try{response.mimeType=this.request.contentType;}catch(ex){}
if(!response.mimeType||!NetworkHelper.isTextMimeType(response.mimeType)){response.encoding="base64";try{response.text=btoa(response.text);}catch(err){}}
if(response.mimeType&&this.request.contentCharset){response.mimeType+="; charset="+this.request.contentCharset;}
this.receivedData="";this.httpActivity.owner.addResponseContent(response,this.httpActivity.discardResponseBody);this._wrappedNotificationCallbacks=null;this.httpActivity.channel=null;this.httpActivity.owner=null;this.httpActivity=null;this.sink=null;this.inputStream=null;this.converter=null;this.request=null;this.owner=null;},onInputStreamReady:function(stream){if(!(stream instanceof Ci.nsIAsyncInputStream)||!this.httpActivity){return;}
let available=-1;try{available=stream.available();}catch(ex){}
if(available!=-1){if(available!=0){if(this.converter){this.converter.onDataAvailable(this.request,null,stream,this.offset,available);}else{this.onDataAvailable(this.request,null,stream,this.offset,available);}}
this.offset+=available;this.setAsyncListener(stream,this);}else{this.onStreamClose();this.offset=0;}},};function NetworkMonitor(filters,owner){if(filters){this.window=filters.window;this.appId=filters.appId;this.topFrame=filters.topFrame;}
if(!this.window&&!this.appId&&!this.topFrame){this._logEverything=true;}
this.owner=owner;this.openRequests={};this.openResponses={};this._httpResponseExaminer=DevToolsUtils.makeInfallible(this._httpResponseExaminer).bind(this);this._serviceWorkerRequest=this._serviceWorkerRequest.bind(this);}
exports.NetworkMonitor=NetworkMonitor;NetworkMonitor.prototype={_logEverything:false,window:null,appId:null,topFrame:null,httpTransactionCodes:{0x5001:"REQUEST_HEADER",0x5002:"REQUEST_BODY_SENT",0x5003:"RESPONSE_START",0x5004:"RESPONSE_HEADER",0x5005:"RESPONSE_COMPLETE",0x5006:"TRANSACTION_CLOSE",0x804b0003:"STATUS_RESOLVING",0x804b000b:"STATUS_RESOLVED",0x804b0007:"STATUS_CONNECTING_TO",0x804b0004:"STATUS_CONNECTED_TO",0x804b0005:"STATUS_SENDING_TO",0x804b000a:"STATUS_WAITING_FOR",0x804b0006:"STATUS_RECEIVING_FROM"},
responsePipeSegmentSize:null,owner:null,saveRequestAndResponseBodies:true,openRequests:null,openResponses:null,init:function(){this.responsePipeSegmentSize=Services.prefs.getIntPref("network.buffer.cache.size");this.interceptedChannels=new Set();if(Services.appinfo.processType!=Ci.nsIXULRuntime.PROCESS_TYPE_CONTENT){gActivityDistributor.addObserver(this);Services.obs.addObserver(this._httpResponseExaminer,"http-on-examine-response",false);Services.obs.addObserver(this._httpResponseExaminer,"http-on-examine-cached-response",false);}
 
Services.obs.addObserver(this._serviceWorkerRequest,"service-worker-synthesized-response",false);},_serviceWorkerRequest:function(subject,topic,data){let channel=subject.QueryInterface(Ci.nsIHttpChannel);if(!this._matchRequest(channel)){return;}
this.interceptedChannels.add(subject);if(Services.appinfo.processType==Ci.nsIXULRuntime.PROCESS_TYPE_CONTENT){this._httpResponseExaminer(channel,"http-on-examine-cached-response");}},_httpResponseExaminer:function(subject,topic){


if(!this.owner||(topic!="http-on-examine-response"&&topic!="http-on-examine-cached-response")||!(subject instanceof Ci.nsIHttpChannel)){return;}
let channel=subject.QueryInterface(Ci.nsIHttpChannel);if(!this._matchRequest(channel)){return;}
let response={id:gSequenceId(),channel:channel,headers:[],cookies:[],};let setCookieHeader=null;channel.visitResponseHeaders({visitHeader:function(name,value){let lowerName=name.toLowerCase();if(lowerName=="set-cookie"){setCookieHeader=value;}
response.headers.push({name:name,value:value});}});if(!response.headers.length){return;}
if(setCookieHeader){response.cookies=NetworkHelper.parseSetCookieHeader(setCookieHeader);}
let httpVersionMaj={};let httpVersionMin={};channel.QueryInterface(Ci.nsIHttpChannelInternal);channel.getResponseVersion(httpVersionMaj,httpVersionMin);response.status=channel.responseStatus;response.statusText=channel.responseStatusText;response.httpVersion="HTTP/"+httpVersionMaj.value+"."+
httpVersionMin.value;this.openResponses[response.id]=response;if(topic==="http-on-examine-cached-response"){let fromServiceWorker=this.interceptedChannels.has(channel);this.interceptedChannels.delete(channel);

let httpActivity=this._createNetworkEvent(channel,{fromCache:!fromServiceWorker,fromServiceWorker:fromServiceWorker});httpActivity.owner.addResponseStart({httpVersion:response.httpVersion,remoteAddress:"",remotePort:"",status:response.status,statusText:response.statusText,headersSize:0,},"",true);
let timings=this._setupHarTimings(httpActivity,true);httpActivity.owner.addEventTimings(timings.total,timings.timings);}},observeActivity:DevToolsUtils.makeInfallible(function(channel,activityType,activitySubtype,timestamp,extraSizeData,extraStringData){if(!this.owner||activityType!=gActivityDistributor.ACTIVITY_TYPE_HTTP_TRANSACTION&&activityType!=gActivityDistributor.ACTIVITY_TYPE_SOCKET_TRANSPORT){return;}
if(!(channel instanceof Ci.nsIHttpChannel)){return;}
channel=channel.QueryInterface(Ci.nsIHttpChannel);if(activitySubtype==gActivityDistributor.ACTIVITY_SUBTYPE_REQUEST_HEADER){this._onRequestHeader(channel,timestamp,extraStringData);return;}

let httpActivity=null;for(let id in this.openRequests){let item=this.openRequests[id];if(item.channel===channel){httpActivity=item;break;}}
if(!httpActivity){return;}
let transCodes=this.httpTransactionCodes;if(activitySubtype in transCodes){let stage=transCodes[activitySubtype];if(stage in httpActivity.timings){httpActivity.timings[stage].last=timestamp;}else{httpActivity.timings[stage]={first:timestamp,last:timestamp,};}}
switch(activitySubtype){case gActivityDistributor.ACTIVITY_SUBTYPE_REQUEST_BODY_SENT:this._onRequestBodySent(httpActivity);break;case gActivityDistributor.ACTIVITY_SUBTYPE_RESPONSE_HEADER:this._onResponseHeader(httpActivity,extraStringData);break;case gActivityDistributor.ACTIVITY_SUBTYPE_TRANSACTION_CLOSE:this._onTransactionClose(httpActivity);break;default:break;}}),_matchRequest:function(channel){if(this._logEverything){return true;}



if(!DevToolsUtils.testing&&channel.loadInfo&&channel.loadInfo.loadingDocument===null&&channel.loadInfo.loadingPrincipal===Services.scriptSecurityManager.getSystemPrincipal()){return false;}
if(this.window){
let win=NetworkHelper.getWindowForRequest(channel);while(win){if(win==this.window){return true;}
if(win.parent==win){break;}
win=win.parent;}}
if(this.topFrame){let topFrame=NetworkHelper.getTopFrameForRequest(channel);if(topFrame&&topFrame===this.topFrame){return true;}}
if(this.appId){let appId=NetworkHelper.getAppIdForRequest(channel);if(appId&&appId==this.appId){return true;}}


if(channel.loadInfo&&channel.loadInfo.externalContentPolicyType==Ci.nsIContentPolicy.TYPE_BEACON){let nonE10sMatch=this.window&&channel.loadInfo.loadingDocument===this.window.document;const loadingPrincipal=channel.loadInfo.loadingPrincipal;let e10sMatch=this.topFrame&&this.topFrame.contentPrincipal&&this.topFrame.contentPrincipal.equals(loadingPrincipal)&&this.topFrame.contentPrincipal.URI.spec==channel.referrer.spec;let b2gMatch=this.appId&&loadingPrincipal.appId===this.appId;if(nonE10sMatch||e10sMatch||b2gMatch){return true;}}
return false;},_createNetworkEvent:function(channel,{timestamp,extraStringData,fromCache,fromServiceWorker}){let win=NetworkHelper.getWindowForRequest(channel);let httpActivity=this.createActivityObject(channel);httpActivity.charset=win?win.document.characterSet:null;channel.QueryInterface(Ci.nsIPrivateBrowsingChannel);httpActivity.private=channel.isChannelPrivate;if(timestamp){httpActivity.timings.REQUEST_HEADER={first:timestamp,last:timestamp};}
let event={};event.method=channel.requestMethod;event.url=channel.URI.spec;event.private=httpActivity.private;event.headersSize=0;event.startedDateTime=(timestamp?new Date(Math.round(timestamp/1000)):new Date()).toISOString();event.fromCache=fromCache;event.fromServiceWorker=fromServiceWorker;httpActivity.fromServiceWorker=fromServiceWorker;if(extraStringData){event.headersSize=extraStringData.length;}
httpActivity.isXHR=event.isXHR=(channel.loadInfo.externalContentPolicyType===Ci.nsIContentPolicy.TYPE_XMLHTTPREQUEST||channel.loadInfo.externalContentPolicyType===Ci.nsIContentPolicy.TYPE_FETCH);let httpVersionMaj={};let httpVersionMin={};channel.QueryInterface(Ci.nsIHttpChannelInternal);channel.getRequestVersion(httpVersionMaj,httpVersionMin);event.httpVersion="HTTP/"+httpVersionMaj.value+"."+
httpVersionMin.value;event.discardRequestBody=!this.saveRequestAndResponseBodies;event.discardResponseBody=!this.saveRequestAndResponseBodies;let headers=[];let cookies=[];let cookieHeader=null;channel.visitRequestHeaders({visitHeader:function(name,value){if(name=="Cookie"){cookieHeader=value;}
headers.push({name:name,value:value});}});if(cookieHeader){cookies=NetworkHelper.parseCookieHeader(cookieHeader);}
httpActivity.owner=this.owner.onNetworkEvent(event,channel);this._setupResponseListener(httpActivity);httpActivity.owner.addRequestHeaders(headers,extraStringData);httpActivity.owner.addRequestCookies(cookies);this.openRequests[httpActivity.id]=httpActivity;return httpActivity;},_onRequestHeader:function(channel,timestamp,extraStringData){if(!this._matchRequest(channel)){return;}
this._createNetworkEvent(channel,{timestamp:timestamp,extraStringData:extraStringData});},createActivityObject:function(channel){return{id:gSequenceId(),channel:channel,charset:null,url:channel.URI.spec, hostname:channel.URI.host,discardRequestBody:!this.saveRequestAndResponseBodies,discardResponseBody:!this.saveRequestAndResponseBodies,timings:{},responseStatus:null, owner:null,};},_setupResponseListener:function(httpActivity){let channel=httpActivity.channel;channel.QueryInterface(Ci.nsITraceableChannel);
let sink=Cc["@mozilla.org/pipe;1"].createInstance(Ci.nsIPipe);
sink.init(false,false,this.responsePipeSegmentSize,PR_UINT32_MAX,null);let newListener=new NetworkResponseListener(this,httpActivity);newListener.inputStream=sink.inputStream;newListener.sink=sink;let tee=Cc["@mozilla.org/network/stream-listener-tee;1"].createInstance(Ci.nsIStreamListenerTee);let originalListener=channel.setNewListener(tee);tee.init(originalListener,sink.outputStream,newListener);},_onRequestBodySent:function(httpActivity){if(httpActivity.discardRequestBody){return;}
let sentBody=NetworkHelper.readPostTextFromRequest(httpActivity.channel,httpActivity.charset);if(!sentBody&&this.window&&httpActivity.url==this.window.location.href){




let webNav=this.window.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIWebNavigation);sentBody=NetworkHelper.readPostTextFromPageViaWebNav(webNav,httpActivity.charset);}
if(sentBody){httpActivity.owner.addRequestPostData({text:sentBody});}},_onResponseHeader:function(httpActivity,extraStringData){




let headers=extraStringData.split(/\r\n|\n|\r/);let statusLine=headers.shift();let statusLineArray=statusLine.split(" ");let response={};response.httpVersion=statusLineArray.shift();response.remoteAddress=httpActivity.channel.remoteAddress;response.remotePort=httpActivity.channel.remotePort;response.status=statusLineArray.shift();response.statusText=statusLineArray.join(" ");response.headersSize=extraStringData.length;httpActivity.responseStatus=response.status;switch(parseInt(response.status,10)){case HTTP_MOVED_PERMANENTLY:case HTTP_FOUND:case HTTP_SEE_OTHER:case HTTP_TEMPORARY_REDIRECT:httpActivity.discardResponseBody=true;break;}
response.discardResponseBody=httpActivity.discardResponseBody;httpActivity.owner.addResponseStart(response,extraStringData);},_onTransactionClose:function(httpActivity){let result=this._setupHarTimings(httpActivity);httpActivity.owner.addEventTimings(result.total,result.timings);delete this.openRequests[httpActivity.id];},_setupHarTimings:function(httpActivity,fromCache){if(fromCache){
 return{total:0,timings:{blocked:0,dns:0,connect:0,send:0,wait:0,receive:0}};}
let timings=httpActivity.timings;let harTimings={};harTimings.blocked=-1;
harTimings.dns=timings.STATUS_RESOLVING&&timings.STATUS_RESOLVED?timings.STATUS_RESOLVED.last-
timings.STATUS_RESOLVING.first:-1;if(timings.STATUS_CONNECTING_TO&&timings.STATUS_CONNECTED_TO){harTimings.connect=timings.STATUS_CONNECTED_TO.last-
timings.STATUS_CONNECTING_TO.first;}else if(timings.STATUS_SENDING_TO){harTimings.connect=timings.STATUS_SENDING_TO.first-
timings.REQUEST_HEADER.first;}else{harTimings.connect=-1;}
if((timings.STATUS_WAITING_FOR||timings.STATUS_RECEIVING_FROM)&&(timings.STATUS_CONNECTED_TO||timings.STATUS_SENDING_TO)){harTimings.send=(timings.STATUS_WAITING_FOR||timings.STATUS_RECEIVING_FROM).first-
(timings.STATUS_CONNECTED_TO||timings.STATUS_SENDING_TO).last;}else{harTimings.send=-1;}
if(timings.RESPONSE_START){harTimings.wait=timings.RESPONSE_START.first-
(timings.REQUEST_BODY_SENT||timings.STATUS_SENDING_TO).last;}else{harTimings.wait=-1;}
if(timings.RESPONSE_START&&timings.RESPONSE_COMPLETE){harTimings.receive=timings.RESPONSE_COMPLETE.last-
timings.RESPONSE_START.first;}else{harTimings.receive=-1;}
let totalTime=0;for(let timing in harTimings){let time=Math.max(Math.round(harTimings[timing]/1000),-1);harTimings[timing]=time;if(time>-1){totalTime+=time;}}
return{total:totalTime,timings:harTimings,};},destroy:function(){if(Services.appinfo.processType!=Ci.nsIXULRuntime.PROCESS_TYPE_CONTENT){gActivityDistributor.removeObserver(this);Services.obs.removeObserver(this._httpResponseExaminer,"http-on-examine-response");Services.obs.removeObserver(this._httpResponseExaminer,"http-on-examine-cached-response");}
Services.obs.removeObserver(this._serviceWorkerRequest,"service-worker-synthesized-response");this.interceptedChannels.clear();this.openRequests={};this.openResponses={};this.owner=null;this.window=null;this.topFrame=null;},};function NetworkMonitorChild(appId,messageManager,connID,owner){this.appId=appId;this.connID=connID;this.owner=owner;this._messageManager=messageManager;this._onNewEvent=this._onNewEvent.bind(this);this._onUpdateEvent=this._onUpdateEvent.bind(this);this._netEvents=new Map();}
exports.NetworkMonitorChild=NetworkMonitorChild;NetworkMonitorChild.prototype={appId:null,owner:null,_netEvents:null,_saveRequestAndResponseBodies:true,get saveRequestAndResponseBodies(){return this._saveRequestAndResponseBodies;},set saveRequestAndResponseBodies(val){this._saveRequestAndResponseBodies=val;this._messageManager.sendAsyncMessage("debug:netmonitor:"+this.connID,{appId:this.appId,action:"setPreferences",preferences:{saveRequestAndResponseBodies:this._saveRequestAndResponseBodies,},});},init:function(){let mm=this._messageManager;mm.addMessageListener("debug:netmonitor:"+this.connID+":newEvent",this._onNewEvent);mm.addMessageListener("debug:netmonitor:"+this.connID+":updateEvent",this._onUpdateEvent);mm.sendAsyncMessage("debug:netmonitor:"+this.connID,{appId:this.appId,action:"start",});},_onNewEvent:DevToolsUtils.makeInfallible(function _onNewEvent(msg){let{id,event}=msg.data;let actor=this.owner.onNetworkEvent(event);this._netEvents.set(id,Cu.getWeakReference(actor));}),_onUpdateEvent:DevToolsUtils.makeInfallible(function _onUpdateEvent(msg){let{id,method,args}=msg.data;let weakActor=this._netEvents.get(id);let actor=weakActor?weakActor.get():null;if(!actor){Cu.reportError("Received debug:netmonitor:updateEvent for unknown "+"event ID: "+id);return;}
if(!(method in actor)){Cu.reportError("Received debug:netmonitor:updateEvent unsupported "+"method: "+method);return;}
actor[method].apply(actor,args);}),destroy:function(){let mm=this._messageManager;try{mm.removeMessageListener("debug:netmonitor:"+this.connID+":newEvent",this._onNewEvent);mm.removeMessageListener("debug:netmonitor:"+this.connID+":updateEvent",this._onUpdateEvent);}catch(e){

}
this._netEvents.clear();this._messageManager=null;this.owner=null;},};function NetworkEventActorProxy(messageManager,connID){this.id=gSequenceId();this.connID=connID;this.messageManager=messageManager;}
exports.NetworkEventActorProxy=NetworkEventActorProxy;NetworkEventActorProxy.methodFactory=function(method){return DevToolsUtils.makeInfallible(function(){let args=Array.slice(arguments);let mm=this.messageManager;mm.sendAsyncMessage("debug:netmonitor:"+this.connID+":updateEvent",{id:this.id,method:method,args:args,});},"NetworkEventActorProxy."+method);};NetworkEventActorProxy.prototype={init:DevToolsUtils.makeInfallible(function(event){let mm=this.messageManager;mm.sendAsyncMessage("debug:netmonitor:"+this.connID+":newEvent",{id:this.id,event:event,});return this;}),};(function(){let methods=["addRequestHeaders","addRequestCookies","addRequestPostData","addResponseStart","addSecurityInfo","addResponseHeaders","addResponseCookies","addResponseContent","addEventTimings"];let factory=NetworkEventActorProxy.methodFactory;for(let method of methods){NetworkEventActorProxy.prototype[method]=factory(method);}})();function NetworkMonitorManager(frame,id){this.id=id;let mm=frame.QueryInterface(Ci.nsIFrameLoaderOwner).frameLoader.messageManager;this.messageManager=mm;this.frame=frame;this.onNetMonitorMessage=this.onNetMonitorMessage.bind(this);this.onNetworkEvent=this.onNetworkEvent.bind(this);mm.addMessageListener("debug:netmonitor:"+id,this.onNetMonitorMessage);}
exports.NetworkMonitorManager=NetworkMonitorManager;NetworkMonitorManager.prototype={netMonitor:null,frame:null,messageManager:null,onNetMonitorMessage:DevToolsUtils.makeInfallible(function(msg){let{action,appId}=msg.json;switch(action){case"start":if(!this.netMonitor){this.netMonitor=new NetworkMonitor({topFrame:this.frame,appId:appId,},this);this.netMonitor.init();}
break;case"setPreferences":{let{preferences}=msg.json;for(let key of Object.keys(preferences)){if(key=="saveRequestAndResponseBodies"&&this.netMonitor){this.netMonitor.saveRequestAndResponseBodies=preferences[key];}}
break;}
case"stop":if(this.netMonitor){this.netMonitor.destroy();this.netMonitor=null;}
break;case"disconnect":this.destroy();break;}}),onNetworkEvent:DevToolsUtils.makeInfallible(function _onNetworkEvent(event){return new NetworkEventActorProxy(this.messageManager,this.id).init(event);}),destroy:function(){if(this.messageManager){this.messageManager.removeMessageListener("debug:netmonitor:"+this.id,this.onNetMonitorMessage);}
this.messageManager=null;this.filters=null;if(this.netMonitor){this.netMonitor.destroy();this.netMonitor=null;}},};function ConsoleProgressListener(window,owner){this.window=window;this.owner=owner;}
exports.ConsoleProgressListener=ConsoleProgressListener;ConsoleProgressListener.prototype={MONITOR_FILE_ACTIVITY:1,MONITOR_LOCATION_CHANGE:2,_fileActivity:false,_locationChange:false,_initialized:false,_webProgress:null,QueryInterface:XPCOMUtils.generateQI([Ci.nsIWebProgressListener,Ci.nsISupportsWeakReference]),_init:function(){if(this._initialized){return;}
this._webProgress=this.window.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIWebNavigation).QueryInterface(Ci.nsIWebProgress);this._webProgress.addProgressListener(this,Ci.nsIWebProgress.NOTIFY_STATE_ALL);this._initialized=true;},startMonitor:function(monitor){switch(monitor){case this.MONITOR_FILE_ACTIVITY:this._fileActivity=true;break;case this.MONITOR_LOCATION_CHANGE:this._locationChange=true;break;default:throw new Error("ConsoleProgressListener: unknown monitor type "+
monitor+"!");}
this._init();},stopMonitor:function(monitor){switch(monitor){case this.MONITOR_FILE_ACTIVITY:this._fileActivity=false;break;case this.MONITOR_LOCATION_CHANGE:this._locationChange=false;break;default:throw new Error("ConsoleProgressListener: unknown monitor type "+
monitor+"!");}
if(!this._fileActivity&&!this._locationChange){this.destroy();}},onStateChange:function(progress,request,state,status){if(!this.owner){return;}
if(this._fileActivity){this._checkFileActivity(progress,request,state,status);}
if(this._locationChange){this._checkLocationChange(progress,request,state,status);}},_checkFileActivity:function(progress,request,state,status){if(!(state&Ci.nsIWebProgressListener.STATE_START)){return;}
let uri=null;if(request instanceof Ci.imgIRequest){let imgIRequest=request.QueryInterface(Ci.imgIRequest);uri=imgIRequest.URI;}else if(request instanceof Ci.nsIChannel){let nsIChannel=request.QueryInterface(Ci.nsIChannel);uri=nsIChannel.URI;}
if(!uri||!uri.schemeIs("file")&&!uri.schemeIs("ftp")){return;}
this.owner.onFileActivity(uri.spec);},_checkLocationChange:function(progress,request,state){let isStart=state&Ci.nsIWebProgressListener.STATE_START;let isStop=state&Ci.nsIWebProgressListener.STATE_STOP;let isNetwork=state&Ci.nsIWebProgressListener.STATE_IS_NETWORK;let isWindow=state&Ci.nsIWebProgressListener.STATE_IS_WINDOW;if(!isNetwork||!isWindow||progress.DOMWindow!=this.window){return;}
if(isStart&&request instanceof Ci.nsIChannel){this.owner.onLocationChange("start",request.URI.spec,"");}else if(isStop){this.owner.onLocationChange("stop",this.window.location.href,this.window.document.title);}},onLocationChange:function(){},onStatusChange:function(){},onProgressChange:function(){},onSecurityChange:function(){},destroy:function(){if(!this._initialized){return;}
this._initialized=false;this._fileActivity=false;this._locationChange=false;try{this._webProgress.removeProgressListener(this);}catch(ex){}
this._webProgress=null;this.window=null;this.owner=null;},};function gSequenceId(){return gSequenceId.n++;}
gSequenceId.n=1;