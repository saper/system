"use strict";const Cu=Components.utils;Cu.import("resource://gre/modules/XPCOMUtils.jsm");Cu.import("resource://gre/modules/Console.jsm");const{require}=Cu.import("resource://devtools/shared/Loader.jsm",{});const DevToolsUtils=require("devtools/shared/DevToolsUtils");XPCOMUtils.defineLazyModuleGetter(this,"Reflect","resource://gre/modules/reflect.jsm");this.EXPORTED_SYMBOLS=["Parser","ParserHelpers","SyntaxTreeVisitor"];this.Parser=function Parser(){this._cache=new Map();this.errors=[];this.logExceptions=true;};Parser.prototype={get:function(source,url=""){if(this._cache.has(url)){return this._cache.get(url);}

let regexp=/<script[^>]*?(?:>([^]*?)<\/script\s*>|\/>)/gim;let syntaxTrees=[];let scriptMatches=[];let scriptMatch;if(source.match(/^\s*</)){while((scriptMatch=regexp.exec(source))){
 scriptMatches.push(scriptMatch[1]||"");}}

if(!scriptMatches.length){try{let nodes=Reflect.parse(source);let length=source.length;syntaxTrees.push(new SyntaxTree(nodes,url,length));}catch(e){this.errors.push(e);if(this.logExceptions){DevToolsUtils.reportException(url,e);}}}else{for(let script of scriptMatches){try{let nodes=Reflect.parse(script);let offset=source.indexOf(script);let length=script.length;syntaxTrees.push(new SyntaxTree(nodes,url,length,offset));}catch(e){this.errors.push(e);if(this.logExceptions){DevToolsUtils.reportException(url,e);}}}}
let pool=new SyntaxTreesPool(syntaxTrees,url);

if(url){this._cache.set(url,pool);}
return pool;},clearCache:function(){this._cache.clear();},clearSource:function(url){this._cache.delete(url);},_cache:null,errors:null};function SyntaxTreesPool(syntaxTrees,url="<unknown>"){this._trees=syntaxTrees;this._url=url;this._cache=new Map();}
SyntaxTreesPool.prototype={getIdentifierAt:function({line,column,scriptIndex,ignoreLiterals}){return this._call("getIdentifierAt",scriptIndex,line,column,ignoreLiterals)[0];},getNamedFunctionDefinitions:function(substring){return this._call("getNamedFunctionDefinitions",-1,substring);},getLastSyntaxTree:function(){return this._trees[this._trees.length-1];},get scriptCount(){return this._trees.length;},getScriptInfo:function(atOffset){let info={start:-1,length:-1,index:-1};for(let{offset,length}of this._trees){info.index++;if(offset<=atOffset&&offset+length>=atOffset){info.start=offset;info.length=length;return info;}}
info.index=-1;return info;},_call:function(functionName,syntaxTreeIndex,...params){let results=[];let requestId=[functionName,syntaxTreeIndex,params].toSource();if(this._cache.has(requestId)){return this._cache.get(requestId);}
let requestedTree=this._trees[syntaxTreeIndex];let targettedTrees=requestedTree?[requestedTree]:this._trees;for(let syntaxTree of targettedTrees){try{let parseResults=syntaxTree[functionName].apply(syntaxTree,params);if(parseResults){parseResults.sourceUrl=syntaxTree.url;parseResults.scriptLength=syntaxTree.length;parseResults.scriptOffset=syntaxTree.offset;results.push(parseResults);}}catch(e){

DevToolsUtils.reportException(`Syntax tree visitor for ${this._url}`,e);}}
this._cache.set(requestId,results);return results;},_trees:null,_cache:null};function SyntaxTree(nodes,url,length,offset=0){this.AST=nodes;this.url=url;this.length=length;this.offset=offset;}
SyntaxTree.prototype={getIdentifierAt:function(line,column,ignoreLiterals){let info=null;SyntaxTreeVisitor.walk(this.AST,{onIdentifier:function(node){if(ParserHelpers.nodeContainsPoint(node,line,column)){info={name:node.name,location:ParserHelpers.getNodeLocation(node),evalString:ParserHelpers.getIdentifierEvalString(node)};SyntaxTreeVisitor.break=true;}},onLiteral:function(node){if(!ignoreLiterals){this.onIdentifier(node);}},onThisExpression:function(node){this.onIdentifier(node);}});return info;},getNamedFunctionDefinitions:function(substring){let lowerCaseToken=substring.toLowerCase();let store=[];function includesToken(name){return name&&name.toLowerCase().includes(lowerCaseToken);}
SyntaxTreeVisitor.walk(this.AST,{onFunctionDeclaration:function(node){let functionName=node.id.name;if(includesToken(functionName)){store.push({functionName:functionName,functionLocation:ParserHelpers.getNodeLocation(node)});}},onFunctionExpression:function(node){let functionName=node.id?node.id.name:"";let functionLocation=ParserHelpers.getNodeLocation(node);let inferredInfo=ParserHelpers.inferFunctionExpressionInfo(node);let inferredName=inferredInfo.name;let inferredChain=inferredInfo.chain;let inferredLocation=inferredInfo.loc;if(node._parent.type=="AssignmentExpression"){this.onFunctionExpression(node._parent);}
if(includesToken(functionName)||includesToken(inferredName)){store.push({functionName:functionName,functionLocation:functionLocation,inferredName:inferredName,inferredChain:inferredChain,inferredLocation:inferredLocation});}},onArrowFunctionExpression:function(node){let inferredInfo=ParserHelpers.inferFunctionExpressionInfo(node);let inferredName=inferredInfo.name;let inferredChain=inferredInfo.chain;let inferredLocation=inferredInfo.loc;if(node._parent.type=="AssignmentExpression"){this.onFunctionExpression(node._parent);}
if(includesToken(inferredName)){store.push({inferredName:inferredName,inferredChain:inferredChain,inferredLocation:inferredLocation});}}});return store;},AST:null,url:"",length:0,offset:0};var ParserHelpers={getNodeLocation:function(node){if(node.type!="Identifier"){return node.loc;}

let{loc:parentLocation,type:parentType}=node._parent;let{loc:nodeLocation}=node;if(!nodeLocation){if(parentType=="FunctionDeclaration"||parentType=="FunctionExpression"){let loc=Cu.cloneInto(parentLocation,{});loc.end.line=loc.start.line;loc.end.column=loc.start.column+node.name.length;return loc;}
if(parentType=="MemberExpression"){let loc=Cu.cloneInto(parentLocation,{});loc.start.line=loc.end.line;loc.start.column=loc.end.column-node.name.length;return loc;}
if(parentType=="LabeledStatement"){let loc=Cu.cloneInto(parentLocation,{});loc.end.line=loc.start.line;loc.end.column=loc.start.column+node.name.length;return loc;}
if(parentType=="ContinueStatement"||parentType=="BreakStatement"){let loc=Cu.cloneInto(parentLocation,{});loc.start.line=loc.end.line;loc.start.column=loc.end.column-node.name.length;return loc;}}else if(parentType=="VariableDeclarator"){let loc=Cu.cloneInto(nodeLocation,{});loc.end.line=loc.start.line;loc.end.column=loc.start.column+node.name.length;return loc;}
return node.loc;},nodeContainsLine:function(node,line){let{start:s,end:e}=this.getNodeLocation(node);return s.line<=line&&e.line>=line;},nodeContainsPoint:function(node,line,column){let{start:s,end:e}=this.getNodeLocation(node);return s.line==line&&e.line==line&&s.column<=column&&e.column>=column;},inferFunctionExpressionInfo:function(node){let parent=node._parent;
if(parent.type=="VariableDeclarator"){return{name:parent.id.name,chain:null,loc:this.getNodeLocation(parent.id)};}

if(parent.type=="AssignmentExpression"){let propertyChain=this._getMemberExpressionPropertyChain(parent.left);let propertyLeaf=propertyChain.pop();return{name:propertyLeaf,chain:propertyChain,loc:this.getNodeLocation(parent.left)};}

if(parent.type=="ObjectExpression"){let propertyKey=this._getObjectExpressionPropertyKeyForValue(node);let propertyChain=this._getObjectExpressionPropertyChain(parent);let propertyLeaf=propertyKey.name;return{name:propertyLeaf,chain:propertyChain,loc:this.getNodeLocation(propertyKey)};}
return{name:"",chain:null,loc:null};},_getObjectExpressionPropertyKeyForValue:function(node){let parent=node._parent;if(parent.type!="ObjectExpression"){return null;}
for(let property of parent.properties){if(property.value==node){return property.key;}}},_getObjectExpressionPropertyChain:function(node,aStore=[]){switch(node.type){case"ObjectExpression":this._getObjectExpressionPropertyChain(node._parent,aStore);let propertyKey=this._getObjectExpressionPropertyKeyForValue(node);if(propertyKey){aStore.push(propertyKey.name);}
break;case"VariableDeclarator":aStore.push(node.id.name);break;
case"AssignmentExpression":this._getMemberExpressionPropertyChain(node.left,aStore);break;
case"NewExpression":case"CallExpression":this._getObjectExpressionPropertyChain(node._parent,aStore);break;}
return aStore;},_getMemberExpressionPropertyChain:function(node,store=[]){switch(node.type){case"MemberExpression":this._getMemberExpressionPropertyChain(node.object,store);this._getMemberExpressionPropertyChain(node.property,store);break;case"ThisExpression":store.push("this");break;case"Identifier":store.push(node.name);break;}
return store;},getIdentifierEvalString:function(node){switch(node._parent.type){case"ObjectExpression":

if(!this._getObjectExpressionPropertyKeyForValue(node)){let propertyChain=this._getObjectExpressionPropertyChain(node._parent);let propertyLeaf=node.name;return[...propertyChain,propertyLeaf].join(".");}
break;case"MemberExpression":if(node._parent.property==node){return this._getMemberExpressionPropertyChain(node._parent).join(".");}
break;}
switch(node.type){case"ThisExpression":return"this";case"Identifier":return node.name;case"Literal":return uneval(node.value);default:return"";}}};var SyntaxTreeVisitor={walk:function(tree,callbacks){this.break=false;this[tree.type](tree,callbacks);},filter:function(tree,predicate){let store=[];this.walk(tree,{onNode:e=>{if(predicate(e)){store.push(e);}}});return store;},break:false,Program:function(node,callbacks){if(callbacks.onProgram){callbacks.onProgram(node);}
for(let statement of node.body){this[statement.type](statement,node,callbacks);}},Statement:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onStatement){callbacks.onStatement(node);}},EmptyStatement:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onEmptyStatement){callbacks.onEmptyStatement(node);}},BlockStatement:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onBlockStatement){callbacks.onBlockStatement(node);}
for(let statement of node.body){this[statement.type](statement,node,callbacks);}},ExpressionStatement:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onExpressionStatement){callbacks.onExpressionStatement(node);}
this[node.expression.type](node.expression,node,callbacks);},IfStatement:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onIfStatement){callbacks.onIfStatement(node);}
this[node.test.type](node.test,node,callbacks);this[node.consequent.type](node.consequent,node,callbacks);if(node.alternate){this[node.alternate.type](node.alternate,node,callbacks);}},LabeledStatement:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onLabeledStatement){callbacks.onLabeledStatement(node);}
this[node.label.type](node.label,node,callbacks);this[node.body.type](node.body,node,callbacks);},BreakStatement:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onBreakStatement){callbacks.onBreakStatement(node);}
if(node.label){this[node.label.type](node.label,node,callbacks);}},ContinueStatement:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onContinueStatement){callbacks.onContinueStatement(node);}
if(node.label){this[node.label.type](node.label,node,callbacks);}},WithStatement:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onWithStatement){callbacks.onWithStatement(node);}
this[node.object.type](node.object,node,callbacks);this[node.body.type](node.body,node,callbacks);},SwitchStatement:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onSwitchStatement){callbacks.onSwitchStatement(node);}
this[node.discriminant.type](node.discriminant,node,callbacks);for(let _case of node.cases){this[_case.type](_case,node,callbacks);}},ReturnStatement:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onReturnStatement){callbacks.onReturnStatement(node);}
if(node.argument){this[node.argument.type](node.argument,node,callbacks);}},ThrowStatement:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onThrowStatement){callbacks.onThrowStatement(node);}
this[node.argument.type](node.argument,node,callbacks);},TryStatement:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onTryStatement){callbacks.onTryStatement(node);}
this[node.block.type](node.block,node,callbacks);if(node.handler){this[node.handler.type](node.handler,node,callbacks);}
for(let guardedHandler of node.guardedHandlers){this[guardedHandler.type](guardedHandler,node,callbacks);}
if(node.finalizer){this[node.finalizer.type](node.finalizer,node,callbacks);}},WhileStatement:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onWhileStatement){callbacks.onWhileStatement(node);}
this[node.test.type](node.test,node,callbacks);this[node.body.type](node.body,node,callbacks);},DoWhileStatement:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onDoWhileStatement){callbacks.onDoWhileStatement(node);}
this[node.body.type](node.body,node,callbacks);this[node.test.type](node.test,node,callbacks);},ForStatement:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onForStatement){callbacks.onForStatement(node);}
if(node.init){this[node.init.type](node.init,node,callbacks);}
if(node.test){this[node.test.type](node.test,node,callbacks);}
if(node.update){this[node.update.type](node.update,node,callbacks);}
this[node.body.type](node.body,node,callbacks);},ForInStatement:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onForInStatement){callbacks.onForInStatement(node);}
this[node.left.type](node.left,node,callbacks);this[node.right.type](node.right,node,callbacks);this[node.body.type](node.body,node,callbacks);},ForOfStatement:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onForOfStatement){callbacks.onForOfStatement(node);}
this[node.left.type](node.left,node,callbacks);this[node.right.type](node.right,node,callbacks);this[node.body.type](node.body,node,callbacks);},LetStatement:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onLetStatement){callbacks.onLetStatement(node);}
for(let{id,init}of node.head){this[id.type](id,node,callbacks);if(init){this[init.type](init,node,callbacks);}}
this[node.body.type](node.body,node,callbacks);},DebuggerStatement:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onDebuggerStatement){callbacks.onDebuggerStatement(node);}},Declaration:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onDeclaration){callbacks.onDeclaration(node);}},FunctionDeclaration:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onFunctionDeclaration){callbacks.onFunctionDeclaration(node);}
this[node.id.type](node.id,node,callbacks);for(let param of node.params){this[param.type](param,node,callbacks);}
for(let _default of node.defaults){if(_default){this[_default.type](_default,node,callbacks);}}
if(node.rest){this[node.rest.type](node.rest,node,callbacks);}
this[node.body.type](node.body,node,callbacks);},VariableDeclaration:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onVariableDeclaration){callbacks.onVariableDeclaration(node);}
for(let declaration of node.declarations){this[declaration.type](declaration,node,callbacks);}},VariableDeclarator:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onVariableDeclarator){callbacks.onVariableDeclarator(node);}
this[node.id.type](node.id,node,callbacks);if(node.init){this[node.init.type](node.init,node,callbacks);}},Expression:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onExpression){callbacks.onExpression(node);}},ThisExpression:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onThisExpression){callbacks.onThisExpression(node);}},ArrayExpression:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onArrayExpression){callbacks.onArrayExpression(node);}
for(let element of node.elements){if(element){this[element.type](element,node,callbacks);}}},SpreadExpression:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onSpreadExpression){callbacks.onSpreadExpression(node);}
this[node.expression.type](node.expression,node,callbacks);},ObjectExpression:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onObjectExpression){callbacks.onObjectExpression(node);}
for(let{key,value}of node.properties){this[key.type](key,node,callbacks);this[value.type](value,node,callbacks);}},FunctionExpression:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onFunctionExpression){callbacks.onFunctionExpression(node);}
if(node.id){this[node.id.type](node.id,node,callbacks);}
for(let param of node.params){this[param.type](param,node,callbacks);}
for(let _default of node.defaults){if(_default){this[_default.type](_default,node,callbacks);}}
if(node.rest){this[node.rest.type](node.rest,node,callbacks);}
this[node.body.type](node.body,node,callbacks);},ArrowFunctionExpression:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onArrowFunctionExpression){callbacks.onArrowFunctionExpression(node);}
for(let param of node.params){this[param.type](param,node,callbacks);}
for(let _default of node.defaults){if(_default){this[_default.type](_default,node,callbacks);}}
if(node.rest){this[node.rest.type](node.rest,node,callbacks);}
this[node.body.type](node.body,node,callbacks);},SequenceExpression:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onSequenceExpression){callbacks.onSequenceExpression(node);}
for(let expression of node.expressions){this[expression.type](expression,node,callbacks);}},UnaryExpression:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onUnaryExpression){callbacks.onUnaryExpression(node);}
this[node.argument.type](node.argument,node,callbacks);},BinaryExpression:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onBinaryExpression){callbacks.onBinaryExpression(node);}
this[node.left.type](node.left,node,callbacks);this[node.right.type](node.right,node,callbacks);},AssignmentExpression:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onAssignmentExpression){callbacks.onAssignmentExpression(node);}
this[node.left.type](node.left,node,callbacks);this[node.right.type](node.right,node,callbacks);},UpdateExpression:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onUpdateExpression){callbacks.onUpdateExpression(node);}
this[node.argument.type](node.argument,node,callbacks);},LogicalExpression:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onLogicalExpression){callbacks.onLogicalExpression(node);}
this[node.left.type](node.left,node,callbacks);this[node.right.type](node.right,node,callbacks);},ConditionalExpression:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onConditionalExpression){callbacks.onConditionalExpression(node);}
this[node.test.type](node.test,node,callbacks);this[node.alternate.type](node.alternate,node,callbacks);this[node.consequent.type](node.consequent,node,callbacks);},NewExpression:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onNewExpression){callbacks.onNewExpression(node);}
this[node.callee.type](node.callee,node,callbacks);for(let argument of node.arguments){if(argument){this[argument.type](argument,node,callbacks);}}},CallExpression:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onCallExpression){callbacks.onCallExpression(node);}
this[node.callee.type](node.callee,node,callbacks);for(let argument of node.arguments){if(argument){if(!this[argument.type]){console.error("Unknown parser object:",argument.type);}
this[argument.type](argument,node,callbacks);}}},MemberExpression:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onMemberExpression){callbacks.onMemberExpression(node);}
this[node.object.type](node.object,node,callbacks);this[node.property.type](node.property,node,callbacks);},YieldExpression:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onYieldExpression){callbacks.onYieldExpression(node);}
if(node.argument){this[node.argument.type](node.argument,node,callbacks);}},ComprehensionExpression:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onComprehensionExpression){callbacks.onComprehensionExpression(node);}
this[node.body.type](node.body,node,callbacks);for(let block of node.blocks){this[block.type](block,node,callbacks);}
if(node.filter){this[node.filter.type](node.filter,node,callbacks);}},GeneratorExpression:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onGeneratorExpression){callbacks.onGeneratorExpression(node);}
this[node.body.type](node.body,node,callbacks);for(let block of node.blocks){this[block.type](block,node,callbacks);}
if(node.filter){this[node.filter.type](node.filter,node,callbacks);}},GraphExpression:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onGraphExpression){callbacks.onGraphExpression(node);}
this[node.expression.type](node.expression,node,callbacks);},GraphIndexExpression:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onGraphIndexExpression){callbacks.onGraphIndexExpression(node);}},LetExpression:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onLetExpression){callbacks.onLetExpression(node);}
for(let{id,init}of node.head){this[id.type](id,node,callbacks);if(init){this[init.type](init,node,callbacks);}}
this[node.body.type](node.body,node,callbacks);},Pattern:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onPattern){callbacks.onPattern(node);}},ObjectPattern:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onObjectPattern){callbacks.onObjectPattern(node);}
for(let{key,value}of node.properties){this[key.type](key,node,callbacks);this[value.type](value,node,callbacks);}},ArrayPattern:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onArrayPattern){callbacks.onArrayPattern(node);}
for(let element of node.elements){if(element){this[element.type](element,node,callbacks);}}},SwitchCase:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onSwitchCase){callbacks.onSwitchCase(node);}
if(node.test){this[node.test.type](node.test,node,callbacks);}
for(let consequent of node.consequent){this[consequent.type](consequent,node,callbacks);}},CatchClause:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onCatchClause){callbacks.onCatchClause(node);}
this[node.param.type](node.param,node,callbacks);if(node.guard){this[node.guard.type](node.guard,node,callbacks);}
this[node.body.type](node.body,node,callbacks);},ComprehensionBlock:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onComprehensionBlock){callbacks.onComprehensionBlock(node);}
this[node.left.type](node.left,node,callbacks);this[node.right.type](node.right,node,callbacks);},Identifier:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onIdentifier){callbacks.onIdentifier(node);}},Literal:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onLiteral){callbacks.onLiteral(node);}},TemplateLiteral:function(node,parent,callbacks){node._parent=parent;if(this.break){return;}
if(callbacks.onNode){if(callbacks.onNode(node,parent)===false){return;}}
if(callbacks.onTemplateLiteral){callbacks.onTemplateLiteral(node);}
for(let element of node.elements){if(element){this[element.type](element,node,callbacks);}}}};XPCOMUtils.defineLazyGetter(Parser,"reflectionAPI",()=>Reflect);