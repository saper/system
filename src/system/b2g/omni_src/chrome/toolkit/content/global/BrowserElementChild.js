"use strict";var{classes:Cc,interfaces:Ci,results:Cr,utils:Cu}=Components;Cu.import("resource://gre/modules/Services.jsm");function debug(msg){}

docShell.isActive=true;function parentDocShell(docshell){if(!docshell){return null;}
let treeitem=docshell.QueryInterface(Ci.nsIDocShellTreeItem);return treeitem.parent?treeitem.parent.QueryInterface(Ci.nsIDocShell):null;}
function isTopBrowserElement(docShell){while(docShell){docShell=parentDocShell(docShell);if(docShell&&docShell.isMozBrowserOrApp){return false;}}
return true;}
if(!('BrowserElementIsPreloaded'in this)){if(isTopBrowserElement(docShell)){if(Services.prefs.getBoolPref("dom.mozInputMethod.enabled")){try{Services.scriptloader.loadSubScript("chrome://global/content/forms.js");}catch(e){}}}
if(Services.appinfo.processType==Services.appinfo.PROCESS_TYPE_CONTENT){ if(isTopBrowserElement(docShell)){Services.scriptloader.loadSubScript("chrome://global/content/BrowserElementCopyPaste.js");}}else{Services.scriptloader.loadSubScript("chrome://global/content/BrowserElementCopyPaste.js");}
if(Services.prefs.getIntPref("dom.w3c_touch_events.enabled")!=0){if(docShell.asyncPanZoomEnabled===false){Services.scriptloader.loadSubScript("chrome://global/content/BrowserElementPanningAPZDisabled.js");ContentPanningAPZDisabled.init();}
Services.scriptloader.loadSubScript("chrome://global/content/BrowserElementPanning.js");ContentPanning.init();}
Services.scriptloader.loadSubScript("chrome://global/content/BrowserElementChildPreload.js");}else{if(Services.prefs.getIntPref("dom.w3c_touch_events.enabled")!=0){if(docShell.asyncPanZoomEnabled===false){ContentPanningAPZDisabled.init();}
ContentPanning.init();}}
var BrowserElementIsReady=true;sendAsyncMessage('browser-element-api:call',{'msg_name':'hello'});